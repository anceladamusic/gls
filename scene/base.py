import ipdb
import math
import copy
from pprint import pprint
from typing import (Dict)

from django.apps import apps


class BoundingBox():

    def __init__(self, **kwargs):
        self.minx = float('inf')
        self.miny = float('inf')
        self.maxx = float('-inf')
        self.maxy = float('-inf')
        self.vertices = kwargs['vertices']
        self.__update()

    def __update(self):

        for v in self.vertices:

            if v['x'] < self.minx:

                self.minx = v['x']

            if v['y'] < self.miny:

                self.miny = v['y']

            if v['x'] > self.maxx:

                self.maxx = v['x']

            if v['y'] > self.maxy:

                self.maxy = v['y']

    @property
    def size_x(self):
        return self.maxx - self.minx

    @property
    def size_y(self):
        return self.maxy - self.miny


class Base():

    def __init__(self):
        pass

    def __get_center_from_vertices(self, vertices):

        return {
            'x': sum([v['x'] for v in vertices])/len(vertices),
            'y': sum([v['y'] for v in vertices])/len(vertices),
        }

    def get_center(self, static):

        if hasattr(static, 'get_vertices') or hasattr(static, 'vertices'):

            try:
                vertices = static.get_vertices()

            except AttributeError:
                vertices = static.vertices

            if vertices:

                center = self.__get_center_from_vertices(vertices)

                center['z'] = static.minz + (static.maxz - static.minz)/2

            else:

                center = {
                    'x': static.minx + (static.maxx - static.minx)/2,
                    'y': static.miny + (static.maxy - static.miny)/2,
                    'z': static.minz + (static.maxz - static.minz)/2,
                }

        return center

    def vertices_to_angle(self, vertices, angle, center):

        for v in vertices:

            x = copy.deepcopy(v['x'])

            y = copy.deepcopy(v['y'])

            v['x'] = center['x'] + (x - center['x']) * math.cos(angle
                                                                ) - (y - center['y']) * math.sin(angle)

            v['y'] = center['y'] + (x - center['x']) * math.sin(angle
                                                                ) + (y - center['y']) * math.cos(angle)

        new_center = self.__get_center_from_vertices(vertices)

        # delta
        delta_x = center['x'] - new_center['x']
        delta_y = center['y'] - new_center['y']

        for v in vertices:

            v['x'] += delta_x
            v['y'] += delta_y

        return vertices

    def get_size(self, static) -> Dict:

        angle = static.zangle

        center = self.get_center(static)

        try:
            vertices = static.get_vertices()

        except AttributeError:
            vertices = static.vertices

        if vertices:

            if angle:

                if angle < 0:

                    to_zero_angle = abs(angle)

                else:

                    to_zero_angle = - angle

                # angle to zero
                vertices = self.vertices_to_angle(
                    vertices, to_zero_angle, center)

            bbox = BoundingBox(**{
                'vertices': vertices
            })

            return {
                'size_x': bbox.size_x,
                'size_y': bbox.size_y,
            }
        return {
            'size_x': static.maxx - static.minx,
            'size_y': static.maxy - static.miny,
        }
