""" models for scene objects """
import json
import ipdb
import datetime
import requests
from rest_framework.response import Response
from requests.exceptions import ConnectionError
from pprint import pprint
from typing import (
    Iterable,
    Optional,
)

from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings
from django.db.models.signals import (
    post_save,
    pre_save,
    pre_delete,
)
from django.dispatch import receiver

from .base import Base as SceneBase
from .football import Football

HOST: str = settings.POSITIONAL_SERVER_HOST
PORT: int = settings.POSITIONAL_SERVER_PORT
ACTIVE: bool = settings.POSITIONAL_SERVER_IS_ACTIVE

MODELS_SIGNALS_IS_ACTIVE: bool = settings.MODELS_SIGNALS_IS_ACTIVE
POSITIONAL_SERVER_IS_ACTIVE: bool = settings.POSITIONAL_SERVER_IS_ACTIVE


class LoadLandscape(models.Model):
    """ Fully Synchronize to SP server!!! layer in SP Loadlandscape in my db. (signal functions are created) """

    class Meta():

        """meta options """

        verbose_name = 'Сцена'
        verbose_name_plural = 'Сцены'

    id = models.AutoField('Идентификатор подложки', primary_key=True)
    # landscape_source = models.FileField('Файл', upload_to='landscapes/', blank=True, null=True)
    name: str = models.CharField('Наименование', max_length=100)
    camera_position_x: float = models.FloatField(
        'Камера x', validators=[MinValueValidator(0), MaxValueValidator(100)])
    camera_position_y: float = models.FloatField(
        'Камера y', validators=[MinValueValidator(-100), MaxValueValidator(0)])
    camera_position_z: float = models.FloatField(
        'Камера z', validators=[MinValueValidator(0), MaxValueValidator(100)])
    camera_up_x: int = models.SmallIntegerField('Ориентация камеры x', default=0, validators=[
                                                MinValueValidator(0), MaxValueValidator(1)])
    camera_up_y: int = models.SmallIntegerField('Ориентация камеры y', default=0, validators=[
                                                MinValueValidator(0), MaxValueValidator(1)])
    camera_up_z: int = models.SmallIntegerField('Ориентация камеры z', default=1, validators=[
                                                MinValueValidator(0), MaxValueValidator(1)])
    controls_target_x: float = models.FloatField(
        'Камера крутится вокруг x', validators=[MinValueValidator(0), MaxValueValidator(100)])
    controls_target_y: float = models.FloatField(
        'Камера крутится вокруг y', validators=[MinValueValidator(0), MaxValueValidator(100)])
    controls_target_z: float = models.FloatField(
        'Камера крутится вокруг z', validators=[MinValueValidator(0), MaxValueValidator(30)])
    dae_rotation_x: float = models.FloatField(
        'Поворот сцены по x(радиана)', default=3.14159265359, validators=[MinValueValidator(-3.14159265359), MaxValueValidator(3.14159265359)])
    dae_rotation_y: float = models.FloatField(
        'Поворот сцены по y(радиана)', default=3.14159265359, validators=[MinValueValidator(-3.14159265359), MaxValueValidator(3.14159265359)])
    dae_rotation_z: float = models.FloatField(
        'Поворот сцены по z(радиана)', default=3.14159265359, validators=[MinValueValidator(-3.14159265359), MaxValueValidator(3.14159265359)])
    dae_position_x: float = models.FloatField('Позиция сцены по x', validators=[
                                              MinValueValidator(0), MaxValueValidator(100)])
    dae_position_y: float = models.FloatField('Позиция сцены по y', validators=[
                                              MinValueValidator(0), MaxValueValidator(100)])
    dae_position_z: float = models.FloatField('Позиция сцены по z', validators=[
                                              MinValueValidator(0), MaxValueValidator(100)])
    minx: float = models.FloatField(
        'Координата левой нижней x', default=0, validators=[MinValueValidator(0), MaxValueValidator(10)])
    miny: float = models.FloatField(
        'Координата левой нижней y', default=0, validators=[MinValueValidator(0), MaxValueValidator(10)])
    minz: float = models.FloatField(
        'Координата левой нижней z', default=0, validators=[MinValueValidator(0), MaxValueValidator(0)])
    maxx: float = models.FloatField(
        'Координата правой верхней x', validators=[MinValueValidator(0), MaxValueValidator(300)])
    maxy: float = models.FloatField(
        'Координата правой верхней y', validators=[MinValueValidator(0), MaxValueValidator(300)])
    maxz: float = models.FloatField(
        'Координата правой верхней z', validators=[MinValueValidator(0), MaxValueValidator(300)])

    def __str__(self) -> str: return f'{self.name}|id:{self.id}'

    def as_tree(self):
        result = {}
        for field in self._meta.fields:
            result[field.name] = getattr(self, field.name)
        return result

    def get_layer(self):
        """ convert to get sp object """

        return {
            'id': self.id,
            'height1': 0,
            'height2': 0,
            'latitude1': 0,
            'latitude2': 0,
            'longitude1': 0,
            'longitude2': 0,
            'name': self.name,
            'scaleX': 0,
            'scaleY': 0,
            'x1': self.minx,
            'y1': self.miny,
            'z1': self.minz,
            'x2': self.maxx,
            'y2': self.maxy,
            'z2': self.maxz,
        }

    # как футбольное поле
    @classmethod
    def as_football_plane(cls, obj_id, name, length):
        """ create football plane by parameters """

        return {
            'id': obj_id,
            'name': name,
            # 'landscape_source': None,
            'camera_position_x': length['x']/2,
            'camera_position_y': -10,
            'camera_position_z': 25,
            'camera_up_x': 0,
            'camera_up_y': 0,
            'camera_up_z': 1,
            'controls_target_y': length['y']/2,
            'controls_target_x': length['x']/2,
            'controls_target_z': 0,
            'dae_rotation_x': 3.14159265359,
            'dae_rotation_y': 3.14159265359,
            'dae_rotation_z': 3.14159265359,
            'dae_position_x': length['x']/2,
            'dae_position_y': length['y']/2,
            'dae_position_z': 0,
            'maxx': length['x'],
            'maxy': length['y'],
            'maxz': 0,
            'minx': 0,
            'miny': 0,
            'minz': 0,
        }


@receiver(pre_save, sender=LoadLandscape)
def create_landscape(sender: LoadLandscape, instance: LoadLandscape, **kwargs) -> None:
    """ Auto creates object in SP

    Arguments:
        sender {LoadLandscape} -- LoadLanscape model
        instance {LoadLandscape} -- LoadLanscape object instance
    """
    if MODELS_SIGNALS_IS_ACTIVE:

        if ACTIVE:
            if not instance.id:
                data = {
                    'command': 'addLayer',
                    'layer': instance.get_layer(),
                }
                try:
                    response: Response = requests.post(
                        f'{HOST}:{PORT}', json.dumps(data))

                    if response.status_code == 400:
                        pprint(f'SP server answers ERROR {response.json()}')

                    instance.id = response.json()['layer']['id']
                except ConnectionError:
                    print('no create layer in SP!')


@receiver(pre_delete, sender=LoadLandscape)
def delete_landscape(sender: LoadLandscape, instance: LoadLandscape, **kwargs) -> None:
    """ Deletes LoadLandscape object from POSITIONAL_SERVER
    Arguments:
        sender {LoadLandscape} -- LoadLandscape model
        instance {Object} -- LoadLandscape object instance
    """
    if MODELS_SIGNALS_IS_ACTIVE:
        if ACTIVE:

            data = {
                'command': 'deleteLayer',
                'id': instance.id,
            }

            try:
                response = requests.post(
                    f'{settings.POSITIONAL_SERVER_HOST}:{settings.POSITIONAL_SERVER_PORT}', json.dumps(data))

                if response.status_code == 400:
                    pprint(f'SP server answers ERROR {response.json()}')
            except ConnectionError:
                pprint('no delete layer in SP!')


@receiver(post_save, sender=LoadLandscape)
def update_landscape(sender: LoadLandscape, instance: LoadLandscape, created: bool, **kwargs) -> None:
    """ Updates Landscape object in POSITIONAL_SERVER

    Arguments:
        sender {LoadLandscape} -- The model class
        instance {LoadLandscape} -- The actual instance being saved

    """

    if MODELS_SIGNALS_IS_ACTIVE:

        if ACTIVE:

            if not created:
                data = {
                    'command': 'updateLayer',
                    'layer': instance.get_layer(),
                }

                try:
                    response: Response = requests.post(
                        f'{settings.POSITIONAL_SERVER_HOST}:{settings.POSITIONAL_SERVER_PORT}', json.dumps(data))

                    if response.status_code == 400:
                        pprint(f'SP server answers ERROR {response.json()}')

                except ConnectionError:
                    print('no updated layer in SP!')


class Router(models.Model):
    """ Fully synchronize with SP Server. Signal methods are written """

    ROUTER_TYPE_CHOICES = (
        ('I', 'Обычный'),
        ('W', 'Этажный'),
    )

    BUTTON_CALL = 'buttonCall'
    BUTTON_ACCEPT = 'buttonAccept'
    BEEPER = 'beeper'
    CHARGE_LEVEL = 'chargeLevel'
    ACCELEROMETER = 'accelerometer'
    BAROMETER = 'barometer'

    ALL_SENSORS = '{0},{1},{2},{3},{4}'.format(BUTTON_CALL, BUTTON_ACCEPT, BEEPER,
                                               CHARGE_LEVEL, ACCELEROMETER)

    class Meta():
        verbose_name = 'Роутер'
        verbose_name_plural = 'Роутеры'

    id = models.BigIntegerField(
        primary_key=True,
        verbose_name='идентификатор роутера на сервере позиционирования')
    name: Optional[str] = models.CharField(
        'Наименование', max_length=100, null=True, blank=True)
    description: Optional[str] = models.TextField(
        'Описание', null=True, blank=True)
    x: float = models.FloatField('Координата по оси X', default=0)
    y: float = models.FloatField('Координата по оси Y', default=0)
    z: float = models.FloatField('Координата по оси Z', default=0)
    inuse: bool = models.BooleanField(
        'В настоящее время используется', default=False)
    LoadLandscape = models.ForeignKey(
        to=LoadLandscape, on_delete=models.CASCADE)

    router_type: str = models.CharField(
        'Тип', choices=ROUTER_TYPE_CHOICES, default='I', max_length=200)
    radius: float = models.FloatField(
        'Радиус в метрах', default=0, validators=[MinValueValidator(0)])
    min_num_points: int = models.PositiveIntegerField(
        'Минимальное количество точек', default=1, validators=[MinValueValidator(0)])
    rf_short_addr: int = models.PositiveIntegerField(
        'Идентификтор RF', default=0)
    pan_id: int = models.PositiveIntegerField('Идентификатор PAN', default=0)
    timeslot: int = models.PositiveIntegerField('Слот времемни', default=0)
    channel_rssi: int = models.PositiveIntegerField(
        'Канал RSSI', default=26, validators=[MaxValueValidator(26)])
    channel_glonas: int = models.PositiveIntegerField(
        'Канал Глонас', default=25, validators=[MaxValueValidator(26)])
    tx_power_level: int = models.IntegerField(
        'Уровень мощности TX', default=-4, validators=[MinValueValidator(-22), MaxValueValidator(5)])
    firmware_vesrsion: int = models.IntegerField('Версия ПО',
                                                 default=10, validators=[MinValueValidator(0)])
    supported_sensors: str = models.CharField('Поддерживаемые сенсоры', max_length=200,  null=True, blank=True,
                                              default=ALL_SENSORS)
    used_sensors: str = models.CharField('Используемые сенсоры', max_length=200, null=True, blank=True,
                                         default=ALL_SENSORS)

    def __str__(self) -> str: return f'{self.id}'

    @property
    def pk(self):
        return self.id

    def as_router(self) -> dict:
        """ Converts to sp type

        Returns:
            dict -- converts to sp object to post into SP server
        """

        return {
            'id': hex(self.id),
            'name': self.name,
            'description': self.description,
            # 'x': self.y,
            # 'y': self.z,
            # 'z': self.x,
            'x': self.x,
            'y': self.y,
            'z': self.z,
            'inUse': self.inuse,
            'idLayer': self.LoadLandscape.id,
            'type': self.router_type,
            'radius': self.radius,
            'minNumPoints': self.min_num_points,
            'properties': {
                'hardwareSettings': {
                    'rfShortAddr': self.rf_short_addr,
                    'panId': self.pan_id,
                    'timeslot': self.timeslot,
                    'channelRssi': self.channel_rssi,
                    'channelGlonas': self.channel_glonas,
                    'txPowerLevel': self.tx_power_level,
                    'firmwareVesrsion': self.firmware_vesrsion,
                    'supportedSensors': self.supported_sensors,
                    'usedSensors': self.used_sensors,
                }
            }
        }

    @staticmethod
    def from_router_to_db(router: dict)->dict:
        """Converts SP router json object to Router object

        Arguments:
            router {dict} -- router json object

        Returns:
            dict -- Router object dict to write in db by Router.objects.create(**this return)
        """

        return {
            'id': int(router['id'], 16),
            'name': router['name'],
            'description': router['description'],
            'x': router['x'],
            'y': router['y'],
            'z': router['z'],
            'inuse': router['inUse'],
            'LoadLandscape_id': router['idLayer'],
            'router_type': router['type'],
            'radius': router['radius'],
            'min_num_points': router['minNumPoints'],
            'rf_short_addr': router['properties']['hardwareSettings']['rfShortAddr'],
            'pan_id': router['properties']['hardwareSettings']['panId'],
            'timeslot': router['properties']['hardwareSettings']['timeslot'],
            'channel_rssi': router['properties']['hardwareSettings']['channelRssi'],
            'channel_glonas': router['properties']['hardwareSettings']['channelGlonas'],
            'tx_power_level': router['properties']['hardwareSettings']['txPowerLevel'],
            'firmware_vesrsion': router['properties']['hardwareSettings']['firmwareVesrsion'],
            'used_sensors': router['properties']['hardwareSettings']['usedSensors'],
        }


@receiver(post_save, sender=Router)
def create_router(sender: Router, instance: Router, created: bool, **kwargs) -> None:
    """ Creates Router object in POSITIONAL_SERVER
    """
    if MODELS_SIGNALS_IS_ACTIVE:

        if ACTIVE:

            if created:
                data = {
                    'command': 'addAnchors',
                    'anchors': [instance.as_router()],
                }

                try:
                    response: Response = requests.post(
                        f'{HOST}:{PORT}', json.dumps(data))

                    if response.status_code == 400:
                        pprint(f'SP server answers ERROR {response.json()}')

                except ConnectionError:
                    print('no create router in SP!')


@receiver(post_save, sender=Router)
def update_router(sender: Router, instance: Router, created: bool, **kwargs) -> None:
    """ Updates Router object in POSITIONAL_SERVER
    """
    if MODELS_SIGNALS_IS_ACTIVE:
        if ACTIVE:
            if not created:
                data = {
                    'command': 'updateAnchors',
                    'anchors': [instance.as_router()],
                }

                try:
                    response: Response = requests.post(
                        f'{HOST}:{PORT}', json.dumps(data))

                    if response.status_code == 400:
                        pprint(f'SP server answers ERROR {response.json()}')

                except ConnectionError:
                    print('no update router in SP!')


@receiver(pre_delete, sender=Router)
def delete_router(sender: Router, instance: Router, **kwargs) -> None:
    """ Deletes Router in POSITIONAL SERVER """
    if MODELS_SIGNALS_IS_ACTIVE:

        if ACTIVE:

            if instance.LoadLandscape:

                data = {
                    'command': 'deleteAnchors',
                    'layerId': instance.LoadLandscape.id,
                    'anchors': [{'id': hex(instance.id)}]
                }

                try:
                    response: Response = requests.post(
                        f'{HOST}:{PORT}', json.dumps(data))

                    if response.status_code == 400:
                        pprint(f'SP server answers ERROR {response.json()}')

                except ConnectionError:
                    print('no delete router in SP!')


class Session(models.Model):
    """ Fully synchronize with SP server!!! Signal methods are written.
    """

    class Meta():

        """ meta props """

        verbose_name = 'Сессия'
        verbose_name_plural = 'Сессии'

    id = models.AutoField(
        primary_key=True,
        verbose_name='идентификатор сессии на сервере позиционирования', unique=True)
    name: str = models.CharField('наименование', max_length=200)
    inuse: bool = models.BooleanField(
        default=True, verbose_name='используется')
    LoadLandscape = models.OneToOneField(
        to=LoadLandscape, on_delete=models.SET_NULL, null=True, blank=True)
    date: datetime.date = models.DateField('Дата создания', auto_now_add=True)

    def __str__(self) -> str: return f'{self.name}'

    def as_tree(self):
        """ tree """
        return {
            'tree': self.LoadLandscape.as_tree(),
        }

    def get_session(self):
        """ i can't remember why """

        return {
            'id': self.id,
            'name': self.name,
            'inuse': self.inuse,
            'idLayer': self.LoadLandscape_id,
        }

    def as_update_session(self, layer=True):
        """ to update session converts to sp """

        buildings = getattr(self.LoadLandscape, 'building_set').all()

        floors = getattr(self.LoadLandscape, 'floor_set').all()

        kabinets = getattr(self.LoadLandscape, 'kabinet_set').filter(
            dae_name__icontains='kabinet')

        result = {
            'command': 'updateSession',
            'id': self.id,
            'session': self.get_session(),
            'layer': self.LoadLandscape.get_layer(),
            'plans': [building.get_plan() for building in buildings] +
            [floor.get_plan() for floor in floors] +
            [kabinet.get_plan() for kabinet in kabinets],
            'plansTree': [building.get_tree() for building in buildings] +
            [floor.get_tree() for floor in floors] +
            [kabinet.get_tree() for kabinet in kabinets],
        }

        if not layer:

            del result['layer']

        return result


@receiver(post_save, sender=Session)
def create_session(sender: Session, instance: Session, created: bool, **kwargs) -> None:
    """ Creates Session object on POSITIONAL_SERVER
    Arguments:
        sender {Session} -- The model class
        instance {Session} -- The actual instance being saved
        created {bool} -- True if a new record was created
    """
    if MODELS_SIGNALS_IS_ACTIVE:
        if created:

            football = Football(
                name=instance.name,
                inuse=instance.inuse,
                plane_x_length=120,
                plane_y_length=90,
                field_x_length=110,
                field_y_length=75)

            landscape = football.sp_objects['plane']
            # IMPORTANT. delete id fix to make creation signal work on LoadLandscape
            landscape.id = None
            landscape.save()

            if ACTIVE:

                data = {
                    'command': 'saveNewSession',
                    'session': {
                        'id': None,
                        'name': instance.name,
                        'idLayer': landscape.id,
                        'inuse': instance.inuse
                    }
                }

                try:
                    response: Response = requests.post(
                        f'{HOST}:{PORT}', json.dumps(data))

                    if response.status_code == 400:
                        pprint(f'SP server answers ERROR {response.json()}')

                    Session.objects.filter(id=instance.id).update(
                        id=response.json()['session']['id'], LoadLandscape=landscape)
                    instance.id = response.json()['session']['id']

                except ConnectionError:
                    print('no create session in SP!')
            else:
                Session.objects.filter(id=instance.id).update(
                    LoadLandscape=landscape)

            instance.LoadLandscape = landscape

            field = football.sp_objects['field']
            field.Session = instance
            # IMPORTANT BUG FIX
            field_id = field.id

            field.save()

            # IMPORTANT BUG FIX
            Plan.objects.filter(id=field_id).update(id=field.id)

            field_vertices = [Vertice(x=v['x'], y=v['y'], Plan=field)
                              for v in football.field['vertices']]
            Vertice.objects.bulk_create(field_vertices)

            side_a = football.sp_objects['side_a']
            side_a.Session = instance

            # IMPORTANT BUG FIX
            side_a_id = side_a.id

            side_a.parent_id = field.id
            side_a.save()

            # IMPORTANT BUG FIX
            Plan.objects.filter(id=side_a_id).update(id=side_a.id)

            side_a_vertices = [Vertice(x=v['x'], y=v['y'], Plan=side_a)
                               for v in football.side_a['vertices']]
            Vertice.objects.bulk_create(side_a_vertices)

            side_b = football.sp_objects['side_b']
            side_b.Session = instance

            # IMPORTANT BUG FIX
            side_b_id = side_b.id

            side_b.parent_id = field.id
            side_b.save()

            # IMPORTANT BUG FIX
            Plan.objects.filter(id=side_b_id).update(id=side_b.id)

            side_b_vertices = [Vertice(x=v['x'], y=v['y'], Plan=side_b)
                               for v in football.side_b['vertices']]
            Vertice.objects.bulk_create(side_b_vertices)

            penalty_a = football.sp_objects['penalty_a']
            penalty_a.Session = instance

            # IMPORTANT BUG FIX
            penalty_a_id = penalty_a.id

            penalty_a.parent = side_a
            penalty_a.save()

            # IMPORTANT BUG FIX
            Plan.objects.filter(id=penalty_a_id).update(id=penalty_a.id)

            penalty_a_vertices = [Vertice(x=v['x'], y=v['y'], Plan=penalty_a)
                                  for v in football.penalty_a['vertices']]
            Vertice.objects.bulk_create(penalty_a_vertices)

            penalty_b = football.sp_objects['penalty_b']
            penalty_b.Session = instance

            # IMPORTANT BUG FIX
            penalty_b_id = penalty_b.id

            penalty_b.parent = side_b
            penalty_b.save()

            # IMPORTANT BUG FIX
            Plan.objects.filter(id=penalty_b_id).update(id=penalty_b.id)

            penalty_b_vertices = [Vertice(x=v['x'], y=v['y'], Plan=penalty_b)
                                  for v in football.penalty_b['vertices']]
            Vertice.objects.bulk_create(penalty_b_vertices)

            gate_a = football.sp_objects['gate_a']
            gate_a.Session = instance

            # IMPORTANT BUG FIX
            gate_a_id = gate_a.id

            gate_a.parent = penalty_a
            gate_a.save()

            # IMPORTANT BUG FIX
            Plan.objects.filter(id=gate_a_id).update(id=gate_a.id)

            gate_a_vertices = [Vertice(x=v['x'], y=v['y'], Plan=gate_a)
                               for v in football.gate_a['vertices']]
            Vertice.objects.bulk_create(gate_a_vertices)

            gate_b = football.sp_objects['gate_b']
            gate_b.Session = instance

            # IMPORTANT BUG FIX
            gate_b_id = gate_b.id

            gate_b.parent = penalty_b
            gate_b.save()

            # IMPORTANT BUG FIX
            Plan.objects.filter(id=gate_b_id).update(id=gate_b.id)

            gate_b_vertices = [Vertice(x=v['x'], y=v['y'], Plan=gate_b)
                               for v in football.gate_b['vertices']]
            Vertice.objects.bulk_create(gate_b_vertices)

            field_center = football.sp_objects['field_center']

            field_center.Session = instance

            # IMPORTANT BUG FIX
            field_center_id = field_center.id

            field_center.parent_id = field.id
            field_center.save()

            # IMPORTANT BUG FIX
            Plan.objects.filter(id=field_center_id).update(id=field_center.id)

            field_center_vertices = [Vertice(
                x=v['x'], y=v['y'], Plan=field_center) for v in football.field_center['vertices']]
            Vertice.objects.bulk_create(field_center_vertices)


@receiver(pre_delete, sender=Session)
def delete_session(sender: Session, instance: Session, **kwargs) -> None:
    """ Deletes Session object on POSITIONAL_SERVER
    Arguments:
        sender {Session} -- Session model
        instance {Session} -- Session object instance
    """
    if MODELS_SIGNALS_IS_ACTIVE:
        if ACTIVE:

            data = {
                'command': 'deleteSession',
                'id': instance.id,
            }

            try:
                response: Response = requests.post(
                    f'{HOST}:{PORT}', json.dumps(data))

                if response.status_code == 400:
                    pprint(f'SP server answers ERROR {response.json()}')

            except ConnectionError:
                pprint('no delete session in SP!')

        instance.LoadLandscape.delete() if instance.LoadLandscape else None


@receiver(post_save, sender=Session)
def update_session(sender: Session, instance: Session, created: bool, **kwargs) -> None:
    """Auto update object in SP to make actualize modifications
    Arguments:
        sender {Session} -- The model class
        instance {Session} -- The actual instance being saved
        created {bool} -- rue if a new record was created
    """
    if MODELS_SIGNALS_IS_ACTIVE:

        if ACTIVE:

            if not created:
                data = {
                    'command': 'updateSession',
                    'id': instance.id,
                    'session': {
                        'id': instance.id,
                        'name': instance.name,
                        'idLayer': instance.LoadLandscape_id,
                        'inuse': instance.inuse,
                    },
                }

                try:
                    response: Response = requests.post(
                        f'{HOST}:{PORT}', json.dumps(data))

                    if response.status_code == 400:
                        pprint(f'SP server answers ERROR {response.json()}')

                except ConnectionError:
                    print('no updated session in SP!')


class Plan(models.Model):

    """ plan object """

    class Meta():
        verbose_name = 'План'
        verbose_name_plural = 'Планы'

    id = models.AutoField(
        primary_key=True,
        verbose_name='Идентификатор плана',
        unique=True,
    )
    name: str = models.CharField(
        'Наименование', max_length=200, null=True, blank=True)
    dae_name: str = models.CharField(
        'dae наименование', max_length=200, db_index=True)
    description: str = models.TextField('Описание', null=True, blank=True)
    minx: float = models.FloatField(
        'Минимальная точка x', default=0, validators=[MinValueValidator(0)])
    miny: float = models.FloatField(
        'Минимальная точка y', default=0, validators=[MinValueValidator(0)])
    minz: float = models.FloatField(
        'Минимальная точка z', default=0, validators=[MinValueValidator(0)])
    maxx: float = models.FloatField(
        'Максимальная точка x', default=0, validators=[MinValueValidator(0)])
    maxy: float = models.FloatField(
        'Максимальная точка y', default=0, validators=[MinValueValidator(0)])
    maxz: float = models.FloatField(
        'Максимальная точка z', default=0, validators=[MinValueValidator(0)])
    zangle: float = models.FloatField('Угол поворота', default=0, validators=[
        MinValueValidator(-3.14), MaxValueValidator(3.14)])
    Session = models.ForeignKey(
        Session, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Сессия')
    level: int = models.SmallIntegerField(default=0)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, related_name='children',
                               verbose_name='родитель для кабинета', null=True, blank=True)

    def __str__(self): return self.name

    def as_tree(self):
        result = {}
        for field in self._meta.fields:
            result[field.name] = getattr(self, field.name)
        return result

    @property
    def vertices(self) -> Iterable[dict]:
        """ Gets vertices of Plan

        Returns:
            list -- Vertices objects
        """

        return getattr(self, 'vertice_set').all().values('x', 'y')

    @property
    def plan(self) -> dict:
        """Autocreates SP object by plan to load in session

        Returns:
            dict -- SP object
        """

        center = SceneBase().get_center(self)

        size_x = SceneBase().get_size(self)['size_x']

        size_y = SceneBase().get_size(self)['size_y']

        parent = getattr(self, 'parent', None)

        return {
            'id': self.id,
            'name': self.dae_name,
            'description': self.description,
            # 'x': center['y'],
            # 'y': self.minz,
            # 'z': center['x'],
            'x': center['x'],
            'y': center['y'],
            'z': self.minz,
            # 'sizeX': size_y,
            # 'sizeY': self.maxz - self.minz,
            # 'sizeZ': size_x,
            'sizeX': size_x,
            'sizeY': size_y,
            'sizeZ': self.maxz - self.minz,
            # 'angleRotateX': 0,
            # 'angleRotateY': self.zangle,
            # 'angleRotateZ': 0,
            'angleRotateX': 0,
            'angleRotateY': 0,
            'angleRotateZ': self.zangle,
            'objType': 2,
            'idParent': getattr(parent, 'id', None),
        }

    def get_plan(self, size_x: float, size_y: float) -> dict:
        """ creates sp plan by size_x and size_y value

        Arguments:
            size_x {float} -- width
            size_y {float} -- height

        Returns:
            dict -- SP plan dict
        """

        center = SceneBase().get_center(self)

        return {
            'id': self.id,
            'name': self.dae_name,
            'description': self.description,
            # 'x': center['y'],
            # 'y': self.minz,
            # 'z': center['x'],
            'x': center['x'],
            'y': center['y'],
            'z': self.minz,
            # 'sizeX': size_y,
            # 'sizeY': self.maxz - self.minz,
            # 'sizeZ': size_x,
            'sizeX': size_x,
            'sizeY': size_y,
            'sizeZ': self.maxz - self.minz,
            # 'angleRotateX': 0,
            # 'angleRotateY': self.zangle,
            # 'angleRotateZ': 0,
            'angleRotateX': 0,
            'angleRotateY': 0,
            'angleRotateZ': self.zangle,
            'objType': 2,
        }

    @property
    def tree(self) -> dict:
        """ Deprecated. Creates SP tree object to load in session

        Returns:
            dict -- SP tree object
        """

        parent_id = self.parent_id

        if self.parent_id:
            parent_id = self.parent_id

        return {
            'id': self.id,
            'idParent': parent_id,
        }

    @staticmethod
    def pre_save_temprorary(obj_id: int, name: str, dae_name: str, length: dict, minz: float, parent_id: int) -> dict:
        """ Builds pre save Plan dict object

        Arguments:
            obj_id {int} -- template id for SP session loading
            name {str} -- name of field
            dae_name {str} -- name for three js dae object if in blender
            length {dict} -- length of field ie {'x': 100, 'y': 45}
            parent_id {int} -- template parent id

        Returns:
            dict -- dict of plan object
        """

        minx = 0
        miny = 0

        height = 5

        return {
            'id': obj_id,
            'name': name,
            'dae_name': dae_name,
            'description': None,
            'minx': minx,
            'miny': miny,
            'minz': minz,
            'maxx': minx + length['x'],
            'maxy': miny + length['y'],
            'maxz': minz + height,
            'zangle': 0,
            'parent_id': parent_id,
        }


@receiver(pre_save, sender=Plan)
def auto_update_level(sender: Plan, instance: Plan, **kwargs) -> None:
    """ auto update level field """

    parent = getattr(instance, 'parent', None)

    if parent:

        instance.level = instance.parent.level + 1

    else:

        instance.level = 0


@receiver(post_save, sender=Plan)
def create_plan(sender: Plan, instance: Plan, created: bool, **kwargs) -> None:
    """creates Plan object on POSITIONAL_SERVER

    Arguments:
        sender {Plan} -- Plan model
        instance {Plan} -- Plan object instance
        created {bool} -- True if a new record was created
    """
    if MODELS_SIGNALS_IS_ACTIVE:

        if ACTIVE:

            if created:

                session = getattr(instance, 'Session', None)

                data = {
                    'command': 'addPlanToSession',
                    'id': getattr(session, 'id', None),
                    'plans': [
                        instance.plan,
                    ],
                }

                try:
                    response: Response = requests.post(
                        f'{HOST}:{PORT}', json.dumps(data))

                    if response.status_code == 400:
                        pprint(f'SP server answers ERROR {response.json()}')

                    instance.id = response.json()['plans'][0]['id']

                except ConnectionError:
                    print('no create plan in SP!')


@receiver(pre_delete, sender=Plan)
def delete_plan(sender: Plan, instance: Plan, **kwargs) -> None:
    """ deletes Plan object from POSITIONAL SERVER

    Arguments:
        sender {Plan} -- Plan model
        instance {Plan} -- Plan instance
    """
    if MODELS_SIGNALS_IS_ACTIVE:
        if ACTIVE:

            data = {
                'command': 'deletePlan',
                'id': instance.id,
            }

            try:
                response: Response = requests.post(
                    f'{HOST}:{PORT}', json.dumps(data))

                if response.status_code == 400:
                    pprint(f'SP server answers ERROR {response.json()}')

            except ConnectionError:
                pprint('no delete plan in SP!')


@receiver(post_save, sender=Plan)
def update_plan(sender: Plan, instance: Plan, created: bool, **kwargs) -> None:
    """ Updates Plan object on POSITIONAL_SERVER

    Arguments:
        sender {Plan} -- Plan model object
        instance {Plan} -- Plan model instance
        created {bool} --  True if a new object was created

    """
    if MODELS_SIGNALS_IS_ACTIVE:

        if ACTIVE:

            if not created:
                data = {
                    'command': 'updatePlans',
                    'plans': [
                        instance.plan,
                    ]
                }

                try:
                    response: Response = requests.post(
                        f'{HOST}:{PORT}', json.dumps(data))

                    if response.status_code == 400:
                        pprint(f'SP server answers ERROR {response.json()}')

                except ConnectionError:
                    print('no updated plan in SP!')


class Vertice(models.Model):
    """ vertices of all plans """
    class Meta():
        verbose_name = 'Вершина'
        verbose_name_plural = 'Вершины'

    x = models.FloatField('Координата X')
    y = models.FloatField('Координата Y')
    Plan = models.ForeignKey(
        Plan, on_delete=models.CASCADE, verbose_name='План')

    def __str__(self) -> str: return f'{self.Plan.name} x:{self.x} y:{self.y}'
