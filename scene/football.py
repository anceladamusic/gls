""" autocreates football field """
import math

from django.apps import apps


class Football():
    """ Autocreates scene objects plans etc.
    Build Temprorary objects (Session, LoadLandscape, Plans).
    Every object must be saved.

    Args:\n
        name - {str} -- Name of Session\n
        inuse - {bool} -- SP prop means session usable\n
        plane_x_length - {float} -- Landscape length by X\n
        plane_y_length - {float} -- Landscape length by Y\n
        field_x_length - {float} -- Field length by X\n
        field_y_length - {float} -- Field lenght by Y\n
        plane_id - {int} -- loadlandscape identifier\n
        field_id - {int} -- building field identifier\n
        side_a_id - {int} -- side_a field identifier\n
        side_b_id - {int} -- side_b field identifier\n
        penalty_a_id - {int} -- penalty_a field identifier\n
        penalty_b_id - {int} -- penalty_b field identifier\n
        gate_a_id - {int} -- gate_b  field identifier\n
        gate_b_id - {int} -- gate_b field identifier\n
        field_center_id - {int} -- field_center identifier\n
    Returns:\n
        object -- Object with params\n
    """

    def __init__(self,
                 name: str,
                 inuse: bool,
                 plane_x_length: float,
                 plane_y_length: float,
                 field_x_length: float,
                 field_y_length: float,
                 plane_id: int = 1,
                 field_id: int = 2,
                 side_a_id: int = 3,
                 side_b_id: int = 4,
                 penalty_a_id: int = 5,
                 penalty_b_id: int = 6,
                 gate_a_id: int = 7,
                 gate_b_id: int = 8,
                 field_center_id: int = 9,
                 ):
        self.name: str = name
        self.inuse: bool = inuse
        self.plane_x_length: float = plane_x_length
        self.plane_y_length: float = plane_y_length
        self.field_x_length: float = field_x_length
        self.field_y_length: float = field_y_length
        self.ids: dict = {
            'plane': plane_id,
            'field': field_id,
            'side_a': side_a_id,
            'side_b': side_b_id,
            'penalty_a': penalty_a_id,
            'penalty_b': penalty_b_id,
            'gate_a': gate_a_id,
            'gate_b': gate_b_id,
            'field_center': field_center_id,
        }

        self.length: dict = {
            'plane': {
                'x': plane_x_length,
                'y': plane_y_length,
            },
            'field': {
                'x': field_x_length,
                'y': field_y_length,
            },
            'side': {
                'x': field_x_length/2,
                'y': field_y_length,
            },
            'penalty': {
                'x': 16.5,
                'y': 40.32,
            },
            'gate': {
                'x': 5.5,
                'y': 18.32,
            },
            'field_center': {
                'x': 18.30,
                'y': 18.30,
            }
        }

        self.vertices: dict = {
            'plane': self.__get_vertices(**{
                'length': self.length['plane'],
            }),
            'field': self.__get_vertices(**{
                'length': self.length['field'],
            }),
            'side': self.__get_vertices(**{
                'length': self.length['side'],
            }),
            'penalty': self.__get_vertices(**{
                'length': self.length['penalty'],
            }),
            'gate': self.__get_vertices(**{
                'length': self.length['gate'],
            }),
            'field_center': self.create_circle(48, self.length['field_center']['x']/2, self.__get_center(self.__get_vertices(**{
                'length': self.length['plane'],
            })),
            )
        }

        self.center: dict = {
            'plane': self.__get_center(self.vertices['plane']),
            'field': self.__get_center(self.vertices['field']),
            'side': self.__get_center(self.vertices['side']),
            'penalty': self.__get_center(self.vertices['penalty']),
            'gate': self.__get_center(self.vertices['gate']),
            'field_center': self.__get_center(self.vertices['field_center']),
        }

        self.offset: dict = {
            'plane': {
                'x': 0,
                'y': 0,
            },
            'field': {
                'x': self.center['plane']['x'] - self.center['field']['x'],
                'y': self.center['plane']['y'] - self.center['field']['y'],
            },
            'side_a': {
                'x': self.center['plane']['x'] - self.center['field']['x'],
                'y': self.center['plane']['y'] - self.center['field']['y'],
            },
            'side_b': {
                'x': (self.center['plane']['x'] - self.center['field']['x']) + self.length['side']['x'],
                'y': self.center['plane']['y'] - self.center['field']['y'],
            },
            'penalty_a': {
                'x': self.center['plane']['x'] - self.center['field']['x'],
                'y': (self.center['plane']['y'] - self.center['field']['y']) +
                (self.center['side']['y'] - self.center['penalty']['y']),
            },
            'penalty_b': {
                'x': (self.center['plane']['x'] - self.center['field']['x']) + self.length['field']['x']
                - self.length['penalty']['x'],
                'y': (self.center['plane']['y'] - self.center['field']['y']) +
                (self.center['side']['y'] - self.center['penalty']['y']),
            },
            'gate_a': {
                'x': self.center['plane']['x'] - self.center['field']['x'],
                'y': (self.center['penalty']['y'] - self.center['gate']['y']) +
                (self.center['plane']['y'] - self.center['field']['y']) +
                (self.center['side']['y'] - self.center['penalty']['y']),
            },
            'gate_b': {
                'x': (self.center['plane']['x'] - self.center['field']['x']) + self.length['field']['x']
                - self.length['gate']['x'],
                'y': (self.center['penalty']['y'] - self.center['gate']['y']) +
                (self.center['plane']['y'] - self.center['field']['y']) +
                (self.center['side']['y'] - self.center['penalty']['y']),
            },
            'field_center': {
                'x': self.center['plane']['x'] - self.center['field_center']['x'],
                'y': self.center['plane']['y'] - self.center['field_center']['y'],
            },
        }

        # вершины и центр для объектов поля
        self.plane = {
            'id': self.ids['plane'],
            'length': self.length['plane'],
            'vertices': self.__get_vertices(**{
                'length': self.length['plane'],
                'offset': self.offset['plane'],
            }),
        }
        self.plane['center'] = self.__get_center(self.plane['vertices'])

        # field
        self.field = {
            'id': self.ids['field'],
            'length': self.length['field'],
            'vertices': self.__get_vertices(**{
                'length': self.length['field'],
                'offset': self.offset['field'],
            }),
            'vertices_table': apps.get_model('scene', 'vertice'),
        }
        self.field['center'] = self.__get_center(self.field['vertices'])

        # side a
        self.side_a = {
            'id': self.ids['side_a'],
            'length': self.length['side'],
            'vertices': self.__get_vertices(**{
                'length': self.length['side'],
                'offset': self.offset['side_a'],
            }),
            'vertices_table': apps.get_model('scene', 'vertice'),
        }
        self.side_a['center'] = self.__get_center(self.side_a['vertices'])

        # side b
        self.side_b = {
            'id': self.ids['side_b'],
            'length': self.length['side'],
            'vertices': self.__get_vertices(**{
                'length': self.length['side'],
                'offset': self.offset['side_b'],
            }),
            'vertices_table': apps.get_model('scene', 'vertice'),
        }
        self.side_b['center'] = self.__get_center(self.side_b['vertices'])

        # penalty a
        self.penalty_a = {
            'id': self.ids['penalty_a'],
            'length': self.length['penalty'],
            'vertices': self.__get_vertices(**{
                'length': self.length['penalty'],
                'offset': self.offset['penalty_a'],
            }),
            'vertices_table': apps.get_model('scene', 'vertice'),
        }
        self.penalty_a['center'] = self.__get_center(
            self.penalty_a['vertices'])

        # penalty b
        self.penalty_b = {
            'id': self.ids['penalty_b'],
            'length': self.length['penalty'],
            'vertices': self.__get_vertices(**{
                'length': self.length['penalty'],
                'offset': self.offset['penalty_b'],
            }),
            'vertices_table': apps.get_model('scene', 'vertice'),
        }
        self.penalty_b['center'] = self.__get_center(
            self.penalty_b['vertices'])

        # gate a
        self.gate_a = {
            'id': self.ids['gate_a'],
            'length': self.length['gate'],
            'vertices': self.__get_vertices(**{
                'length': self.length['gate'],
                'offset': self.offset['gate_a'],
            }),
            'vertices_table': apps.get_model('scene', 'vertice'),
        }
        self.gate_a['center'] = self.__get_center(self.gate_a['vertices'])

        # gate b
        self.gate_b = {
            'id': self.ids['gate_b'],
            'length': self.length['gate'],
            'vertices': self.__get_vertices(**{
                'length': self.length['gate'],
                'offset': self.offset['gate_b'],
            }),
            'vertices_table': apps.get_model('scene', 'vertice'),
        }
        self.gate_b['center'] = self.__get_center(self.gate_b['vertices'])

        # field center
        self.field_center = {
            'id': self.ids['field_center'],
            'length': self.length['field_center'],
            'vertices': self.create_circle(48, self.length['field_center']['x']/2, self.__get_center(self.vertices['plane'])),
            'vertices_table': apps.get_model('scene', 'vertice'),
        }
        self.field_center['center'] = self.__get_center(
            self.field_center['vertices'])

        self.figures = {
            'plane': self.plane,
            'field': self.field,
            'side_a': self.side_a,
            'side_b': self.side_b,
            'penalty_a': self.penalty_a,
            'penalty_b': self.penalty_b,
            'gate_a': self.gate_a,
            'gate_b': self.gate_b,
            'field_center': self.field_center,
        }
        self.sp_objects: dict = {
            'plane': apps.get_model('scene', 'loadlandscape')(**apps.get_model('scene', 'loadlandscape').as_football_plane(**{
                'obj_id': self.figures['plane']['id'],
                'name': f'Подложка для {self.name}',
                'length': self.figures['plane']['length'],
            })),
            'session': apps.get_model('scene', 'session')(**{
                'inuse': self.inuse,
                'name': self.name,
            }),
            'field': apps.get_model('scene', 'plan')(**apps.get_model('scene', 'plan')().pre_save_temprorary(**{
                'obj_id': self.figures['field']['id'],
                'name': 'Тестовое футбольное поле',
                'dae_name': 'building_001',
                'length': self.figures['field']['length'],
                'minz': 0.02,
                'parent_id': None,
            })),
            'side_a': apps.get_model('scene', 'plan')(**apps.get_model('scene', 'plan')().pre_save_temprorary(**{
                'obj_id': self.figures['side_a']['id'],
                'name': 'Сторона поля А',
                'dae_name': 'floor_001',
                'length': self.figures['side_a']['length'],
                'minz': 0.04,
                'parent_id': None,
                # 'parent_id': self.sp_objects['field'].id,
            })),
            'side_b': apps.get_model('scene', 'plan')(**apps.get_model('scene', 'plan')().pre_save_temprorary(**{
                'obj_id': self.figures['side_b']['id'],
                'name': 'Сторона поля Б',
                'dae_name': 'floor_002',
                'length': self.figures['side_b']['length'],
                'minz': 0.04,
                'parent_id': None,
                # 'parent_id': self.sp_objects['field'].id,
            })),
            'penalty_a': apps.get_model('scene', 'plan')(**apps.get_model('scene', 'plan')().pre_save_temprorary(**{
                'obj_id': self.figures['penalty_a']['id'],
                'name': 'Штрафная стороны A',
                'dae_name': 'kabinet_001',
                'length': self.figures['penalty_a']['length'],
                'minz': 0.06,
                'parent_id': None,
                # 'parent_id': self.sp_objects['side_a'].id,
            })),
            'penalty_b': apps.get_model('scene', 'plan')(**apps.get_model('scene', 'plan')().pre_save_temprorary(**{
                'obj_id': self.figures['penalty_b']['id'],
                'name': 'Штрафная стороны Б',
                'dae_name': 'kabinet_002',
                'length': self.figures['penalty_b']['length'],
                'minz': 0.06,
                'parent_id': None,
                # 'parent_id': self.sp_objects['side_b'].id,
            })),
            'gate_a': apps.get_model('scene', 'plan')(**apps.get_model('scene', 'plan')().pre_save_temprorary(**{
                'obj_id': self.figures['gate_a']['id'],
                'name': 'Площадь ворот стороны А',
                'dae_name': 'kabinet_003',
                'length': self.figures['gate_a']['length'],
                'minz': 0.08,
                'parent_id': None,
                # 'parent_id': self.sp_objects['side_a'].id,
            })),
            'gate_b': apps.get_model('scene', 'plan')(**apps.get_model('scene', 'plan')().pre_save_temprorary(**{
                'obj_id': self.figures['gate_b']['id'],
                'name': 'Площадь ворот стороны Б',
                'dae_name': 'kabinet_004',
                'length': self.figures['gate_b']['length'],
                'minz': 0.08,
                'parent_id': None,
                # 'parent_id': self.sp_objects['side_b'].id,
            })),
            'field_center': apps.get_model('scene', 'plan')(**apps.get_model('scene', 'plan')().pre_save_temprorary(**{
                'obj_id': self.figures['field_center']['id'],
                'name': 'Центр поля',
                'dae_name': 'floor_003',
                'length': self.figures['field_center']['length'],
                'minz': 0.09,
                'parent_id': None,
                # 'parent_id': self.sp_objects['field'].id,
            })),
        }
        """ Depricated """
        # self.plans = [
        #     self.sp_objects['field'].get_plan(
        #         self.length['field']['x'], self.length['field']['y']),
        #     self.sp_objects['side_a'].get_plan(
        #         self.length['side']['x'], self.length['side']['y']),
        #     self.sp_objects['side_b'].get_plan(
        #         self.length['side']['x'], self.length['side']['y']),
        #     self.sp_objects['penalty_a'].get_plan(
        #         self.length['penalty']['x'], self.length['penalty']['y']),
        #     self.sp_objects['penalty_b'].get_plan(
        #         self.length['penalty']['x'], self.length['penalty']['y']),
        #     self.sp_objects['gate_a'].get_plan(
        #         self.length['gate']['x'], self.length['gate']['y']),
        #     self.sp_objects['gate_b'].get_plan(
        #         self.length['gate']['x'], self.length['gate']['y']),
        #     self.sp_objects['field_center'].get_plan(
        #         self.length['field_center']['x'], self.length['field_center']['y']),
        # ]

        # self.tree = [
        #     self.sp_objects['field'].tree,
        #     self.sp_objects['side_a'].tree,
        #     self.sp_objects['side_b'].tree,
        #     self.sp_objects['penalty_a'].tree,
        #     self.sp_objects['penalty_b'].tree,
        #     self.sp_objects['gate_a'].tree,
        #     self.sp_objects['gate_b'].tree,
        #     self.sp_objects['field_center'].tree,
        # ]

    def __get_vertices(self, length, offset={'x': 0, 'y': 0}):

        return [
            {
                'x': 0+offset['x'],
                'y': 0+offset['y'],
            },
            {
                'x': 0+offset['x'],
                'y': length['y']+offset['y'],
            },
            {
                'x': length['x']+offset['x'],
                'y': length['y']+offset['y'],
            },
            {
                'x': length['x']+offset['x'],
                'y': 0+offset['y'],
            },
        ]

    def __get_center(self, vertices):

        return {
            'x': sum([v['x'] for v in vertices])/len(vertices),
            'y': sum([v['y'] for v in vertices])/len(vertices),
        }

    def create_circle(self, number, radius, center):

        vertices = []

        for i in range(0, number):

            angle = i/number*(math.pi*2)

            vertices.append({
                'x': center['x'] + math.cos(angle) * radius,
                'y': center['y'] + math.sin(angle) * radius,
            })

        return vertices
