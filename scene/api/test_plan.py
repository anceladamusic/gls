""" test plan object create, update, delete and SP synchronization """
import json
import ipdb
import requests

from requests.exceptions import ConnectionError
from pprint import pprint
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User
from django.conf import settings

from scene.models import (
    Session,
    Plan,
    LoadLandscape,
)

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER

HOST = settings.POSITIONAL_SERVER_HOST
PORT = settings.POSITIONAL_SERVER_PORT


class PlanAPITestCase(APITestCase):
    """ test Plan model """

    def setUp(self):
        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        Session.objects.create(**{
            'name': 'Сцена',
            'inuse': True,
        })

        Session.objects.create(**{
            'name': 'Сцена 2',
            'inuse': True,
        })

    def tearDown(self):
        Session.objects.all().delete()
        # try:
        #     plans = requests.post(f'{HOST}:{PORT}', data=json.dumps(
        #         {'command': 'listPlans'}))
        #     pprint(plans.json())
        # except ConnectionError:
        #     pprint('sp server not answer')

    def test_not_login(self):
        """ check unaviable if unauthorized """

        url = api_reverse('api-scene:plan-list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ test list if authorized """

        url = api_reverse('api-scene:plan-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_concrete_plan(self):
        """ test get one concrete plan """

        url = api_reverse('api-scene:plan-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(
            f'{url}{Plan.objects.first().id}/', {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_multiple_posts(self):
        """ test multiple create """

        url = api_reverse('api-scene:plan-list')

        data = [{
            'name': 'План 3',
            'dae_name': 'dae_name3',
            'description': '',
            'minx': 0,
            'miny': 0,
            'minz': 0,
            'maxx': 0,
            'maxy': 0,
            'maxz': 0,
            'zangle': 0,
            'parent': Plan.objects.first().id,
            'Session': Session.objects.last().id,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_one(self):
        """ test create """

        url = api_reverse('api-scene:plan-list')

        data = {
            'name': 'План 3',
            'dae_name': 'dae_name3',
            'description': '',
            'minx': 0,
            'miny': 0,
            'minz': 0,
            'maxx': 0,
            'maxy': 0,
            'maxz': 0,
            'zangle': 0,
            'parent': Plan.objects.first().id,
            'Session': Session.objects.last().id,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_patch_one(self):
        """ updates one """

        url = api_reverse('api-scene:plan-detail', kwargs={
            'id': Plan.objects.first().id,
        })

        data = {
            'name': 'План 3',
            'dae_name': 'dae_name3',
            'description': '',
            'minx': 0,
            'miny': 0,
            'minz': 0,
            'maxx': 0,
            'maxy': 0,
            'maxz': 0,
            'zangle': 0,
            'parent': Plan.objects.first().parent,
            'Session': Plan.objects.first().Session.id,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_multiple(self):
        """ deletes multiple """

        url = api_reverse('api-scene:plan-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = [{
            'id': Plan.objects.first().id,
        }]

        response = self.client.delete(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
