""" test update football """
import ipdb
import json
from pprint import pprint

from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User
from django.conf import settings

from scene.models import (
    Session,
    LoadLandscape,
)

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class UpdateFootballAPITestCase(APITestCase):

    """ test update football """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        Session.objects.create(
            name='some name',
            inuse=True,
        )

    def tearDown(self):

        Session.objects.all().delete()

    def test_not_login(self):
        """ test when user isn't auth """

        url = api_reverse('update_football')

        response = self.client.post(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login_wrong_params(self):
        """ if login with wrong params """

        url = api_reverse('update_football')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = {
            'some_param': 123,
        }

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login_with_params(self):

        url = api_reverse('update_football')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        session: Session = Session.objects.first()

        data = {
            'session_id': session.id,
            'plane_x_length': 80,
            'plane_y_length': 40,
            'offset': 1.0,
        }

        response: Response = self.client.patch(
            url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
