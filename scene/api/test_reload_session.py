""" test reload session """
import ipdb
import json
from pprint import pprint

from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User
from django.conf import settings

from scene.models import (
    Session,
    LoadLandscape,
)

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class ReloadSessionAPITestCase(APITestCase):

    """ test reload session """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        Session.objects.create(
            name='some name',
            inuse=True,
        )

    def tearDown(self):

        Session.objects.all().delete()

    def test_not_login(self):
        """ test when user isn't auth """

        url = api_reverse('reload_session')

        response = self.client.post(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_wrong_request(self):
        """ if login with wrong request type"""

        url = api_reverse('reload_session')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, {}, format='json')

        self.assertEqual(response.status_code,
                         status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_login(self):

        url = api_reverse('reload_session')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response: Response = self.client.post(
            url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
