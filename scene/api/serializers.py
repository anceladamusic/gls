""" scene models serializers """
import ipdb

from typing import Iterable
from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
)
from rest_framework_bulk import (BulkSerializerMixin, BulkListSerializer)

from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist

from mainapp.models import File
from scene.base import Base as SceneBase
from scene.models import (
    Session,
    LoadLandscape,
    Vertice,
    Plan,
    Router,
)


class RouterSerializer(BulkSerializerMixin, ModelSerializer):
    """ router object """

    class Meta:
        model = Router
        list_serializer_class = BulkListSerializer
        fields = [field.name for field in Router._meta.fields]
        update_lookup_field = 'id'


class VerticeSerializer(BulkSerializerMixin, ModelSerializer):
    """ vertice object """

    class Meta:
        model = Vertice
        list_serializer_class = BulkListSerializer
        fields = [field.name for field in Vertice._meta.fields]
        read_only_fields = ['id']
        update_lookup_field = 'id'


class SubPlan(ModelSerializer):
    """ nested plan """

    class Meta:
        model = Plan

    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data


class PlanSerializer(BulkSerializerMixin, ModelSerializer):
    """ plan """

    children = SubPlan(many=True, required=False, read_only=True)
    vertice_set = VerticeSerializer(many=True, required=False, read_only=True)

    class Meta:
        model = Plan
        list_serializer_class = BulkListSerializer
        fields = [field.name for field in Plan._meta.fields] + \
            ['children', 'vertice_set', 'plan']
        read_only_fields = ['id']
        update_lookup_field = 'id'


class LandscapeSerializer(BulkSerializerMixin, ModelSerializer):
    """ LoadLandscape """

    class Meta:

        model = LoadLandscape
        list_serializer_class = BulkListSerializer
        fields = '__all__'


class SessionSerializer(BulkSerializerMixin, ModelSerializer):
    """ Session """

    # LoadLandscape = LandscapeSerializer(many=False, required=False)
    plans = SerializerMethodField(required=False)

    @staticmethod
    def get_plans(session):
        plans: Iterable[Plan] = session.plan_set.filter(parent=None)
        if plans:
            return PlanSerializer(instance=plans[0], many=False).data

    class Meta:
        model = Session
        list_serializer_class = BulkListSerializer
        fields = [field.name for field in Session._meta.fields] + \
            ['plans']
        read_only_fields = ['id']
        update_lookup_field = 'id'
