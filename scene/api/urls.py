
from rest_framework_bulk.routes import BulkRouter

from django.urls import path
from django.conf.urls import include

from scene.api.views import (
    SessionViewSet,
    LandscapeViewSet,
    VerticeViewSet,
    PlanViewSet,
    RouterViewSet,
    update_football,
    reload_session,
)

router = BulkRouter()
router.register(r'landscape', LandscapeViewSet, 'landscape')
router.register(r'vertice', VerticeViewSet, 'vertice')
router.register(r'plan', PlanViewSet, 'plan')
router.register(r'session', SessionViewSet, 'session')
router.register(r'router', RouterViewSet, 'router')

urlpatterns = [
    path('api-scene/', include((router.urls, 'api-scene'), namespace='api-scene')),
    path('api-scene/update_football/', update_football, name='update_football'),
    path('api-scene/reload_session/', reload_session, name='reload_session'),
]
