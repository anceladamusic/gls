
""" test LoadLandscape object create, update, delete and SP synchronization """
import ipdb
import json
import requests
from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User
from django.conf import settings

from scene.models import (
    LoadLandscape,
)

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER

HOST = settings.POSITIONAL_SERVER_HOST
PORT = settings.POSITIONAL_SERVER_PORT


class LandscapeAPITestCase(APITestCase):

    """ test user login """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')

        user.set_password('PDVAncel115648')

        user.save()

        # authorization token
        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        self.landscape = LoadLandscape.objects.create(**{
            "name": "сцена для тестирования взаимодействия ws-sp",
            "camera_position_x": 70.0,
            "camera_position_y": -80.0,
            "camera_position_z": 50.0,
            "camera_up_x": 0.0,
            "camera_up_y": 0.0,
            "camera_up_z": 1.0,
            "controls_target_x": 67.0,
            "controls_target_y": 40.0,
            "controls_target_z": 0.0,
            "dae_rotation_x": 3.14159265359,
            "dae_rotation_y": 3.14159265359,
            "dae_rotation_z": 3.14159265359,
            "dae_position_x": 76.639468457,
            "dae_position_y": 74.03660896,
            "dae_position_z": 6.0,
            "minx": 0.0,
            "miny": 0.0,
            "minz": 0.0,
            "maxx": 149.998046875,
            "maxy": 149.998046875,
            "maxz": 0.0,
        })

    def tearDown(self):

        LoadLandscape.objects.all().delete()

        # try:
        #     layers = requests.post(f'{HOST}:{PORT}', data=json.dumps(
        #         {'command': 'getListLayers'}))
        #     pprint(layers.json())

        # except ConnectionError:
        #     pprint('sp server not answer')

    def test_not_auth(self):
        """ if not loged in """

        url = api_reverse('api-scene:landscape-list')

        data = {}

        response = self.client.get(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_logedin(self):
        """ if user loged in """

        url = api_reverse('api-scene:landscape-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_multiple_post(self):
        """ if post multiple new objects """

        url = api_reverse('api-scene:landscape-list')

        data = [{
            "name": "сцена 3",
            "camera_position_x": 70.0,
            "camera_position_y": -80.0,
            "camera_position_z": 50.0,
            "camera_up_x": 0.0,
            "camera_up_y": 0.0,
            "camera_up_z": 1.0,
            "controls_target_x": 67.0,
            "controls_target_y": 40.0,
            "controls_target_z": 0.0,
            "dae_rotation_x": 3.14159265359,
            "dae_rotation_y": 3.14159265359,
            "dae_rotation_z": 3.14159265359,
            "dae_position_x": 76.639468457,
            "dae_position_y": 74.03660896,
            "dae_position_z": 6.0,
            "minx": 0.0,
            "miny": 0.0,
            "minz": 0.0,
            "maxx": 149.998046875,
            "maxy": 149.998046875,
            "maxz": 0.0,
        }, {
            "name": "сцена 4",
            "camera_position_x": 70.0,
            "camera_position_y": -80.0,
            "camera_position_z": 50.0,
            "camera_up_x": 0.0,
            "camera_up_y": 0.0,
            "camera_up_z": 1.0,
            "controls_target_x": 67.0,
            "controls_target_y": 40.0,
            "controls_target_z": 0.0,
            "dae_rotation_x": 3.14159265359,
            "dae_rotation_y": 3.14159265359,
            "dae_rotation_z": 3.14159265359,
            "dae_position_x": 76.639468457,
            "dae_position_y": 74.03660896,
            "dae_position_z": 6.0,
            "minx": 0.0,
            "miny": 0.0,
            "minz": 0.0,
            "maxx": 149.998046875,
            "maxy": 149.998046875,
            "maxz": 0.0,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post(self):
        """ if post new object """

        url = api_reverse('api-scene:landscape-list')

        data = {
            "name": "сцена 3",
            "camera_position_x": 70.0,
            "camera_position_y": -80.0,
            "camera_position_z": 50.0,
            "camera_up_x": 0.0,
            "camera_up_y": 0.0,
            "camera_up_z": 1.0,
            "controls_target_x": 67.0,
            "controls_target_y": 40.0,
            "controls_target_z": 0.0,
            "dae_rotation_x": 3.14159265359,
            "dae_rotation_y": 3.14159265359,
            "dae_rotation_z": 3.14159265359,
            "dae_position_x": 76.639468457,
            "dae_position_y": 74.03660896,
            "dae_position_z": 6.0,
            "minx": 0.0,
            "miny": 0.0,
            "minz": 0.0,
            "maxx": 149.998046875,
            "maxy": 149.998046875,
            "maxz": 0.0,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_patch(self):
        """ if patch new object """

        url = api_reverse('api-scene:landscape-detail',
                          kwargs={'id': self.landscape.id})

        data = {
            "name": "сцена измененная",
            "camera_position_x": 70.0,
            "camera_position_y": -80.0,
            "camera_position_z": 50.0,
            "camera_up_x": 0.0,
            "camera_up_y": 0.0,
            "camera_up_z": 1.0,
            "controls_target_x": 67.0,
            "controls_target_y": 40.0,
            "controls_target_z": 0.0,
            "dae_rotation_x": 3.14159265359,
            "dae_rotation_y": 3.14159265359,
            "dae_rotation_z": 3.14159265359,
            "dae_position_x": 76.639468457,
            "dae_position_y": 74.03660896,
            "dae_position_z": 6.0,
            "minx": 0.0,
            "miny": 0.0,
            "minz": 0.0,
            "maxx": 149.998046875,
            "maxy": 149.998046875,
            "maxz": 0.0,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_patch_multiple(self):
        """ if patch multiple objects """

        url = api_reverse('api-scene:landscape-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = [{
            "id": LoadLandscape.objects.first().id,
            "name": "измененная множественным запросом",
            "camera_position_x": 70.0,
            "camera_position_y": -80.0,
            "camera_position_z": 50.0,
            "camera_up_x": 0.0,
            "camera_up_y": 0.0,
            "camera_up_z": 1.0,
            "controls_target_x": 67.0,
            "controls_target_y": 40.0,
            "controls_target_z": 0.0,
            "dae_rotation_x": 3.14159265359,
            "dae_rotation_y": 3.14159265359,
            "dae_rotation_z": 3.14159265359,
            "dae_position_x": 76.639468457,
            "dae_position_y": 74.03660896,
            "dae_position_z": 6.0,
            "minx": 0.0,
            "miny": 0.0,
            "minz": 0.0,
            "maxx": 149.998046875,
            "maxy": 149.998046875,
            "maxz": 0.0,
        }]

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_multiple(self):
        """ if delete multiple objects """

        url = api_reverse('api-scene:landscape-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = [{
            "id": LoadLandscape.objects.first().id,
        }]

        response = self.client.delete(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        self.assertEqual(LoadLandscape.objects.all().count(), 0)
