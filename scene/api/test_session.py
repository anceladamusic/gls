""" test session object create, update, delete and SP synchronization """
import json
import ipdb
import requests

from requests.exceptions import ConnectionError
from pprint import pprint
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User
from django.conf import settings

from scene.models import (
    Session,
    LoadLandscape,
    Plan,
)

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER

HOST: str = settings.POSITIONAL_SERVER_HOST
PORT: int = settings.POSITIONAL_SERVER_PORT
ACTIVE: bool = settings.POSITIONAL_SERVER_IS_ACTIVE


class SessionAPITestCase(APITestCase):
    """ test session model """

    def setUp(self):
        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        self.session = Session.objects.create(**{
            'name': 'Сцена',
            'inuse': True,
        })

        Session.objects.create(**{
            'name': 'Сцена 2',
            'inuse': True,
        })

    def tearDown(self):
        Session.objects.all().delete()
        # try:
        #     sessions = requests.post(f'{HOST}:{PORT}', data=json.dumps(
        #         {'command': 'getListSessions'}))
        #     pprint(sessions.json())
        # except ConnectionError:
        #     pprint('sp server not answer')

    def test_not_login(self):
        """ check unaviable if unauthorized """

        url = api_reverse('api-scene:session-list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ test list if authorized """

        url = api_reverse('api-scene:session-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_detail(self):
        """ test get detail """

        url = api_reverse('api-scene:session-detail', kwargs={
            'id': Session.objects.first().id,
        })

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_multiple_posts(self):
        """ test multiple create """

        url = api_reverse('api-scene:session-list')

        data = [{
            'name': 'Сцена 3',
            'inuse': True,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        if ACTIVE:

            self.assertEqual(Plan.objects.all().count(), 8 * 3)

    def test_patch_one(self):
        """ updates one """

        url = api_reverse('api-scene:session-detail', kwargs={
            'id': Session.objects.first().id,
        })

        data = {
            'name': 'Измененная сцена',
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        if ACTIVE:

            self.assertEqual(Plan.objects.all().count(), 8 * 2)

    def test_delete_multiple(self):
        """ deletes multiple """

        url = api_reverse('api-scene:session-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = [{
            'id': Session.objects.first().id,
        }]

        response = self.client.delete(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        self.assertEqual(Session.objects.all().count(), 1)

        if ACTIVE:
            self.assertEqual(Plan.objects.all().count(), 8 * 1)

            self.assertEqual(LoadLandscape.objects.all().count(), 1)
