""" api views of scene objects """
import ipdb
import json
import requests

from pprint import pprint
from schema import (
    Schema,
    SchemaError,
    Optional,
    Use,
)
from rest_framework_bulk import BulkModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.decorators import (
    api_view,
    permission_classes,
)
from rest_framework.response import Response
from rest_framework.request import Request

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

from mainapp.rest_bulk_destroyer import RestBulkDestroyer
from .serializers import (
    LandscapeSerializer,
    VerticeSerializer,
    PlanSerializer,
    SessionSerializer,
    VerticeSerializer,
    RouterSerializer,
)
from scene.models import (
    LoadLandscape,
    Vertice,
    Plan,
    Session,
    Router,
)
from scene.football import Football

HOST = settings.POSITIONAL_SERVER_HOST
PORT = settings.POSITIONAL_SERVER_PORT
POSITIONAL_SERVER_IS_ACTIVE: bool = settings.POSITIONAL_SERVER_IS_ACTIVE


class RouterViewSet(RestBulkDestroyer, BulkModelViewSet):
    """ Serves Router model object

    """

    lookup_field = 'id'
    serializer_class = RouterSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        if getattr(self.request.user, 'is_authenticated'):
            landscape = self.request.query_params.get('landscape', None)
            if landscape:
                return Router.objects.filter(LoadLandscape_id=landscape)
            return Router.objects.all()

    def perform_destroy(self, serializer):
        objects = json.loads(self.request.body)
        Router.objects.filter(id__in=(obj['id'] for obj in objects)).delete()
        return objects


class SessionViewSet(RestBulkDestroyer, BulkModelViewSet):
    """ Serves Session model object

    Arguments:
        RestBulkDestroyer {Class} -- delete object
        BulkModelViewSet {Class} -- inherit framework view
    """

    lookup_field = 'id'
    serializer_class = SessionSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        if getattr(self.request.user, 'is_authenticated'):
            return Session.objects.all()

    def perform_destroy(self, serializer):
        objects = json.loads(self.request.body)
        Session.objects.filter(id__in=(obj['id'] for obj in objects)).delete()
        return objects


class LandscapeViewSet(RestBulkDestroyer, BulkModelViewSet):

    """
    Landscape
    """

    lookup_field = 'id'
    serializer_class = LandscapeSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        if getattr(self.request.user, 'is_authenticated'):
            return LoadLandscape.objects.all()

    def perform_destroy(self, serializer):
        objects = json.loads(self.request.body)
        LoadLandscape.objects.filter(
            id__in=(obj['id'] for obj in objects)).delete()
        return objects


class VerticeViewSet(RestBulkDestroyer, BulkModelViewSet):

    lookup_field = 'id'
    serializer_class = VerticeSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        return Vertice.objects.all()

    def perform_destroy(self, serializer):
        objects = json.loads(self.request.body)
        Vertice.objects.filter(
            id__in=(obj['id'] for obj in objects)).delete()
        return objects


class PlanViewSet(RestBulkDestroyer, BulkModelViewSet):

    """
    Plan
    """

    lookup_field = 'id'
    serializer_class = PlanSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        if getattr(self.request.user, 'is_authenticated'):
            plan_id = self.request.GET.get('plan_id', None)
            if plan_id:
                return Plan.objects.filter(id=plan_id)
            return Plan.objects.all()

    def perform_destroy(self, serializer):
        objects = json.loads(self.request.body)
        Plan.objects.filter(
            id__in=(obj['id'] for obj in objects)).delete()
        return objects


@api_view(['PATCH'])
@permission_classes([IsAuthenticated])
def update_football(request: Request) -> Response:
    """ Updates football field

    Arguments:
        [dict] -- dict with params ids loadlandscape, plane

    Returns:
        Response -- [description]
    """

    params: dict = json.loads(json.dumps(request.data))

    schema = Schema({
        'session_id': Use(int),
        'plane_x_length': Use(float),
        'plane_y_length': Use(float),
        'offset': Use(float),
    })

    error: dict = {}

    try:
        schema.validate(params)
    except SchemaError as e:
        error = e.__dict__['autos']

    if error:
        return Response(error, 400)

    try:
        session: Session = Session.objects.get(id=params['session_id'])
    except ObjectDoesNotExist:
        Response(data={'session_id': [
                 'Сессия с данным идентификатором не существует']}, status=status.HTTP_400_BAD_REQUEST)

    offset: float = 1.0
    if 'offset' in params:
        offset = params['offset']
        params.pop('offset', None)

    params['field_x_length'] = params['plane_x_length'] - 2*offset
    params['field_y_length'] = params['plane_y_length'] - 2*offset

    params['name'] = session.name
    params['inuse'] = session.inuse

    layer: LoadLandscape = LoadLandscape.objects.get(
        id=session.LoadLandscape.id)
    params['plane_id'] = layer.id

    field: Plan = Plan.objects.get(
        Session_id=params['session_id'], dae_name='building_001')
    params['field_id'] = field.id

    side_a: Plan = Plan.objects.get(
        Session_id=params['session_id'], dae_name='floor_001')
    params['side_a_id'] = side_a.id

    side_b: Plan = Plan.objects.get(
        Session_id=params['session_id'], dae_name='floor_002')
    params['side_b_id'] = side_b.id

    penalty_a: Plan = Plan.objects.get(
        Session_id=params['session_id'], dae_name='kabinet_001')
    params['penalty_a_id'] = penalty_a.id

    penalty_b: Plan = Plan.objects.get(
        Session_id=params['session_id'], dae_name='kabinet_002')
    params['penalty_b_id'] = penalty_b.id

    gate_a: Plan = Plan.objects.get(
        Session_id=params['session_id'], dae_name='kabinet_003')
    params['gate_a_id'] = gate_a.id

    gate_b: Plan = Plan.objects.get(
        Session_id=params['session_id'], dae_name='kabinet_004')
    params['gate_b_id'] = gate_b.id

    field_center: Plan = Plan.objects.get(
        Session_id=params['session_id'], dae_name='floor_003')
    params['field_center_id'] = field_center.id

    params.pop('session_id', None)

    football = Football(**params)

    # update landscape
    for key, value in football.sp_objects['plane'].__dict__.items():
        setattr(layer, key, value)
    layer.save()

    # update field
    for key, value in football.sp_objects['field'].__dict__.items():
        setattr(field, key, value)
    field.Session = session
    field.save()

    Vertice.objects.filter(Plan=field).delete()
    field_vertices = [Vertice(x=v['x'], y=v['y'], Plan=field)
                      for v in football.field['vertices']]
    Vertice.objects.bulk_create(field_vertices)

    # update side_a
    for key, value in football.sp_objects['side_a'].__dict__.items():
        setattr(side_a, key, value)
    side_a.Session = session
    side_a.parent = field
    side_a.save()

    Vertice.objects.filter(Plan=side_a).delete()
    side_a_vertices = [Vertice(x=v['x'], y=v['y'], Plan=side_a)
                       for v in football.side_a['vertices']]
    Vertice.objects.bulk_create(side_a_vertices)

    # update side_b
    for key, value in football.sp_objects['side_b'].__dict__.items():
        setattr(side_b, key, value)
    side_b.Session = session
    side_b.parent = field
    side_b.save()

    Vertice.objects.filter(Plan=side_b).delete()
    side_b_vertices = [Vertice(x=v['x'], y=v['y'], Plan=side_b)
                       for v in football.side_b['vertices']]
    Vertice.objects.bulk_create(side_b_vertices)

    # update penalty_a
    for key, value in football.sp_objects['penalty_a'].__dict__.items():
        setattr(penalty_a, key, value)
    penalty_a.Session = session
    penalty_a.parent = side_a
    penalty_a.save()

    Vertice.objects.filter(Plan=penalty_a).delete()
    penalty_a_vertices = [Vertice(x=v['x'], y=v['y'], Plan=penalty_a)
                          for v in football.penalty_a['vertices']]
    Vertice.objects.bulk_create(penalty_a_vertices)

    # update penalty_b
    for key, value in football.sp_objects['penalty_b'].__dict__.items():
        setattr(penalty_b, key, value)
    penalty_b.Session = session
    penalty_b.parent = side_b
    penalty_b.save()

    Vertice.objects.filter(Plan=penalty_b).delete()
    penalty_b_vertices = [Vertice(x=v['x'], y=v['y'], Plan=penalty_b)
                          for v in football.penalty_b['vertices']]
    Vertice.objects.bulk_create(penalty_b_vertices)

    # update gate_a
    for key, value in football.sp_objects['gate_a'].__dict__.items():
        setattr(gate_a, key, value)
    gate_a.Session = session
    gate_a.parent = penalty_a
    gate_a.save()

    Vertice.objects.filter(Plan=gate_a).delete()
    gate_a_vertices = [Vertice(x=v['x'], y=v['y'], Plan=gate_a)
                       for v in football.gate_a['vertices']]
    Vertice.objects.bulk_create(gate_a_vertices)

    # update gate_b
    for key, value in football.sp_objects['gate_b'].__dict__.items():
        setattr(gate_b, key, value)
    gate_b.Session = session
    gate_b.parent = penalty_b
    gate_b.save()

    Vertice.objects.filter(Plan=gate_b).delete()
    gate_b_vertices = [Vertice(x=v['x'], y=v['y'], Plan=gate_b)
                       for v in football.gate_b['vertices']]
    Vertice.objects.bulk_create(gate_b_vertices)

    # update field_center
    for key, value in football.sp_objects['field_center'].__dict__.items():
        setattr(field_center, key, value)
    field_center.Session = session
    field_center.parent = field
    field_center.save()

    Vertice.objects.filter(Plan=field_center).delete()
    field_center_vertices = [Vertice(x=v['x'], y=v['y'], Plan=field_center)
                             for v in football.field_center['vertices']]
    Vertice.objects.bulk_create(field_center_vertices)

    data: dict = {}

    try:

        session = Session.objects.get(id=session.id)
        serializer = SessionSerializer(session, many=False)
        data = serializer.data

    except ObjectDoesNotExist:
        pass

    return Response(data=data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def reload_session(request: Request) -> Response:
    """ reloads SP server to make updated landscape, Plans

    Arguments:
        request {Request} -- [description]

    Returns:
        Response -- [description]
    """

    if POSITIONAL_SERVER_IS_ACTIVE:

        data = {
            'command': 'reloadSession',
        }

        try:
            response: Response = requests.post(
                f'{settings.POSITIONAL_SERVER_HOST}:{settings.POSITIONAL_SERVER_PORT}', json.dumps(
                    data)
            )

            if response.status_code == 400:
                return Response(data={'error': [response.json()]}, status=400)

            return Response(data={'success': ['ok']}, status=200)

        except ConnectionError:
            return Response(data={'connection': ['Нет соединения с Сервером позиционирования']}, status=400)

    return Response(data={'success': ['ok'], 'message': ['Сервер позиционирования не активен']}, status=200)
