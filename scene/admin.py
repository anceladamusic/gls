from django.apps import apps
from django.contrib import admin
from .models import *

# class VerticesBuildingInline(admin.TabularInline):
# 	model = VerticesBuilding
# 	extra = 0

# class VerticesFloorInline(admin.TabularInline):
# 	model = VerticesFloor
# 	extra = 0

# class VerticesKabinetInline(admin.TabularInline):
# 	model = VerticesKabinet
# 	extra = 0


class Inlines():

    def __init__(self):
        self.model = {
            # 'scene.verticesbuilding': [
            # 	VerticesBuildingInline,
            # ],
            # 'scene.verticesfloor': [
            # 	VerticesFloorInline,
            # ],
            # 'scene.verticeskabinet': [
            # 	VerticesKabinetInline,
            # ],
        }


app_models = apps.get_app_config('scene').get_models()

for model in app_models:

    class Admin(admin.ModelAdmin):
        list_display = [field.name for field in model._meta.fields]

        if str(model._meta) in Inlines().model:

            inlines = Inlines().model[str(model._meta)]

    admin.site.register(model, Admin)
