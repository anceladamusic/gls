
from rest_framework_jwt.views import obtain_jwt_token

from django.urls import path

from login.api.views import (
    logout,
    create_user,
    delete_user,
)


urlpatterns = [
    path('api-auth/login/', obtain_jwt_token, name='api-login'),
    path('api-auth/logout/', logout, name='api-logout'),

    path('api-create_user/', create_user, name='api-create_user'),
    path('api-delete_user/', delete_user, name='api-delete_user'),
]
