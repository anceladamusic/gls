
from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class CreateUserAPITestCase(APITestCase):

    """ test user login """

    def setUp(self):

        self.url = api_reverse('login:api-create_user')

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

    def test_get_wrong_method(self):
        """ if get """

        data = {}

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(self.url, data, format='json')

        self.assertEqual(response.status_code,
                         status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_no_credentials(self):
        """ post with no credentials """

        data = {
            'username': 'someuser',
            'password': 'somepassword',
            'email': 'someemail@gmail.com',
        }

        response = self.client.post(self.url, data, format='json')

        self.assertEqual(response.status_code,
                         status.HTTP_401_UNAUTHORIZED)

    def test_post(self):
        """ post """

        data = {
            'username': 'someuser',
            'password': 'army beautiful',
            'email': 'someemail@gmail.com',
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(self.url, data, format='json')

        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
