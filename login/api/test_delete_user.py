
from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class DeleteUserAPITestCase(APITestCase):

    """ test user login """

    def setUp(self):

        self.url = api_reverse('login:api-delete_user')

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user2 = User.objects.create(
            username='denis2', email='anceladamusic@gmail.com')
        user2.set_password('PDVAncel1156482')
        user2.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

    def test_get_wrong_method(self):
        """ if get """

        data = {}

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(self.url, data, format='json')

        self.assertEqual(response.status_code,
                         status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_delete(self):
        """ delete """

        data = {
            'id': User.objects.last().id,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.delete(self.url, data, format='json')

        self.assertEqual(response.status_code,
                         status.HTTP_204_NO_CONTENT)

        self.assertEqual(User.objects.all().count(), 1)
