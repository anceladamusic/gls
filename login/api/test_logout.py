
from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework_jwt.settings import api_settings
from rest_framework import status

from django.contrib.auth.models import User
from django.contrib.sessions.backends.db import SessionStore

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class UserLogoutAPITestCase(APITestCase):

    """ test user logout """

    def setUp(self):

        self.url = api_reverse('login:api-logout')

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')

        user.set_password('PDVAncel115648')

        user.save()

        session_store = SessionStore()
        session_store.create()
        self.session_key = session_store.session_key

        payload = payload_handler(user)

        self.token_rsp = encode_handler(payload)

    def test_unauthorized(self):
        """ if wrong type """

        response = self.client.post(self.url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_wrong_type(self):
        """ if wrong type """

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(self.url, {}, format='json')

        self.assertEqual(response.status_code,
                         status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_wront_schema(self):
        """ if wrong schema """

        data = {
            'username': 'username',
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(self.url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_logout_with_params(self):
        """ if auth n params """

        data = {
            'session_key': self.session_key,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(self.url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
