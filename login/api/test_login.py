
from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status

from django.contrib.auth.models import User


class UserLoginAPITestCase(APITestCase):

    """ test user login """

    def setUp(self):

        self.url = api_reverse('login:api-login')

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')

        user.set_password('PDVAncel115648')

        user.save()

    def test_login_wrong(self):
        """ if wrong auth """

        data = {
            'username': 'username',
            'password': 'password',
        }

        response = self.client.post(self.url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login_ok(self):
        """ if auth """

        data = {
            'username': 'denis',
            'password': 'PDVAncel115648',
        }

        response = self.client.post(self.url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
