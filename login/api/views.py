import json
import ipdb
from schema import Schema, Use, SchemaError
from typing import Dict, Optional

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.request import Request

from django.shortcuts import get_object_or_404
from django.contrib.auth.backends import ModelBackend
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User

from login.api.serializes import UserSerializer


def jwt_response_payload_handler(token, user=None, request=None):
    """ custom response payload handler """

    ModelBackend().authenticate(
        request=request, username=user.username, password=user.password)

    request.session.create()

    return {
        'token': token,
        'email': user.email,
        'username': user.username,
        'session_key': request.session.session_key,
        'id': user.id,
    }


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def logout(request):

    params = json.loads(json.dumps(request.query_params))

    schema = Schema({
        'session_key': Use(str),
    })

    error = None

    try:
        schema.validate(params)
    except SchemaError as e:
        error = e.__dict__['autos']

    if error:
        return Response(error, 400)

    s = SessionStore(session_key=params['session_key'])
    s.delete()

    return Response(status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_user(request: Request) -> Response:
    """ Creates user object

    Arguments:
        request {Request} -- Rest framework Request object

    Returns:
        Response -- respose with created User object data
    """

    serializer = UserSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def delete_user(request: Request) -> Response:
    """ Deletes user object if exists

    Arguments:
        request {Request} -- rest framework Request object

    Returns:
        Response -- response with status code and errors
    """

    data: Dict[str, int] = request.data

    schema: Schema = Schema({
        'id': Use(int),
    })

    error: Optional[Dict[str, list]] = None

    try:
        schema.validate(data)
    except SchemaError as e:
        error = e.__dict__['autos']

    if error:
        return Response(error, 400)

    user: User = get_object_or_404(User, id=data['id'])

    user.delete()

    return Response(status=status.HTTP_204_NO_CONTENT)
