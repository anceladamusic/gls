
""" test tag object """
import json
import ipdb
from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

from values.models import (
    Tag,
    Node,
)
from tag_node.models import (
    TagProperties,
    NodeProperties,
)

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class TagAPITestCase(APITestCase):

    """ test Tag """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        self.tag = Tag.objects.create(**{
            'id': 8000,
        })

        self.tag = Tag.objects.create(**{
            'id': 8001,
        })

        self.node = Node.objects.create(**{
            'id': 1,
        })

        NodeProperties.objects.filter(Node=self.node).update(Tag=self.tag)

    def tearDown(self):
        Tag.objects.all().delete()
        Node.objects.all().delete()

    def test_not_login(self):
        """ test when user isn't auth """

        url = api_reverse('api-values:tag-list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get(self):
        """ get """

        url = api_reverse('api-values:tag-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_with_param(self):
        """ get with param """

        url = api_reverse('api-values:tag-list')

        data = {
            'unlinked': True,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_multiple_post(self):
        """ posts multiple objects """

        url = api_reverse('api-values:tag-list')

        data = [{
            'id': 8002,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        tagproperties = TagProperties.objects.filter(Tag_id=data[0]['id'])

        self.assertEqual(len(tagproperties), 1)

    def test_delete_multiple(self):
        """ deletes multiple objects """

        url = api_reverse('api-values:tag-list')

        data = [{
            'id': 8000,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.delete(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
