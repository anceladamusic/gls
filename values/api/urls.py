
from rest_framework_bulk.routes import BulkRouter

from django.urls import path
from django.conf.urls import include

from values.api.views import (
    NodeViewSet,
    PureNodeViewSet,
    TagViewSet,
    auto_position_routers,
    get_activity_routers,
)

router = BulkRouter()

router.register(r'node', NodeViewSet, 'node')
router.register(r'pure_node', PureNodeViewSet, 'pure_node')
router.register(r'tag', TagViewSet, 'tag')

urlpatterns = [
    path('api-values/', include((router.urls, 'api-values'), namespace='api-values')),
    path('api-values/auto_position_routers/',
         auto_position_routers, name='auto_position_routers'),
    path('api-values/get_activity_routers/',
         get_activity_routers, name='get_activity_routers'),
]
