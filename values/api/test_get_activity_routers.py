""" test get activity routers """

import ipdb
import json
from pprint import pprint

from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings
from rest_framework.response import Response

from django.contrib.auth.models import User
from django.conf import settings

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER

IS_ACTIVE: bool = settings.POSITIONAL_SERVER_IS_ACTIVE


class GetActivityRoutersAPITestCase(APITestCase):

    """ test activity Routers """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

    def test_not_login(self):
        """ test when user isn't auth """

        url = api_reverse('get_activity_routers')

        response = self.client.post(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ if login """

        url = api_reverse('get_activity_routers')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response: Response = self.client.get(url, {}, format='json')

        if IS_ACTIVE:
            pprint(response.data)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        else:
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
