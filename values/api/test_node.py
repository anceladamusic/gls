
""" tests node model """
import ipdb
from pprint import pprint

from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

from values.models import Node
from tag_node.models import NodeProperties
from node_group.models import (
    UserGroup,
    UserGroupNode,
)

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class NodeAPITestCase(APITestCase):

    """ test Node """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        self.node = Node.objects.create()
        self.node2 = Node.objects.create()

        UserGroup.objects.create(**{
            'name': 'usergrouop',
            'User': user_obj,
        })

        self.usergroupnode = UserGroupNode.objects.create(**{
            'UserGroup': UserGroup.objects.first(),
            'Node': Node.objects.first(),
        })

    def tearDown(self):

        nodes = Node.objects.all()
        nodes.delete()

    def test_not_login(self):
        """ test when user isn't auth """

        url = api_reverse('api-values:node-list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ if login """

        url = api_reverse('api-values:node-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_list_of_ids(self):
        """ if list_of_ids in request params """

        url = api_reverse('api-values:node-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = {
            'ids': [self.node.id]
        }
        response = self.client.get(url, data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_multiple_post(self):
        """ posts multiple objects """

        url = api_reverse('api-values:node-list')

        data = [{}]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        node_props = NodeProperties.objects.filter(
            Node_id=response.data[0]['id'])

        self.assertEqual(len(node_props), 1)

    def test_delete_multiple(self):
        """ deletes multiple objects """

        url = api_reverse('api-values:node-list')

        data = [{
            'id': Node.objects.first().id,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.delete(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        node_props = NodeProperties.objects.filter(Node_id=data[0]['id'])

        self.assertEqual(len(node_props), 0)
