""" Node and Tag views of models """

import ipdb
import json
import random
import requests

from requests.exceptions import ConnectionError
from pprint import pprint
from typing import (
    List,
    Optional,
    Iterable,
)
from schema import (
    Schema,
    Use,
    SchemaError,
)
from rest_framework.decorators import (
    api_view,
    permission_classes
)
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework_bulk import (
    BulkModelViewSet,
)

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

from mainapp.rest_bulk_destroyer import RestBulkDestroyer
from mainapp.rest_permissions import IsOwnerOrReadOnly
from mainapp.base_api import (
    LargeResultsSetPagination,
    SmallResultsSetPagination
)
from values.models import (
    Node,
    Tag,
)
from scene.models import (
    Router,
    LoadLandscape,
)
from values.api.serializers import (
    NodeSerializer,
    PureNodeSerializer,
    TagSerializer,
)
from node_group.api.serializers import (TimePeriodSerializer)
from scene.api.serializers import RouterSerializer

HOST = settings.POSITIONAL_SERVER_HOST
PORT = settings.POSITIONAL_SERVER_PORT
POSITIONAL_SERVER_IS_ACTIVE = settings.POSITIONAL_SERVER_IS_ACTIVE


class NodeViewSet(RestBulkDestroyer, BulkModelViewSet):
    """ node viewset related to props """

    lookup_field = 'id'
    serializer_class = NodeSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = SmallResultsSetPagination

    def get_queryset(self, *args, **kwargs):
        if self.request.query_params:
            request: Request = self.request
            ids: Optional[List[int]] = request.query_params.get('ids', [])
            if ids:
                return Node.objects.filter(id__in=ids)
        return Node.objects.all()

    def perform_destroy(self, serializer):
        objects = json.loads(self.request.body)
        Node.objects.filter(id__in=(obj['id'] for obj in objects)).delete()
        return objects


class PureNodeViewSet(RestBulkDestroyer, BulkModelViewSet):
    """ only node id viewset """

    lookup_field = 'id'
    serializer_class = PureNodeSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        if self.request.user:
            return Node.objects.all()


class TagViewSet(RestBulkDestroyer, BulkModelViewSet):
    """ tag viewset related to props """

    lookup_field = 'id'
    serializer_class = TagSerializer
    permission_classes = [IsAuthenticated]
    # pagination_class = LargeResultsSetPagination

    def get_queryset(self, *args, **kwargs):
        if self.request.query_params:
            unlinked = bool(self.request.query_params.get('unlinked', None))
            if unlinked:
                return Tag.objects.filter(nodeproperties__Tag__isnull=True)
        return Tag.objects.all()

    def perform_destroy(self, serializer):
        objects = json.loads(self.request.body)
        Tag.objects.filter(id__in=(obj['id'] for obj in objects)).delete()
        return objects


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def auto_position_routers(request) -> Response:
    """ Send request to SP to get list of Routers then update or create this routers

    Arguments
        [dict] -- dict {'command': 'auto_position_anchors', 'idLayer': int}

    Returns:
        [list of Routers] -- list of routers to update or create
    """

    params = json.loads(json.dumps(request.data))

    schema = Schema({
        'command': Use(str),
        'idLayer': Use(int),
    })

    error = None

    try:
        schema.validate(params)

    except SchemaError as e:
        error = e.__dict__['autos']

    try:
        landscape: LoadLandscape = LoadLandscape.objects.get(
            id=params['idLayer'])
    except ObjectDoesNotExist as e:
        error = 'Подложки с таким идентификатором не существует'

    if error:
        return Response(error, 400)

    if POSITIONAL_SERVER_IS_ACTIVE:

        response = requests.post(
            f'{HOST}:{PORT}',
            data=json.dumps({
                'command': 'autoPositionAnchors',
                'idLayer': landscape.id
            })
        )

        result = []

        maxx: float = 0
        maxy: float = 0

        for router in response.json()['anchors']:

            # for router in response['result']['anchors']:
            r = Router.from_router_to_db(router)

            obj, created = Router.objects.update_or_create(
                id=r['id'],
                defaults=r
            )

            result.append(obj)

            maxx = obj.x if obj.x > maxx else maxx
            maxy = obj.y if obj.y > maxy else maxy

        # update maximum in scene
        landscape.maxx = maxx
        landscape.maxy = maxy
        landscape.dae_position_x = maxx / 2
        landscape.dae_position_y = maxy / 2
        landscape.controls_target_x = maxx / 2
        landscape.controls_target_y = maxy / 2
        landscape.camera_position_x = maxx / 2
        landscape.save()

        serializer = RouterSerializer(result, many=True)

    else:

        lenX: int = random.randint(90, 120)
        lenY: int = random.randint(45, 75)

        # update maximum in scene
        landscape.maxx = lenX
        landscape.maxy = lenY
        landscape.dae_position_x = lenX / 2
        landscape.dae_position_y = lenY / 2
        landscape.controls_target_x = lenX / 2
        landscape.controls_target_y = lenY / 2
        landscape.camera_position_x = lenX / 2
        landscape.save()

        routers: List[Router] = []

        router_1, created = Router.objects.update_or_create(
            id=3834330223851012125,
            defaults={
                'name': 'Роутер 1',
                'x': 0,
                'y': 0,
                'z': 1.5,
                'inuse': True,
                'LoadLandscape': landscape,
            },
        )

        if router_1:
            routers.append(router_1)

        router_2, created = Router.objects.update_or_create(
            id=3834330210966044715,
            defaults={
                'name': 'Роутер 2',
                'x': 0,
                'y': lenY,
                'z': 1.5,
                'inuse': True,
                'LoadLandscape': landscape,
            },
        )

        if router_2:
            routers.append(router_2)

        router_3, created = Router.objects.update_or_create(
            id=3762283697943347232,
            defaults={
                'name': 'Роутер 3',
                'x': lenX,
                'y': lenY,
                'z': 1.5,
                'inuse': True,
                'LoadLandscape': landscape,
            },
        )

        if router_3:
            routers.append(router_3)

        router_4, created = Router.objects.update_or_create(
            id=3690226035188236325,
            defaults={
                'name': 'Роутер 4',
                'x': lenX,
                'y': 0,
                'z': 1.5,
                'inuse': True,
                'LoadLandscape': landscape,
            },
        )

        if router_4:
            routers.append(router_4)

        serializer = RouterSerializer(routers, many=True)

    return Response(data=serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_activity_routers(request: Request) -> Response:
    """ Send request to SP to get list of Active routers
    """

    if POSITIONAL_SERVER_IS_ACTIVE:

        try:

            response: Response = requests.post(
                f'{HOST}:{PORT}',
                data=json.dumps({
                    'command': 'listActivityAnchors',
                })
            )

            routers = response.json()['anchors']

            for r in routers:
                r['id'] = int(r['id'], 0)

            return Response(data=routers, status=status.HTTP_200_OK)

        except ConnectionError:
            return Response(data={'status': 'error', 'message': ' no connection with POSITIONAL_SERVER'}, status=status.HTTP_408_REQUEST_TIMEOUT)

    return Response(data={'status': ['ok'], 'message': ['You must set in settings.py POSITIONAL_SERVER_IS_ACTIVE = True']}, status=status.HTTP_200_OK)
