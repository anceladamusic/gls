
import ipdb

from rest_framework.serializers import (
    ModelSerializer,
    PrimaryKeyRelatedField,
    IntegerField,
)
from rest_framework_bulk import (
    BulkSerializerMixin,
    BulkListSerializer,
)

from django.conf import settings

from values.models import Node, Tag
from tag_node.models import TagProperties, NodeProperties
from tag_node.api.serializers import (
    TagPropertiesSerializer,
    NodePropertiesSerializer,
)



class PureNodeSerializer(BulkSerializerMixin, ModelSerializer):
    """ serializes Nodes """

    class Meta:
        """ props """
        model = Node
        list_serializer_class = BulkListSerializer
        update_lookup_field = 'id'
        fields = ['id']


class NodeSerializer(BulkSerializerMixin, ModelSerializer):
    """ serializes Nodes """
    nodeproperties = NodePropertiesSerializer(
        many=False, read_only=True, required=False)
    id = IntegerField(required=False)

    class Meta:
        """ props """
        model = Node
        list_serializer_class = BulkListSerializer
        update_lookup_field = 'id'
        fields = ('id', 'nodeproperties')


class TagSerializer(BulkSerializerMixin, ModelSerializer):
    """ serializes Tag """
    tagproperties = TagPropertiesSerializer(
        many=False, read_only=True, required=False)

    class Meta:
        """ props """
        model = Tag
        list_serializer_class = BulkListSerializer
        update_lookup_field = 'id'
        fields = ('id', 'tagproperties')
