""" test auto position routers """
import ipdb
import json
from pprint import pprint

from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User
from django.conf import settings

from scene.models import (
    Router,
    LoadLandscape,
)

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER

IS_ACTIVE: bool = settings.POSITIONAL_SERVER_IS_ACTIVE


class AutoPositionRoutersAPITestCase(APITestCase):

    """ test Node """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        self.landscape = LoadLandscape.objects.create(**{
            "name": "сцена для тестирования взаимодействия ws-sp",
            "camera_position_x": 70.0,
            "camera_position_y": -80.0,
            "camera_position_z": 50.0,
            "camera_up_x": 0.0,
            "camera_up_y": 0.0,
            "camera_up_z": 1.0,
            "controls_target_x": 67.0,
            "controls_target_y": 40.0,
            "controls_target_z": 0.0,
            "dae_rotation_x": 3.14159265359,
            "dae_rotation_y": 3.14159265359,
            "dae_rotation_z": 3.14159265359,
            "dae_position_x": 76.639468457,
            "dae_position_y": 74.03660896,
            "dae_position_z": 6.0,
            "minx": 0.0,
            "miny": 0.0,
            "minz": 0.0,
            "maxx": 149.998046875,
            "maxy": 149.998046875,
            "maxz": 0.0,
        })

        Router.objects.create(**{
            'id': 3834330223851012125,
            'name': 'Роутер 1',
            'LoadLandscape': LoadLandscape.objects.first()
        })

    def tearDown(self):

        LoadLandscape.objects.all().delete()

        Router.objects.all().delete()

    def test_not_login(self):
        """ test when user isn't auth """

        url = api_reverse('auto_position_routers')

        response = self.client.post(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login_wrong_params(self):
        """ if login with wrong params """

        url = api_reverse('auto_position_routers')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = {
            'command': 'auto_position_anchors',
            'idLayer': 122,
        }

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_login(self):
        """ if login """

        url = api_reverse('auto_position_routers')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = {
            'command': 'auto_position_anchors',
            'idLayer': LoadLandscape.objects.first().id,
        }

        response = self.client.post(
            url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(len(Router.objects.all()), 4)
