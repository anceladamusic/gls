""" values models """
import json
from pprint import pprint
import requests
from rest_framework.response import Response
from requests.exceptions import ConnectionError
from django.db.models.signals import pre_delete
from django.db import models
from django.core.exceptions import ValidationError
from django.db.models.signals import (
    post_save,
    pre_save,
)
import ipdb
from django.apps import apps
from django.dispatch import receiver
from django.conf import settings


HOST: str = settings.POSITIONAL_SERVER_HOST
PORT: int = settings.POSITIONAL_SERVER_PORT
ACTIVE: bool = settings.POSITIONAL_SERVER_IS_ACTIVE

AIO_HOST: str = settings.AIOHTTP_SERVER_HOST
AIO_PORT: int = settings.AIOHTTP_SERVER_PORT
AIO_ACTIVE: bool = settings.AIOHTTP_SERVER_IS_ACTIVE

MODELS_SIGNALS_IS_ACTIVE: bool = settings.MODELS_SIGNALS_IS_ACTIVE


class Tag(models.Model):

    """ tag """

    class Meta():
        """ props """
        ordering = ['id']
        verbose_name = 'Метка'
        verbose_name_plural = 'Метки'

    id = models.BigIntegerField(
        primary_key=True,
        verbose_name='идентификатор метки',
        unique=True,
    )

    def __str__(self): return f'{self.id}'

    def as_dict(self):
        """ dict """
        return {
            'TagId': self.id,
        }

    def as_unique(self):
        """ adds node props to tag """
        node_id = None

        if hasattr(self, 'tag'):
            node = getattr(self, 'tag')

            node_name = node.name

            node_id = node.Node_id

        else:
            node_name = None

        return {
            'tag_id': self.id,
            'node_id': node_id,
            'nodename': node_name,
        }


@receiver(pre_save, sender=Tag)
def pre_create_tag(sender: Tag, instance: Tag, **kwargs):
    if MODELS_SIGNALS_IS_ACTIVE:
        if ACTIVE:
            tagproperties_model = apps.get_model('tag_node', 'tagproperties')

            data = {
                'command': 'saveTags',
                'tags': [
                    {
                        'tagId': hex(instance.id),
                        'properties': tagproperties_model().as_sp_props(),
                    }
                ]
            }

            try:
                response: Response = requests.post(f'{HOST}:{PORT}', data=json.dumps(data), headers={
                    'content-type': 'application/json', 'charset': 'utf-8'})

                if response.status_code == 400:
                    pprint(f'SP server answers ERROR {response.json()}')
            except ConnectionError:
                pprint('no create tag in SP!')


@receiver(post_save, sender=Tag)
def post_create_tag(sender: Tag, instance: Tag, created: bool, **kwargs):
    if MODELS_SIGNALS_IS_ACTIVE:
        if not created:
            return
        tagproperties_model = apps.get_model('tag_node', 'tagproperties')
        tagproperties_model.objects.create(Tag=instance)


@receiver(pre_delete, sender=Tag)
def delete_tag(sender: Tag, instance: Tag, **kwargs):
    if MODELS_SIGNALS_IS_ACTIVE:
        if ACTIVE:
            data = {
                'command': 'deleteTags',
                'tags': [
                    {
                        'tagId': hex(instance.id),
                    }
                ]
            }
            try:
                response: Response = requests.post(f'{HOST}:{PORT}', data=json.dumps(data), headers={
                    'content-type': 'application/json', 'charset': 'utf-8'})

                if response.status_code == 400:
                    pprint(f'SP server answers ERROR {response.json()}')
            except ConnectionError:
                pprint('no delete tag in SP!')


class Node(models.Model):
    """ Node """

    class Meta():
        """ props """
        ordering = ['-id']
        verbose_name = 'Объекты слежения'
        verbose_name_plural = 'Объекты слежения'

    id = models.AutoField(
        primary_key=True,
        verbose_name='идентификатор объекта слежения'
    )

    def __str__(self): return f'{self.id}'


@receiver(pre_save, sender=Node)
def pre_create(sender: Node, instance: Node, **kwargs) -> None:
    """ signals POSITIONAL SERVER to create object

    Arguments:
        sender {Node} -- node model
        instance {Node} -- node object
    """
    if MODELS_SIGNALS_IS_ACTIVE:
        if ACTIVE:
            data = {
                'command': 'addNodes',
                'nodes': [
                    {
                        'id': -1,
                    }
                ]
            }

            try:
                response: Response = requests.post(f'{HOST}:{PORT}', data=json.dumps(data), headers={
                    'content-type': 'application/json', 'charset': 'utf-8'})

                if response.status_code == 400:
                    pprint(f'SP server answers ERROR {response.json()}')
                instance.id = response.json()['nodes'][0]['id']

            except ConnectionError:
                pprint('no create node in SP!')

    if AIO_ACTIVE:
        try:
            response = requests.post(
                f'{AIO_HOST}:{AIO_PORT}/node/{instance.id}')
        except ConnectionError:
            pprint('no create node in AIO!')


@receiver(post_save, sender=Node)
def post_create_node(sender: Node, instance: Node, created: bool, **kwargs) -> None:
    """ auto creates nodeproperties object

    Arguments:
        sender {Node} -- node model
        instance {Node} -- node object
        created {bool} -- created or not signal
    """
    if MODELS_SIGNALS_IS_ACTIVE:

        if not created:
            return
        nodeproperties_model = apps.get_model('tag_node', 'nodeproperties')
        nodeproperties_model.objects.update_or_create(Node=instance)


@receiver(pre_delete, sender=Node)
def delete_node(sender: Node, instance: Node, **kwargs) -> None:
    """ signals POSITIONAL_SERVER to delete node object

    Arguments:
        sender {Node} -- node model
        instance {Node} -- node object
    """

    if MODELS_SIGNALS_IS_ACTIVE:

        if ACTIVE:
            data = {
                'command': 'deleteNodes',
                'nodes': [
                    {
                        'id': instance.id,
                    }
                ]
            }

            try:
                response: Response = requests.post(f'{HOST}:{PORT}', data=json.dumps(data), headers={
                    'content-type': 'application/json', 'charset': 'utf-8'})

                if response.status_code == 400:
                    pprint(f'SP server answers ERROR {response.json()}')
            except ConnectionError:
                pprint('no delete node in SP!')

    if AIO_ACTIVE:
        try:
            requests.delete(
                f'{AIO_HOST}:{AIO_PORT}/node/{instance.id}')
        except ConnectionError:
            pprint('no delete node in AIO!')
