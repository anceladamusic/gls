FROM python:3.7
ENV PYTHONUNBUFFERED 1
RUN mkdir /gls2_back_docker
WORKDIR /gls2_back_docker
ADD requirements.txt /gls2_back_docker/

RUN apt-get update
# make sure that locales package is available
RUN apt-get install --reinstall -y locales
# uncomment chosen locale to enable it's generation
RUN sed -i 's/# ru_RU.UTF-8 UTF-8/ru_RU.UTF-8 UTF-8/' /etc/locale.gen
# generate chosen locale
RUN locale-gen ru_RU.UTF-8
# set system-wide locale settings

# GLS ONLY!!!! geos for shaperly
RUN apt-get --no-install-recommends -y install libgeos-dev python3-numpy cmake

ENV LANG ru_RU.UTF-8
ENV LANGUAGE ru_RU
ENV LC_ALL ru_RU.UTF-8
# verify modified configuration
RUN dpkg-reconfigure --frontend noninteractive locales

RUN pip install -r requirements.txt
ADD . /gls2_back_docker/
