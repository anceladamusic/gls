
import ipdb
import json
import requests
import datetime

from pprint import pprint
from colorfield.fields import ColorField

from rest_framework.response import Response
from requests.exceptions import ConnectionError
from django.contrib.auth.models import User
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.conf import settings
from django.db.models.signals import (
    pre_delete,
    post_delete,
    post_save,
    pre_save,
)
from django.dispatch import receiver

from mainapp.base import Base
from scene.models import (Plan, LoadLandscape)
from values.models import (
    Node,
)
from tag_node.models import NodeProperties

from node_group.api.validators import (
    validate_object_quantity_already_is_two,
    validate_object_if_team_with_period_already_have,
)

HOST: str = settings.POSITIONAL_SERVER_HOST
PORT: int = settings.POSITIONAL_SERVER_PORT
ACTIVE: bool = settings.POSITIONAL_SERVER_IS_ACTIVE


MODELS_SIGNALS_IS_ACTIVE: bool = settings.MODELS_SIGNALS_IS_ACTIVE


class UserGroup(models.Model):
    """ User group """

    class Meta():
        """ props """
        verbose_name = 'Группа объектов слежения'
        verbose_name_plural = 'Группы объектов слежения'

    name: str = models.CharField('Наименование', max_length=200)
    color: str = ColorField(verbose_name='цвет', blank=False, null=False)
    User = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True,
                             verbose_name='Пользователь')

    @property
    def owner(self):
        """ adds user prop """
        return self.User

    def __str__(self):
        return 'Пользователь: {0} | Наименование: {1}'.format(self.User, self.name)

    def as_dict(self):
        """ obj to dict """
        return {
            'id': self.id,
            'name': self.name,
            # 'type': str(self.content_type),
            'color': self.color,
            'user': self.User_id,
            'nodes': [node.Node.nodeproperties.as_dict() for node in getattr(self, 'usergroupnode_set').all() if node.Node]
        }


class UserGroupNode(models.Model):
    """ nodes to usergroup"""
    class Meta():
        """ props """
        verbose_name = 'Связь между нодами и группами'
        verbose_name_plural = 'Связи между нодами и группами'

    UserGroup = models.ForeignKey(
        to=UserGroup, on_delete=models.CASCADE, verbose_name='Группа')
    Node = models.ForeignKey(
        Node, on_delete=models.CASCADE, verbose_name='Нода')

    def __str__(self):
        return 'Группа: {0} | объект слежения: {1}'.format(self.UserGroup, self.Node)


class Team(models.Model):
    """ team of node """

    class Meta():
        verbose_name = 'Команда'
        verbose_name_plural = 'Команды'

    name: str = models.CharField('Наименование', max_length=200)
    color: str = ColorField(verbose_name='цвет', blank=False, null=False)

    def __str__(self):
        return f'{self.name}'


class TeamNode(models.Model):
    """ teamnode connection """

    class Meta():
        verbose_name = 'Связь между Нодой и Коммандой'
        verbose_name_plural = 'Связи между Нодами и Командой'

    Team = models.ForeignKey(
        to=Team, on_delete=models.CASCADE, verbose_name='Команда')
    Node = models.OneToOneField(
        to=Node, on_delete=models.CASCADE, verbose_name='Объект слежения')

    def __str__(self):
        nodeproperties: NodeProperties = self.Node.nodeproperties
        return f'{nodeproperties.last_name} {self.Team.name}'


class TimePeriod(models.Model):
    """ game periods """

    class Meta():
        """ props """
        verbose_name = 'Пользовательский период'
        verbose_name_plural = 'Пользовательскиеи периоды'

    FIRST_PERIOD = 'матч, 1-й тайм'
    SECOND_PERIOD = 'матч, 2-й тайм'
    FIRST_ADV_PERIOD = 'матч, 1-й дополнительный тайм'
    SECOND_ADV_PERIOD = 'матч, 2-й дополнительный тайм'
    CUSTOM_PERIOD = 'тренировка'

    PERIOD_TYPES = (
        (FIRST_PERIOD, FIRST_PERIOD),
        (SECOND_PERIOD, SECOND_PERIOD),
        (FIRST_ADV_PERIOD, FIRST_ADV_PERIOD),
        (SECOND_ADV_PERIOD, SECOND_ADV_PERIOD),
        (CUSTOM_PERIOD, CUSTOM_PERIOD),
    )

    landscape = models.ForeignKey(LoadLandscape, null=True, blank=True, on_delete=models.CASCADE,
                                  verbose_name='сцена')
    name: str = models.CharField('наименование', max_length=200)
    type: str = models.CharField('тип', max_length=200,
                                 choices=PERIOD_TYPES, default=FIRST_PERIOD)
    start: datetime.datetime = models.DateTimeField(
        'начало', default=timezone.now)
    end: datetime.datetime = models.DateTimeField(
        'конец', null=True, blank=True)

    def __str__(self):
        return f'{self.name}'

    @property
    def owner(self):
        """ adds user prop """
        return self.user

    def clean(self):
        """ compares start-end """
        if self.end:
            if self.start > self.end:
                raise ValidationError(
                    'Поле Конец не должно быть ранее поля Начало.')


@receiver(post_save, sender=TimePeriod)
def create_time_period(sender: TimePeriod, instance: TimePeriod, created: bool, **kwargs) -> None:
    """ updates start by POSITIONAL_SERVER """

    if MODELS_SIGNALS_IS_ACTIVE:

        if ACTIVE:

            if created:

                data = {
                    'command': 'startWriteHistory',
                }

                try:
                    response: Response = requests.post(
                        f'{HOST}:{PORT}', json.dumps(data))

                    if response.status_code == 400:
                        pprint(f'SP server answers ERROR {response.json()}')

                    instance.start = Base().datetime_localize(datetime.datetime.fromtimestamp(
                        response.json()['dT']/1000))
                    instance.save()

                except ConnectionError:
                    pprint('no create start time')


@receiver(post_save, sender=TimePeriod)
def update_time_period(sender: TimePeriod, instance: TimePeriod, created: bool, update_fields: set, **kwargs) -> None:
    """ Update End by POSITIONAL_SERVER
    """

    if MODELS_SIGNALS_IS_ACTIVE:

        if ACTIVE:

            if not created:

                data = {
                    'command': 'stopWriteHistory',
                }

                try:
                    response: Response = requests.post(
                        f'{HOST}:{PORT}', json.dumps(data))

                    if response.status_code == 400:
                        pprint(f'SP server answers ERROR {response.json()}')

                    instance.end = Base().datetime_localize(datetime.datetime.fromtimestamp(
                        response.json()['dT']/1000))

                except ConnectionError:
                    pprint('no receive End datetime from SP')


@receiver(pre_delete, sender=TimePeriod)
def delete_time_period(sender: TimePeriod, instance: TimePeriod, **kwargs) -> None:
    """ Update send stopHistory before delete """
    if MODELS_SIGNALS_IS_ACTIVE:
        if ACTIVE:

            data = {
                'command': 'stopWriteHistory',
            }

            try:
                response: Response = requests.post(
                    f'{HOST}:{PORT}', json.dumps(data))

                if response.status_code == 400:
                    pprint(f'SP server answers ERROR {response.json()}')

                instance.end = Base().datetime_localize(datetime.datetime.fromtimestamp(
                    response.json()['dT']/1000))

            except ConnectionError:
                pprint('no receive End datetime from SP')


class TeamTimePeriodPlan(models.Model):
    """ Team with TimePeriod connection """

    class Meta():
        verbose_name = 'Связь между командой, временным периодом, планом'
        verbose_name_plural = 'Связи между командой, временными периодами, планами'

    Team = models.ForeignKey(
        to=Team, on_delete=models.CASCADE, verbose_name='Команда')
    TimePeriod = models.ForeignKey(
        to=TimePeriod, on_delete=models.CASCADE, verbose_name='Временной период')
    Plan = models.ForeignKey(
        to=Plan, on_delete=models.CASCADE, verbose_name='План')

    def __str__(self): return f'{self.id} {self.TimePeriod.name}'

    def clean(self):
        validate_object_quantity_already_is_two(instance=self)
        validate_object_if_team_with_period_already_have(instance=self)


@receiver(pre_save, sender=TeamTimePeriodPlan)
def validate(sender: TeamTimePeriodPlan, instance: TeamTimePeriodPlan, **kwargs):
    instance.full_clean()
