
import json
import ipdb
import datetime

from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

from mainapp.base import Base
from scene.models import LoadLandscape
from node_group.models import TimePeriod

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class TimePeriodAPITestCase(APITestCase):

    """ test TimePeriod """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user2 = User.objects.create(username='denis2', email='anc@gmail.com')
        user2.set_password('PDVAncel1156482')
        user2.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        landscape = LoadLandscape.objects.create(**{
            "name": "сцена для тестирования взаимодействия ws-sp",
            "camera_position_x": 70.0,
            "camera_position_y": -80.0,
            "camera_position_z": 50.0,
            "camera_up_x": 0.0,
            "camera_up_y": 0.0,
            "camera_up_z": 1.0,
            "controls_target_x": 67.0,
            "controls_target_y": 40.0,
            "controls_target_z": 0.0,
            "dae_rotation_x": 3.14159265359,
            "dae_rotation_y": 3.14159265359,
            "dae_rotation_z": 3.14159265359,
            "dae_position_x": 76.639468457,
            "dae_position_y": 74.03660896,
            "dae_position_z": 6.0,
            "minx": 0.0,
            "miny": 0.0,
            "minz": 0.0,
            "maxx": 149.998046875,
            "maxy": 149.998046875,
            "maxz": 0.0,
        })

        self.landscape = landscape

        LoadLandscape.objects.create(**{
            "name": "сцена для тестирования взаимодействия ws-sp2",
            "camera_position_x": 70.0,
            "camera_position_y": -80.0,
            "camera_position_z": 50.0,
            "camera_up_x": 0.0,
            "camera_up_y": 0.0,
            "camera_up_z": 1.0,
            "controls_target_x": 67.0,
            "controls_target_y": 40.0,
            "controls_target_z": 0.0,
            "dae_rotation_x": 3.14159265359,
            "dae_rotation_y": 3.14159265359,
            "dae_rotation_z": 3.14159265359,
            "dae_position_x": 76.639468457,
            "dae_position_y": 74.03660896,
            "dae_position_z": 6.0,
            "minx": 0.0,
            "miny": 0.0,
            "minz": 0.0,
            "maxx": 149.998046875,
            "maxy": 149.998046875,
            "maxz": 0.0,
        })

        TimePeriod.objects.create(**{
            'landscape': landscape,
            'name': 'test',
            'type': TimePeriod.FIRST_PERIOD,
            'start': Base().datetime_localize(datetime.datetime(2018, 9, 26, hour=16)),
            'end': Base().datetime_localize(datetime.datetime(2018, 9, 26, hour=16, minute=45)),
        })

        TimePeriod.objects.create(**{
            'landscape': landscape,
            'name': 'test',
            'type': TimePeriod.SECOND_PERIOD,
            'start': Base().datetime_localize(datetime.datetime(2018, 9, 26, hour=17)),
            'end': Base().datetime_localize(datetime.datetime(2018, 9, 26, hour=17, minute=45)),
        })

    def tearDown(self):
        LoadLandscape.objects.all().delete()
        TimePeriod.objects.all().delete()

    def test_not_login(self):
        """ tests when user isn't auth """

        url = api_reverse('api-node_group:time_period-list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ if user loged in """

        url = api_reverse('api-node_group:time_period-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_landscape_parameter(self):
        """ if landsape_id parameter """

        url = api_reverse('api-node_group:time_period-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = {
            'landscape_id': LoadLandscape.objects.last().id,
        }

        response = self.client.get(url, data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_multiple_post(self):
        """ posts multiple objects """

        url = api_reverse('api-node_group:time_period-list')

        data = [{
            'landscape': self.landscape.id,
            'name': 'custom',
            'type': TimePeriod.CUSTOM_PERIOD,
            'start': datetime.datetime.strftime(Base().datetime_localize(datetime.datetime(2018, 9, 26, hour=18)), '%d-%m-%YT%H:%M:%S'),
            'end': datetime.datetime.strftime(Base().datetime_localize(datetime.datetime(2018, 9, 26, hour=18, minute=45)), '%d-%m-%YT%H:%M:%S'),
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_patch_multiple(self):
        """ patches multiple objects """

        url = api_reverse('api-node_group:time_period-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = [{
            'id': TimePeriod.objects.first().id,
            'landscape': self.landscape.id,
            'name': 'test modified',
            'type': TimePeriod.FIRST_PERIOD,
            'start': datetime.datetime.strftime(Base().datetime_localize(datetime.datetime(2018, 9, 26, hour=16)), '%d-%m-%YT%H:%M:%S'),
            'end': datetime.datetime.strftime(Base().datetime_localize(datetime.datetime(2018, 9, 26, hour=16, minute=45)), '%d-%m-%YT%H:%M:%S'),
        }]

        response = self.client.patch(url, data, format='json')
        pprint(json.loads(json.dumps(response.data)))

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_multiple(self):
        """ deletes multiple objects """

        url = api_reverse('api-node_group:time_period-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = [{
            'id': TimePeriod.objects.first().id,
        }]

        response = self.client.delete(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
