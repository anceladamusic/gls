

import ipdb
import json
from pprint import pprint
from rest_framework.permissions import IsAuthenticated
from rest_framework_bulk import BulkModelViewSet

from mainapp.rest_bulk_destroyer import RestBulkDestroyer
from mainapp.rest_permissions import IsOwnerOrReadOnly
from node_group.models import (
    UserGroup,
    UserGroupNode,
    Team,
    TeamNode,
    TeamTimePeriodPlan,
    TimePeriod,
)
from .serializers import (
    UserGroupSerializer,
    UserGroupNodeSerializer,
    TeamSerializer,
    TeamNodeSerializer,
    TeamTimePeriodPlanSerializer,
    TimePeriodSerializer
)


class TimePeriodViewSet(RestBulkDestroyer, BulkModelViewSet):
    """ creates rest viewset """

    lookup_field = 'id'
    serializer_class = TimePeriodSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def get_queryset(self, *args, **kwargs):
        if getattr(self.request.user, 'is_authenticated'):
            if 'landscape_id' in self.request.GET:
                return TimePeriod.objects.all().filter(
                    landscape_id=self.request.GET.get('landscape_id'))
            return TimePeriod.objects.all()

    def perform_destroy(self, serializer):
        objects = json.loads(self.request.body)
        TimePeriod.objects.filter(
            id__in=(obj['id'] for obj in objects)).delete()
        return objects


class TeamTimePeriodPlanViewSet(RestBulkDestroyer, BulkModelViewSet):
    """ Team TimePeriod and Plan connection """
    lookup_field = 'id'
    serializer_class = TeamTimePeriodPlanSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        if getattr(self.request.user, 'is_authenticated'):
            return TeamTimePeriodPlan.objects.all()

    def perform_destroy(self, serializer):
        objects = json.loads(self.request.body)
        TeamTimePeriodPlan.objects.filter(
            id__in=(obj['id'] for obj in objects)).delete()
        return objects


class TeamNodeViewSet(RestBulkDestroyer, BulkModelViewSet):
    """ serializes connection between Team and Node object """
    lookup_field = 'id'
    serializer_class = TeamNodeSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        if getattr(self.request.user, 'is_authenticated'):
            return TeamNode.objects.all()

    def perform_destroy(self, serializer):
        objects = json.loads(self.request.body)
        TeamNode.objects.filter(
            id__in=(obj['id'] for obj in objects)).delete()
        return objects


class TeamViewSet(RestBulkDestroyer, BulkModelViewSet):
    """ """
    lookup_field = 'id'
    serializer_class = TeamSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        if getattr(self.request.user, 'is_authenticated'):
            return Team.objects.all()

    def perform_destroy(self, serializer):
        objects = json.loads(self.request.body)
        Team.objects.filter(
            id__in=(obj['id'] for obj in objects)).delete()
        return objects


class UserGroupViewSet(RestBulkDestroyer, BulkModelViewSet):
    """ creates usegroups  """

    lookup_field = 'id'
    serializer_class = UserGroupSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        return serializer.save(User=self.request.user)

    def get_queryset(self, *args, **kwargs):
        if getattr(self.request.user, 'is_authenticated'):
            group_id = self.request.query_params.get('group_id', None)
            if group_id is not None:
                group = UserGroup.objects.filter(
                    id=group_id, User=self.request.user)
                return group
            return UserGroup.objects.all().filter(User=self.request.user)

    def perform_destroy(self, serializer):
        objects = json.loads(self.request.body)
        UserGroup.objects.filter(
            id__in=(obj['id'] for obj in objects)).delete()
        return objects


class UserGroupNodeViewSet(RestBulkDestroyer, BulkModelViewSet):
    """ creates connection between group and nodes """

    lookup_field = 'id'
    serializer_class = UserGroupNodeSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        result = []
        for line in self.request.data:
            new_line = {}
            for key, value in line.items():
                new_line[f'{key}_id'] = value
            result.append(new_line)

        created_list = UserGroupNode.objects.bulk_create(
            UserGroupNode(**line) for line in result)

        new_serializer = UserGroupNodeSerializer(
            instance=created_list, many=True)

        for key, line in enumerate(serializer.data):
            line['Node'] = new_serializer.data[key]['Node']
            line['id'] = new_serializer.data[key]['id']

    def get_queryset(self, *args, **kwargs):
        if getattr(self.request.user, 'is_authenticated'):
            group_id = self.request.query_params.get('UserGroup_id', None)
            if group_id is not None:
                return UserGroupNode.objects.filter(UserGroup_id=group_id)
            return UserGroupNode.objects.all().filter(UserGroup__User=self.request.user)

    def perform_destroy(self, serializer):
        objects = json.loads(self.request.body)
        UserGroupNode.objects.filter(
            id__in=(obj['id'] for obj in objects)).delete()
        return objects
