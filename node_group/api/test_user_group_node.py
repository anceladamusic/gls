""" tests UserGroupNode object """
import ipdb
import json
from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

from node_group.models import (
    UserGroup,
    UserGroupNode,
)
from values.models import (
    Tag,
    Node,
)
from tag_node.models import (
    TagProperties,
    NodeProperties,
)

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class UserGroupNodeAPITestCase(APITestCase):
    """ test UserGroupNode """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        tag = Tag.objects.create(id=1)

        tag2 = Tag.objects.create(id=2)

        self.node = Node.objects.create(id=1)
        self.node2 = Node.objects.create(id=2)

        self.user_group = UserGroup.objects.create(**{
            'name': 'init user group',
            'User': user_obj,
        })

        UserGroup.objects.create(**{
            'name': 'another user group',
            'User': user_obj,
        })

        self.usergroupnode = UserGroupNode.objects.create(**{
            'UserGroup': self.user_group,
            'Node': self.node,
        })

    def tearDown(self):
        Tag.objects.all().delete()
        Node.objects.all().delete()

    def test_get_not_login(self):
        """ test when user isn't auth """

        url = api_reverse('api-node_group:user_group_node-list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_login(self):
        """ if login """

        url = api_reverse('api-node_group:user_group_node-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_with_params(self):
        """ get with params """

        url = api_reverse('api-node_group:user_group_node-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = {'UserGroup_id': UserGroup.objects.last().id}

        response = self.client.get(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_multiple(self):
        """ post multiple object """

        url = api_reverse('api-node_group:user_group_node-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        tag3 = Tag.objects.create(id=3)

        node3 = Node.objects.create(id=3)

        data = [
            {
                'Node': self.node2.id,
                'UserGroup': UserGroup.objects.last().id,
            },
            {
                'Node': node3.id,
                'UserGroup': UserGroup.objects.last().id,
            }
        ]
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_multiple(self):
        """ updates multiple objects """

        url = api_reverse('api-node_group:user_group_node-list')

        data = [{
            'id': UserGroupNode.objects.last().id,
            'Node': Node.objects.first().id,
            'UserGroup': UserGroup.objects.last().id,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_multiple(self):
        """ deletes multiple objects """

        url = api_reverse('api-node_group:user_group_node-list')

        data = [{
            'id': self.usergroupnode.id,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.delete(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
