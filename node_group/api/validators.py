import datetime
import ipdb
from pprint import pprint
from rest_framework.serializers import ValidationError as RestValidationError


def validate_object_quantity_already_is_two(instance):
    """ Checks if model allready have 2 quantity (of ForeignKey=TimePeriod)

    Arguments:
        instance {[TeamTimePeriodPlan]} -- TeamTimePeriodPlan object instance
    """
    time_period = instance.TimePeriod
    model = type(instance)

    team_time_period_plan = model.objects.filter(TimePeriod=time_period)

    if len(team_time_period_plan) > 1:
        raise RestValidationError(
            {'id': ['Количество записей на один период не должено быть более двух']})


def validate_object_if_team_with_period_already_have(instance):
    """Checks if model allready have TimePeriod and Team

    Arguments:
        instance {[TeamTimePeriodPlan]} -- TeamTimePeriodPlan object instance"""

    time_period = instance.TimePeriod
    team = instance.Team
    model = type(instance)

    team_time_period_plan = model.objects.filter(
        TimePeriod=time_period, Team=team)

    if len(team_time_period_plan) == 1:
        raise RestValidationError(
            {'id': [f'Команда {team.name} уже привязана к временному периоду {time_period.name}']})
