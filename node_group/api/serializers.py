""" serializes API requests to NodeGroup model """
import ipdb
from pprint import pprint
from rest_framework.serializers import (
    ModelSerializer,
    DateTimeField,
)
from rest_framework_bulk import (
    BulkSerializerMixin,
    BulkListSerializer,
)
from node_group.models import (
    UserGroup,
    UserGroupNode,
    Team,
    TeamNode,
    TeamTimePeriodPlan,
    TimePeriod,
)
from values.api.serializers import (NodeSerializer)
from scene.api.serializers import (PlanSerializer)


class TeamNodeSerializer(BulkSerializerMixin, ModelSerializer):
    """ connection between team and node """

    node = NodeSerializer(source='Node', many=False,
                          read_only=True, required=False)

    class Meta:
        model = TeamNode
        list_serializer_class = BulkListSerializer
        update_lookup_field = 'id'
        fields = [field.name for field in TeamNode._meta.fields] + ['node']


class TeamSerializer(BulkSerializerMixin, ModelSerializer):
    """ team objects to connect node to team and timeperiod to get home half of football field """

    teamnode_set = TeamNodeSerializer(
        many=True, required=False, read_only=True)

    class Meta:
        model = Team
        list_serializer_class = BulkListSerializer
        update_lookup_field = 'id'
        fields = [field.name for field in Team._meta.fields] + ['teamnode_set']


class UserGroupNodeSerializer(BulkSerializerMixin, ModelSerializer):
    """ serializes connection between group and node """

    Node = NodeSerializer(many=False, read_only=True, required=False)

    class Meta:
        """ props """
        model = UserGroupNode
        list_serializer_class = BulkListSerializer
        update_lookup_field = 'id'
        fields = ['id', 'UserGroup', 'Node', 'UserGroup_id', 'Node_id']


class UserGroupSerializer(BulkSerializerMixin, ModelSerializer):
    """ serializes UserGroup """
    usergroupnode_set = UserGroupNodeSerializer(
        many=True, read_only=True, required=False)

    class Meta:
        """ props """

        model = UserGroup
        list_serializer_class = BulkListSerializer
        delete_lookup_field = 'id'
        fields = [
            'id',
            'name',
            'color',
            'usergroupnode_set',
        ]
        read_only_fields = ['id', 'User']


class TeamTimePeriodPlanSerializer(BulkSerializerMixin, ModelSerializer):
    """ Connection between Team TimePeriod and Plan objects """

    team = TeamSerializer(source='Team', many=False,
                          read_only=True, required=False)
    plan = PlanSerializer(source='Plan', many=False,
                          read_only=True, required=False)

    class Meta:
        model = TeamTimePeriodPlan
        list_serializer_class = BulkListSerializer
        delete_lookup_field = 'id'
        update_lookup_field = 'id'
        fields = [
            field.name for field in TeamTimePeriodPlan._meta.fields] + ['team', 'plan']
        read_only_fields = ['id']


class TimePeriodSerializer(BulkSerializerMixin, ModelSerializer):
    """ serializes Time Period """

    teamtimeperiodplan_set = TeamTimePeriodPlanSerializer(
        many=True, read_only=True, required=False)
    start = DateTimeField(format='%d-%m-%YT%H:%M:%S')
    end = DateTimeField(format='%d-%m-%YT%H:%M:%S')

    class Meta:
        """ props """

        model = TimePeriod
        list_serializer_class = BulkListSerializer
        fields = ['id', 'landscape', 'name', 'type',
                  'start', 'end', 'teamtimeperiodplan_set']
        read_only_fields = ['id', 'user']
