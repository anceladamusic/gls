""" tests team_time_period_plan api team """

import datetime
import io
import ipdb
import json
from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

from mainapp.base import Base
from values.models import (
    Node,
    Tag,
)
from scene.models import (
    Session,
    Plan,
    LoadLandscape,
)
from node_group.models import (
    Team,
    TimePeriod,
    TeamTimePeriodPlan,
)

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class TeamTimePeriodPlanAPITestCase(APITestCase):

    """ test Team """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        self.session: Session = Session.objects.create(**{
            'name': 'Сцена',
            'inuse': True,
        })

        self.landscape: LoadLandscape = self.session.LoadLandscape

        TimePeriod.objects.create(**{
            'landscape': self.landscape,
            'name': 'test period',
            'type': TimePeriod.FIRST_PERIOD,
            'start': Base().datetime_localize(datetime.datetime(2018, 9, 26, hour=16)),
            'end': Base().datetime_localize(datetime.datetime(2018, 9, 26, hour=16, minute=45)),
        })

        Node.objects.create(**{
            'id': 1,
        })

        Tag.objects.create(**{'id': 8000})

        Team.objects.create(**{
            'name': 'Team 1',
            'color': '#ffffff',
        })

        Node.objects.create(**{
            'id': 2,
        })

        Tag.objects.create(**{'id': 8001})

        self.second_team = Team.objects.create(**{
            'name': 'Team 2',
            'color': '#aaaaaa',
        })

        Node.objects.create(**{
            'id': 3,
        })

        Tag.objects.create(**{'id': 8002})

        Team.objects.create(**{
            'name': 'Team 3',
            'color': '#bbbbbb',
        })

        Node.objects.create(**{
            'id': 4,
        })

        TeamTimePeriodPlan.objects.create(**{
            'Team': Team.objects.first(),
            'TimePeriod': TimePeriod.objects.first(),
            'Plan': Plan.objects.first(),
        })

    def test_not_login(self):
        """ test when user isn't login """

        url = api_reverse('api-node_group:team_time_period_plan-list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ if login """

        url = api_reverse('api-node_group:team_time_period_plan-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(len(response.data), 1)

    def test_post_multiple(self):
        """ posts multiple objects """

        url = api_reverse('api-node_group:team_time_period_plan-list')

        data = [{
            'Team': Team.objects.last().id,
            'TimePeriod': TimePeriod.objects.first().id,
            'Plan': Plan.objects.last().id,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_multiple_validation_double(self):
        """ post multiple with validation error """

        url = api_reverse('api-node_group:team_time_period_plan-list')

        data = [{
            'Team': Team.objects.last().id,
            'TimePeriod': TimePeriod.objects.first().id,
            'Plan': Plan.objects.last().id,
        }, {
            'Team': self.second_team.id,
            'TimePeriod': TimePeriod.objects.first().id,
            'Plan': Plan.objects.last().id,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_multiple_team_and_time_period_already_have(self):
        """ post multiple with validation error """

        url = api_reverse('api-node_group:team_time_period_plan-list')

        data = [{
            'Team': Team.objects.first().id,
            'TimePeriod': TimePeriod.objects.first().id,
            'Plan': Plan.objects.first().id,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post(self):
        """ posts object """

        url = api_reverse('api-node_group:team_time_period_plan-list')

        data = {
            'Team': Team.objects.last().id,
            'TimePeriod': TimePeriod.objects.first().id,
            'Plan': Plan.objects.last().id,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_multiple(self):
        """ updates multiple objects """

        url = api_reverse('api-node_group:team_time_period_plan-list')

        data = [{
            'id': TeamTimePeriodPlan.objects.first().id,
            'Team': Team.objects.last().id,
            'TimePeriod': TimePeriod.objects.last().id,
            'Plan': Plan.objects.last().id,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_one(self):
        """ updates one object """

        url = api_reverse('api-node_group:team_time_period_plan-detail', kwargs={
            'id': TeamTimePeriodPlan.objects.first().id,
        })

        data = {
            'Team': Team.objects.last().id,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_multiple(self):
        """ deletes multiple objects """

        url = api_reverse('api-node_group:team_time_period_plan-list')

        data = [{
            'id': TeamTimePeriodPlan.objects.first().id,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.delete(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
