""" tests api team """

import io
import ipdb
import json
from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

from values.models import (
    Node,
    Tag,
)
from node_group.models import (Team)

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class TeamAPITestCase(APITestCase):

    """ test Team """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        Node.objects.create(**{
            'id': 1,
        })

        Tag.objects.create(**{'id': 8000})

        Team.objects.create(**{
            'name': 'Team 1',
            'color': '#ffffff',
        })

    def test_not_login(self):
        """ test when user isn't login """

        url = api_reverse('api-node_group:team-list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ if login """

        url = api_reverse('api-node_group:team-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_multiple(self):
        """ posts multiple objects """

        url = api_reverse('api-node_group:team-list')

        data = [{
            'name': 'зенит',
            'color': '#ffffff',
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post(self):
        """ posts object """

        url = api_reverse('api-node_group:team-list')

        data = {
            'name': 'зенит',
            'color': '#ffffff',
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_multiple(self):
        """ updates multiple objects """

        url = api_reverse('api-node_group:team-list')

        data = [{
            'id': Team.objects.first().id,
            'name': 'updated team name',
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_one(self):
        """ updates one object """

        url = api_reverse('api-node_group:team-detail', kwargs={
            'id': Team.objects.first().id,
        })

        data = {
            'name': 'updated team name',
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_multiple(self):
        """ deletes multiple objects """

        url = api_reverse('api-node_group:team-list')

        data = [{
            'id': Team.objects.first().id,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.delete(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
