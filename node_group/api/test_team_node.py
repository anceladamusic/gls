""" tests api teamnode """

import io
import ipdb
import json
from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

from values.models import (
    Node,
    Tag,
)
from node_group.models import (
    Team,
    TeamNode,
)

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class TeamNodeAPITestCase(APITestCase):

    """ test Team """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        Node.objects.create(**{
            'id': 1,
        })

        Node.objects.create(**{
            'id': 2,
        })

        Team.objects.create(**{
            'name': 'Team 1',
        })

        TeamNode.objects.create(**{
            'Team': Team.objects.first(),
            'Node': Node.objects.first(),
        })

    def test_not_login(self):
        """ test when user isn't login """

        url = api_reverse('api-node_group:team_node-list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ if login """

        url = api_reverse('api-node_group:team_node-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_multiple(self):
        """ posts multiple objects """

        url = api_reverse('api-node_group:team_node-list')

        data = [{
            'Node': Node.objects.last().id,
            'Team': Team.objects.first().id,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post(self):
        """ posts object """

        url = api_reverse('api-node_group:team_node-list')

        data = {
            'Node': Node.objects.last().id,
            'Team': Team.objects.first().id,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_detail(self):
        """ updates one object """

        url = api_reverse('api-node_group:team_node-detail', kwargs={
            'id': TeamNode.objects.first().id,
        })

        data = {
            'Node': Node.objects.last().id,
            'Team': Team.objects.first().id,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_multiple(self):
        """ deletes multiple objects """

        url = api_reverse('api-node_group:team_node-list')

        data = [{
            'id': TeamNode.objects.first().id,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.delete(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
