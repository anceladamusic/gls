
from rest_framework_bulk.routes import BulkRouter

from django.urls import path
from django.conf.urls import include

from .views import (
    UserGroupViewSet,
    UserGroupNodeViewSet,
    TeamViewSet,
    TeamNodeViewSet,
    TeamTimePeriodPlanViewSet,
    TimePeriodViewSet
)

router = BulkRouter()
router.register(r'user_group', UserGroupViewSet, 'user_group')
router.register(r'user_group_node',
                UserGroupNodeViewSet, 'user_group_node')
router.register(r'team', TeamViewSet, 'team')
router.register(r'team_node', TeamNodeViewSet, 'team_node')
router.register(r'time_period', TimePeriodViewSet, 'time_period')
router.register(r'team_time_period_plan',
                TeamTimePeriodPlanViewSet, 'team_time_period_plan')

urlpatterns = [
    path('api-node_group/', include((router.urls,
                                     'api-node_group'), namespace='api-node_group')),
]
