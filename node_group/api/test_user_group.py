""" tests UserGroup object """
import json
from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

from node_group.models import (
    UserGroup,
    UserGroupNode,
)
from values.models import (
    Tag,
    Node,
)
from tag_node.models import (
    TagProperties,
    NodeProperties,
)

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class UserGroupAPITestCase(APITestCase):

    """ test UserGroup """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        tag = Tag.objects.create(id=1)

        self.node = Node.objects.create(id=1)

        self.tag2 = Tag.objects.create(id=2)

        self.user_group = UserGroup.objects.create(**{
            'name': 'init user group',
            'User': user_obj,
        })

        UserGroup.objects.create(**{
            'name': 'another user group',
            'User': user_obj,
        })

        self.usergroupnode = UserGroupNode.objects.create(**{
            'UserGroup': self.user_group,
            'Node': self.node,
        })

    def tearDown(self):
        Tag.objects.all().delete()
        Node.objects.all().delete()

    def test_get_not_login(self):
        """ test when user isn't auth """

        url = api_reverse('api-node_group:user_group-list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_login(self):
        """ if login """

        url = api_reverse('api-node_group:user_group-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_with_params(self):
        """ if get with params """

        url = api_reverse('api-node_group:user_group-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = {'group_id': UserGroup.objects.first().id}

        response = self.client.get(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_multiple_post_without_node(self):
        """ posts multiple objects without nested """

        url = api_reverse('api-node_group:user_group-list')

        data = [{
            'name': 'зенит',
            'color': '#ffffff',
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_multiple_post_if_include_node(self):
        """ posts multiple objects with nested """

        url = api_reverse('api-node_group:user_group-list')

        data = [{
            'name': 'зенит',
            'color': '#ffffff',
            'usergroupnode_set': [
                {
                    'Node': {
                        'id': 2,
                        'props': {
                            'name': 'object no 2',
                            'description': 'desc 2',
                            'Tag': self.tag2.id,
                        }
                    },
                }
            ]
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_multiple(self):
        """ updates multiple objects """

        url = api_reverse('api-node_group:user_group-list')

        data = [{
            'id': self.user_group.id,
            'name': 'changed init user group',
            'usergroupnode_set': []
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_multiple(self):
        """ deletes multiple objects """

        url = api_reverse('api-node_group:user_group-list')

        data = [{
            'id': self.user_group.id,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.delete(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(UserGroup.objects.all()), 1)
