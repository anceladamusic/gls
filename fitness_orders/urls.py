
from django.urls import path
from .views import fitness_orders

urlpatterns = [
	path('fitness_orders/<int:landscape_id>/', fitness_orders, name='fitness_orders'),
]
