
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from django.forms import ValidationError
from django.conf import settings

MODELS_SIGNALS_IS_ACTIVE: bool = settings.MODELS_SIGNALS_IS_ACTIVE


class SpeedParameter(models.Model):
    """ sets speed parameters """

    class Meta:
        """ props """
        verbose_name = 'Параметр скорости'
        verbose_name_plural = 'Параметры скорости'

    walk = models.FloatField('Скорость ходьбы (минимум)', validators=[
                             MinValueValidator(0)], default=0)
    jog = models.FloatField('Скорость бега трусцой (минимум)', validators=[
                            MinValueValidator(0)], default=0.5)
    run = models.FloatField('Скорость бега (минимум)', validators=[
                            MinValueValidator(0)], default=1.5)
    hispeed = models.FloatField('Скорость быстрого бега (минимум)', validators=[
                                MinValueValidator(0)], default=5)
    sprint = models.FloatField('Скорость спринта (минимум)', validators=[
                               MinValueValidator(0)], default=9)
    user = models.ForeignKey(
        User,
        verbose_name='Пользователь',
        null=True,
        blank=True,
        on_delete=models.CASCADE
    )

    @staticmethod
    def get_table_type():
        """ adds table type to admin """
        return 'models.Model'

    def clean(self):
        """ compares walk and jog """
        if self.walk > self.jog:
            raise ValidationError({
                'walk': ':не должно быть более поля Скорость бега трусцой (минимум).',
            })
        if self.jog > self.run:
            raise ValidationError({
                'jog': ':не должно быть более поля Скорость бега (минимум).',
            })
        if self.run > self.hispeed:
            raise ValidationError({
                'run': ':не должно быть более поля Скорость быстрого бега (минимум).',
            })
        if self.hispeed > self.sprint:
            raise ValidationError({
                'hispeed': ':не должно быть более поля Скорость спринта (минимум).',
            })

    def as_sp_speed_parameters(self):
        """ sets sp speed props """
        return {
            'walk': {
                'min': self.walk,
                'max': set.jog,
            },
            'jog': {
                'min': self.jog,
                'max': self.run,
            },
            'run': {
                'min': self.run,
                'max': self.hispeed,
            },
            'hispeed': {
                'min': self.hispeed,
                'max': self.sprint,
            },
            'sprint': {
                'min': self.sprint,
                'max': 100,
            }
        }
