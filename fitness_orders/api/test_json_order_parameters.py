
""" api test """

import json
import datetime
import ipdb


from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

from mainapp.base import Base as MainappBase
from values.models import Node, TimePeriod
from scene.models import (
    LoadLandscape,
    Building,
)
from node_group.models import (
    UserGroup,
    UserGroupNode,
)

from ..models import SpeedParameter

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class JsonOrderParametersAPITestCase(APITestCase):

    """ test SpeedParameter """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        self.node = Node.objects.create(**{
            'id': 1
        })

        self.user_group = UserGroup.objects.create(User=user_obj)

        self.user_group_node = UserGroupNode.objects.create(
            UserGroup=self.user_group, Node=self.node)

        landscape = LoadLandscape.objects.create(**{
            "name": "сцена для тестирования взаимодействия ws-sp",
            "camera_position_x": 70.0,
            "camera_position_y": -80.0,
            "camera_position_z": 50.0,
            "camera_up_x": 0.0,
            "camera_up_y": 0.0,
            "camera_up_z": 1.0,
            "controls_target_x": 67.0,
            "controls_target_y": 40.0,
            "controls_target_z": 0.0,
            "dae_rotation_x": 3.14159265359,
            "dae_rotation_y": 3.14159265359,
            "dae_rotation_z": 3.14159265359,
            "dae_position_x": 76.639468457,
            "dae_position_y": 74.03660896,
            "dae_position_z": 6.0,
            "minx": 0.0,
            "miny": 0.0,
            "minz": 0.0,
            "maxx": 149.998046875,
            "maxy": 149.998046875,
            "maxz": 0.0,
            "circle_step_symbol": True,
            "get_wall_height_symbol": True,
            "light_target_symbol": True,
            "server_ip_address": "http://192.168.1.111:7123"
        })

        Building.objects.create(**{
            'LoadLandscape_id': landscape.id,
            'dae_name': 'building_001',
            'description': '',
            'maxx': 94.8243568411316,
            'maxy': 85.9966247679445,
            'maxz': 9.19401546975327,
            'minx': 46.6604423014814,
            'miny': 56.1367782085171,
            'minz': 0.010000229813152,
            'name': None,
            'zangle': 0.0,
        })

        self.speed_parameter = SpeedParameter.objects.create(**{
            'walk': 0,
            'jog': 0.5,
            'run': 1.5,
            'hispeed': 5,
            'sprint': 9,
            'user': user_obj,
        })

        TimePeriod.objects.create(**{
            'user': user,
            'landscape': landscape,
            'name': 'test',
            'type': TimePeriod.FIRST_PERIOD,
            'start': MainappBase().datetime_localize(datetime.datetime(2018, 9, 26, hour=16)),
            'end': MainappBase().datetime_localize(datetime.datetime(2018, 9, 26, hour=16, minute=45)),
        })

    def test_not_login(self):
        """ test when user isn't auth """

        url = api_reverse('json_order_parameters')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ if login """

        url = api_reverse('json_order_parameters')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code,
                         status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_post_wrong(self):
        """ posts order parameters with wrong structure"""

        url = api_reverse('json_order_parameters')

        data = {
            'selectedNodes': [92, 93],
            'selectedPeriods': [46],
            'selectedZone': {
                'type': 'building',
                'id': 1973,
            },
            'speedParameter': {
                'id': 1,
                'hispeed': 8,
                'jog': 0.7,
                'run': 1.5,
                'sprint': 9,
                'walk': 0.1,
            },
            'timeGroupBy': {
                'id': 1,
                'hispeed': 8,
                'jog': 0.7,
                'run': 1.5,
                'sprint': 9,
                'walk': 0.1,
            },
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post(self):
        """ posts order parameters with wrong structure """

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        url = api_reverse('api-node_group:user_group-list')

        response = self.client.get(url, {}, format='json')

        selected_groups = json.loads(json.dumps(response.data))

        url = api_reverse('json_order_parameters')

        speed_parameter = SpeedParameter.objects.first()

        data = {
            'selectedNodes': [node.id for node in Node.objects.all()],
            'selectedPeriods': [TimePeriod.objects.first().id],
            'selectedZone': {
                'type': 'building',
                'id': Building.objects.first().id,
            },
            'selectedGroups': selected_groups,
            'speedParameter': {
                'id': speed_parameter.id,
                'hispeed': speed_parameter.hispeed,
                'jog': speed_parameter.jog,
                'run': speed_parameter.run,
                'sprint': speed_parameter.sprint,
                'walk': speed_parameter.walk,
            },
            'timeGroupBy': 1,
        }

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
