
import ipdb

from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

from ..models import SpeedParameter

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class SpeedParameterAPITestCase(APITestCase):

    """ test SpeedParameter """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        self.speed_parameter = SpeedParameter.objects.create(**{
            'walk': 0,
            'jog': 0.5,
            'run': 1.5,
            'hispeed': 5,
            'sprint': 9,
            'user': user_obj,
        })

    def test_not_login(self):
        """ test when user isn't auth """

        url = api_reverse('api-fitness_orders:speed_parameter-list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ if login """

        url = api_reverse('api-fitness_orders:speed_parameter-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_multiple_post(self):
        """ posts multiple objects """

        url = api_reverse('api-fitness_orders:speed_parameter-list')

        data = [{
            'walk': 0.0,
            'jog': 0.6,
            'run': 1.5,
            'hispeed': 5.9,
            'sprint': 9,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_multiple_post_incorrect(self):
        """ posts multiple objects """

        url = api_reverse('api-fitness_orders:speed_parameter-list')

        data = [{
                'walk': 0.0,
                'jog': 0.6,
                'run': 0.5,
                'hispeed': 5.9,
                'sprint': 9,
                }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_patch_multiple(self):
        """ patches multiple objects """

        url = api_reverse('api-fitness_orders:speed_parameter-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = [{
            'id': SpeedParameter.objects.first().id,
            'walk': '0.5',
            'jog': 0.6,
            'run': 1.5,
            'hispeed': 5.9,
            'sprint': 12,
        }]

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_multiple(self):
        """ deletes multiple objects """

        url = api_reverse('api-fitness_orders:speed_parameter-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = [{
            'id': SpeedParameter.objects.first().id,
        }]

        response = self.client.delete(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
