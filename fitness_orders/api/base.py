
""" creates data for fitness_orders.api.views.py """
import ipdb
import json

from rest_framework.reverse import reverse as api_reverse
from rest_framework.test import APIClient
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class Base():
    """ creates data for fitness_orders.api.views.py """

    def __init__(self, **kwargs):
        self.user = User.objects.get(id=kwargs['request'].user.id)
        self.payload = payload_handler(self.user)
        self.token_rsp = encode_handler(self.payload)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

    def get_nodes_by_list_of_ids(self, ids):
        """ returns node objects by list_of_ids """

        url = api_reverse('api-values:node-list')

        data = {
            'ids': ids,
        }
        response = self.client.get(url, data, format='json')

        return json.loads(json.dumps(response.data['results']))
