
from rest_framework_bulk.routes import BulkRouter

from django.urls import path
from django.conf.urls import include

from .views import (
    SpeedParameterViewSet,
    json_order_parameters,
    sp_response_generator,
)

router = BulkRouter()
router.register(r'speed_parameter',
                SpeedParameterViewSet, 'speed_parameter')

urlpatterns = [
    path('api-fitness_orders/', include((router.urls,
                                         'api-fitness_orders'), namespace='api-fitness_orders')),
    path('api-fitness_orders/json_order_parameters/',
         json_order_parameters, name='json_order_parameters'),
    path('api-fitness_orders/sp_response_generator/',
         sp_response_generator, name='sp_response_generator'),
]
