
""" tests api sp_response_generator """

import json
import datetime
import ipdb


from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

from mainapp.base import Base as MainappBase

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class SpResponseGeneratorAPITestCase(APITestCase):

    """ tests SP response generator """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

    def test_not_login(self):
        """ test when user isn't auth """

        url = api_reverse('sp_response_generator')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ if login """

        url = api_reverse('sp_response_generator')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = {
            'start': 1537528896,
            'end': 1537528956,
            'planId': 1973,
            'speedParameter': {
                'id': 1,
                'hispeed': 8,
                'jog': 1.3,
                'run': 1.5,
                'sprint': 9,
                'walk': 0.2,
            },
            'nodes': [92, 93, 94, 95, 96, 97, 98],
        }

        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
