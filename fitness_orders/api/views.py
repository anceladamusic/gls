
""" api views """

import random
import time
import json
import datetime
import numpy
import ipdb

from pprint import pprint
from schema import Schema, Use, SchemaError
from rest_framework_bulk import BulkModelViewSet
from rest_framework.response import Response
from rest_framework.decorators import (
    api_view,
    permission_classes
)
from rest_framework.permissions import IsAuthenticated

from mainapp.rest_bulk_destroyer import RestBulkDestroyer
from mainapp.rest_permissions import IsOwnerOrReadOnly
from node_group.models import (TimePeriod)
from .serializers import SpeedParameterSerializer
from .base import Base as APIBase
from fitness_orders.models import SpeedParameter


class SpeedParameterViewSet(RestBulkDestroyer, BulkModelViewSet):

    """
    Speed parameter
    """

    lookup_field = 'id'
    serializer_class = SpeedParameterSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        return serializer.save(user=self.request.user)

    def get_queryset(self, *args, **kwargs):
        if getattr(self.request.user, 'is_authenticated'):
            speed_parameter = SpeedParameter.objects.all().filter(user=self.request.user)
            if not speed_parameter:
                speed_parameter = [
                    SpeedParameter.objects.create(user=self.request.user)]
            return speed_parameter


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def json_order_parameters(request):
    """
    converts request to json
    """

    params = json.loads(request.body.decode('utf-8'))

    schema = Schema({
        'selectedNodes': Use(list),
        'selectedGroups': Use(list),
        'selectedPeriods': Use(list),
        'selectedZone': Use(dict),
        'speedParameter': Use(dict),
        'timeGroupBy': Use(int),
    })

    error = None

    try:
        schema.validate(params)
    except SchemaError as e:
        error = e.__dict__['autos']

    if error:
        return Response(error, 400)

    time_periods = [time_period.as_dict() for time_period in TimePeriod.objects.filter(
        id__in=params['selectedPeriods'])]

    for time_period in time_periods:

        start = time.mktime(time_period['start'].timetuple())
        end = time.mktime(time_period['end'].timetuple())

        request_periods = []

        if params['timeGroupBy'] and params['timeGroupBy'] != 999:

            delta = datetime.timedelta(
                minutes=params['timeGroupBy']).total_seconds()

            for i in numpy.arange(start, end, delta):

                period = {
                    'nodes': params['selectedNodes'],
                    'planId': params['selectedZone']['id'],
                    'speedParameter': params['speedParameter']
                }
                if i+delta < end:
                    period['start'] = i
                    period['end'] = i + delta
                else:
                    period['start'] = i
                    period['end'] = end

                request_periods.append(period)
        else:

            request_periods.append({
                'nodes': params['selectedNodes'],
                'planId': params['selectedZone']['id'],
                'speedParameter': params['speedParameter'],
                'start': start,
                'end': end,
            })

        time_period['request_periods'] = request_periods

    # requests periods to sp

    # create state structure of results
    api_base = APIBase(**{'request': request})

    # gets node parameters of out of group nodes
    params['selectedNodes'] = api_base.get_nodes_by_list_of_ids(
        params['selectedNodes'])

    return Response({
        'selectedNodes': params['selectedNodes'],
        'selectedGroups': params['selectedGroups'],
        'periods': time_periods,
    }, 201)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def sp_response_generator(request):
    """
    generates jog walk run etc for nodes at timeperiod
    """

    params = json.loads(request.body.decode('utf-8'))

    schema = Schema({
        'start': Use(int),
        'end': Use(int),
        'nodes': Use(list),
        'planId': Use(int),
        'speedParameter': Use(dict),
    })

    error = None

    try:
        schema.validate(params)
    except SchemaError as e:
        error = e.__dict__['autos']

    if error:
        return Response(error, 400)

    distance = []

    for node in params['nodes']:
        distance.append({
            'node_id': node,
            'hispeed': random.uniform(0, 50),
            'jog': random.uniform(0, 50),
            'run': random.uniform(0, 50),
            'sprint': random.uniform(0, 50),
            'walk': random.uniform(0, 50),
        })

    time = []
    for node in params['nodes']:
        time.append({
            'node_id': node,
            'hispeed': random.uniform(0, params['end'] - params['start']),
            'jog': random.uniform(0, params['end'] - params['start']),
            'run': random.uniform(0, params['end'] - params['start']),
            'sprint': random.uniform(0, params['end'] - params['start']),
            'walk': random.uniform(0, params['end'] - params['start']),
        })

    response = {
        'start': params['start'],
        'end': params['end'],
        'planId': params['planId'],
        'distance': distance,
        'time': time,
    }

    return Response(response, status=201)
