
import ipdb


from rest_framework.serializers import (
    ModelSerializer,
    ValidationError,
)

from rest_framework_bulk import (
    BulkSerializerMixin,
    BulkListSerializer,
)
from ..models import SpeedParameter


class SpeedParameterSerializer(BulkSerializerMixin, ModelSerializer):
    """ serializes Speed parameter """

    class Meta:
        """ props """

        model = SpeedParameter
        list_serializer_class = BulkListSerializer
        fields = [
            field.name for field in SpeedParameter._meta.fields if field.name != 'user']
        read_only_fields = ['id', 'user']
        update_lookup_field = 'id'

    def validate(self, data):
        """ compares walk and jog """
        if 'walk' in data:
            if data['walk'] > data['jog']:
                raise ValidationError(
                    'Скорость ходьбы (минимум) не должно быть более Скорости бега трусцой (минимум).',
                )
        if 'jog' in data:
            if data['jog'] > data['run']:
                raise ValidationError(
                    'Скорость бега трусцой (минимум) не должно быть более Скорости бега (минимум).',
                )
        if 'run' in data:
            if data['run'] > data['hispeed']:
                raise ValidationError(
                    'Скорость бега (минимум) не должно быть более Скорости быстрого бега (минимум).',
                )
        if 'hispeed' in data:
            if data['hispeed'] > data['sprint']:
                raise ValidationError(
                    'Скорость быстрого бега (минимум) не должно быть более Скорости спринта (минимум).',
                )
        return data
