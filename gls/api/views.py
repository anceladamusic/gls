""" common API views of project """
import json
import ipdb

from rest_framework.decorators import (
    api_view,
    permission_classes,
)
from rest_framework.permissions import (
    IsAuthenticated,
    AllowAny,
)
from rest_framework.response import Response
from schema import (
    Schema,
    Use,
    SchemaError,
)

from django.contrib.sessions.models import Session
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone


@api_view(['GET'])
@permission_classes([AllowAny])
def session_is_alive(request):
    """
    CHeck if session is expired => true / false
    """

    if 'session_key' in request.GET:

        session_key = request.GET['session_key']

        try:

            s = Session.objects.get(pk=session_key)

            if (timezone.now() < s.expire_date):
                return Response('Session is alive', 200)
            else:
                return Response('Session is expired', 400)

        except ObjectDoesNotExist:
            return Response('Session does not exists', 400)

    else:
        return Response('No session key', 400)
