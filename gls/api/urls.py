""" API for common project """
from django.urls import path

from gls.api.views import (
    session_is_alive,
)

urlpatterns = [
    path('api-common/session_is_alive/',
         session_is_alive, name='session_is_alive'),
]
