""" test Session is alive """
import datetime
import json
import ipdb
from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status

from django.utils import timezone
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.sessions.models import Session


class SessionIsAliveAPITestCase(APITestCase):

    """ test session is alive """

    def setUp(self):

        s = SessionStore()
        s.create()
        self.alived_key = s.session_key

        s.create()
        self.expired_key = s.session_key

        expired = Session.objects.last()
        expired.expire_date = timezone.now() - datetime.timedelta(hours=1)
        expired.save()

        self.url = api_reverse('session_is_alive')

    def test_wrong_type(self):
        """ wrong type request """

        response = self.client.post(self.url, {}, format='json')

        self.assertEquals(response.status_code,
                          status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_get_without_param(self):
        """ without param """

        response = self.client.get(self.url, {})

        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_wrong_param(self):
        """ wrong param """

        data = {
            'session_key': 'kas;fdjakd'
        }

        response = self.client.get(self.url, data)

        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_alive(self):
        """ get """

        data = {
            'session_key': Session.objects.first().session_key,
        }

        response = self.client.get(self.url, data)

        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_get_expired(self):
        """ get """

        data = {
            'session_key': Session.objects.last().session_key,
        }

        response = self.client.get(self.url, data)

        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
