
from django.urls import path
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

    path('admin/', admin.site.urls),
    path('', include(('login.api.urls', 'login'), namespace='login')),
    path('', include('scene.api.urls')),
    path('', include('values.api.urls')),
    path('', include('node_group.api.urls')),
    path('', include('tag_node.api.urls')),
    path('', include('incomezonedefine.api.urls')),
    path('', include('fitness_orders.api.urls')),
    path('', include('mainapp.api.urls')),
    path('', include('gls.api.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
