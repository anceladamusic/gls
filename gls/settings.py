""" initial setting of the project """
import os
import datetime

# SIGNAL MODELS ALLOWED
MODELS_SIGNALS_IS_ACTIVE: bool = True

POSITIONAL_SERVER_IS_ACTIVE: bool = True
POSITIONAL_SERVER_HOST: str = 'http://192.168.1.111'
POSITIONAL_SERVER_PORT: int = 7123

AIOHTTP_SERVER_IS_ACTIVE: bool = True
AIOHTTP_SERVER_HOST: str = 'http://192.168.1.92'
AIOHTTP_SERVER_PORT: int = 5678

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'hn#v7s(bk&cvyqk+u2)fj=q-k75e6@0v=rkvk8*dgtko6@-7pj'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 'django_mysql',
    'django_mptt_admin',
    'django_cleanup',
    'django_extensions',
    'rest_framework',
    'corsheaders',
    'reset_migrations',
    'colorfield',
    # 'multiselectfield',
    'jsonify',

    'fitness_orders',
    'scene',
    'incomezonedefine',
    'login',
    'values',
    'node_group',
    'tag_node',
    'mainapp',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'gls.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
            os.path.join(BASE_DIR, 'media'),
            os.path.join(BASE_DIR, 'models'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'gls.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
        'PORT': '5432',
    },
}

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'gls2',
#         'HOST': '127.0.0.1',
#         'PORT': '3306',
#         'USER': 'gls2',
#         'PASSWORD': '115648',
#         'OPTIONS': {
#             'init_command': "SET sql_mode='STRICT_TRANS_TABLES', innodb_strict_mode=1",
#             'charset': 'utf8mb4',
#         },
#         'TEST': {
#             'CHARSET': 'utf8mb4',
#         }
#     },
# }


# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'ru-Ru'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

# STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# Media Files
MEDIA_URL = '/models/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'models')


ORDERS_FREE_LOCK_TIME = 0
ORDERS_REDIS_HOST = 'localhost'
ORDERS_REDIS_PORT = 6379
ORDERS_REDIS_PASSWORD = None
ORDERS_REDIS_DB = 0

# rest framework
REST_FRAMEWORK = {
    'DATE_FORMAT': '%d-%m-%Y',
    'DATE_INPUT_FORMATS': [('%d-%m-%Y'), ],
    'DATETIME_INPUT_FORMATS': [('%d-%m-%YT%H:%M:%S'), ],
    'UPLOADED_FILES_USE_URL': True,
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ],
    'DEFAULT_FILTER_BACKEND': ('django_filters.rest_framework.DjangoFilterBackend'),
    # 'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    # 'PAGE_SIZE': 100,
}

# custom jwt authorization
JWT_AUTH = {
    'JWT_RESPONSE_PAYLOAD_HANDLER': 'login.api.views.jwt_response_payload_handler',
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=14)
}


# enable get requests from all domains
CORS_ORIGIN_ALLOW_ALL = True
