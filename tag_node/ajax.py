import json
from django.template.loader import render_to_string
from django_bulk_update.helper import bulk_update
from django.http import JsonResponse
from scene.models import LoadLandscape
from values.models import *
from .models import *
from mainapp.base import Base as MainappBase

class Ajax():
	def __init__(self):
		self.commands = {
			'get_list_of_tags': {
				'command': 'listTags',
			},
			'save_tags': {
				'command': 'saveTags',
				'tags': [],
			},
			'delete_tags': {
				'command': 'deleteTags',
				'tags': [],
			},
			'get_unregistered_devices': {
				'command': 'listUnregisteredDevices',
			},
			'get_list_of_nodes': {
				'command': 'listNodes',
			},
			'update_nodes': {
				'command': 'updateNodes',
				'nodes': [],
			},
			'add_nodes': {
				'command': 'addNodes',
				'nodes': [],
			},
			'delete_nodes': {
				'command': 'deleteNodes',
				'nodes': [],
			}
		}
		self.default_tag = {
			'registered': False,
			'location_methods': 'rssi,gps',
			'sensors': 'buttonCall,buttonAccept,beeper,chargeLevel,accelerometer,gps,barometer',
			'move': 1000,
			'stay': 5000,
			'mode': 'motion',
			'max_speed_stay': 0.3,
			'max_speed_move': 1.5,
			'max_speed_run': 2.5,
			'smooth_coefficient': 0.5,
			'rf_short_addr': 0,
			'pan_id': 0,
			'timeslot': 0,
			'channel_rssi': 26,
			'channel_glonas': 25,
			'tx_power_level': 8,
			'firmware_version': 11,
			'used_sensors': 'buttonCall,chargeLevel',
		}
		self.default_node = {
			'name': '',
			'description': '',
			'tagId': 0,
		}

	def node_init(self, **properties):

		for key, value in self.default_node.items():
			if not key in properties:
				properties[key] = value

		node = {
			'id': properties['id'],
			'name': properties['name'],
			'description': properties['description'],
			'tagId': hex(int(properties['Tag'])),
		}
		return node

	def tag_init(self, **properties):

		for key, value in self.default_tag.items():
			if not key in properties:
				properties[key] = value

		tag = {
			'tagId': hex(properties['Tag']),
			'properties': {
				'registered': properties['registered'],
				'locationMethods': properties['location_methods'],
				'sensors': properties['sensors'],
				'timeUpdateLocation': {
					'move': properties['move'],
					'stay': properties['stay'],
				},
				'correctionFilter': {
					'mode': properties['mode'],
					'maxSpeedStay': properties['max_speed_stay'],
					'maxSpeedMove': properties['max_speed_move'],
					'maxSpeedRun': properties['max_speed_run'],
					'smoothCoefficient': properties['smooth_coefficient'],
				},
				'hardwareSettings': {
					'rfShortAddr': properties['rf_short_addr'],
					'panId': properties['pan_id'],
					'timeslot': properties['timeslot'],
					'channelRssi': properties['channel_rssi'],
					'channelGlonas': properties['channel_glonas'],
					'txPowerLevel': properties['tx_power_level'],
					'firmwareVesrsion': properties['firmware_vesrsion'],
					'usedSensors': properties['used_sensors'],
				}
			}
		}
		return tag

	def __update_node_properties(self, node):
		return NodeProperties(
			Node_id=node['id'],
			name=node['name'],
			description=node['description'],
			Tag_id=int(node['tagId'], 0),
		)

	def __update_tag_properties(self, tag):
		return TagProperties(
			Tag_id=int(tag['tagId'], 0),
			registered=tag['properties']['registered'],
			location_methods=tag['properties']['locationMethods'],
			sensors=tag['properties']['sensors'],
			move=tag['properties']['timeUpdateLocation']['move'],
			stay=tag['properties']['timeUpdateLocation']['stay'],
			mode=tag['properties']['correctionFilter']['mode'],
			max_speed_stay=tag['properties']['correctionFilter']['maxSpeedStay'],
			max_speed_move=tag['properties']['correctionFilter']['maxSpeedMove'],
			max_speed_run=tag['properties']['correctionFilter']['maxSpeedRun'],
			smooth_coefficient=tag['properties']['correctionFilter']['smoothCoefficient'],
			rf_short_addr=tag['properties']['hardwareSettings']['rfShortAddr'],
			pan_id=tag['properties']['hardwareSettings']['panId'],
			timeslot=tag['properties']['hardwareSettings']['timeslot'],
			channel_rssi=tag['properties']['hardwareSettings']['channelRssi'],
			channel_glonas=tag['properties']['hardwareSettings']['channelGlonas'],
			tx_power_level=tag['properties']['hardwareSettings']['txPowerLevel'],
			firmware_vesrsion=tag['properties']['hardwareSettings']['firmwareVesrsion'],
			used_sensors=tag['properties']['hardwareSettings']['usedSensors'],
		)

	def read_ajax(self, request):

		if isinstance(request, dict):
			string = request
			request_method = 'POST'
		else:
			string = json.loads(request.body.decode('utf-8'))
			request_method = request.method

		if request_method == 'POST':

			# удалить объект слежения
			if string['method'] == 'submit_delete_node':

				nodes = string['nodes']

				sp_ready = MainappBase().check_sp_connection()

				if sp_ready:

					command = self.commands['delete_nodes']
					command['nodes']=[{'id': node} for node in nodes]
					delete_nodes = MainappBase().request_to_sp(command)

					if delete_nodes['result']['status'] == 'ok':

						Node.objects.filter(id__in=nodes).delete()

						result = 'Люди(объекты) удалены'

					else:
						result = 'Во время людей(объектов) произошла ошибка'

					return JsonResponse({
						'sp_ready': sp_ready,
						'result': result,
					})

				else:

					return JsonResponse({
						'sp_ready': sp_ready,
					})
			# добавить объект слежения
			if string['method'] == 'add_node_submit':

				sp_ready = MainappBase().check_sp_connection()

				if sp_ready:

					properties = MainappBase().request_params_to_dict(string['properties'])

					form = NodePropertiesForm(properties)

					if form.is_valid():

						is_valid = True

						properties['id'] = -1

						node = self.node_init(**properties)

						self.commands['add_nodes']['nodes'].append(node)
						command = self.commands['add_nodes']

						add_nodes = MainappBase().request_to_sp(command)

						if add_nodes['result']['status'] == 'ok':

							new_node_id = add_nodes['result']['nodes'][0]['id']

							new_node = Node.objects.create(id=new_node_id)

							node_prop = NodeProperties.objects.create(
								Node_id=new_node.id,
								name=node['name'],
								description=node['description'],
								Tag_id=int(node['tagId'], 0),
							)

							node_line = render_to_string('node_line.html', {
								'node_prop': node_prop,
							})
							result = 'Человек(объект) добавлен'
						else:
							node_line = None
							result = 'Во время добавления человека(объекта) произошла ошибка'

					else:
						is_valid = False

						node_line = None

						result = render_to_string('add_node_form.html', {
							'form': form,
						})

					return JsonResponse({
						'sp_ready': sp_ready,
						'result': result,
						'is_valid': is_valid,
						'node_line': node_line,
					})
				else:
					return JsonResponse({
						'sp_ready': sp_ready,
					})
			# добавить метку
			if string['method'] == 'add_tag':

				sp_ready = MainappBase().check_sp_connection()

				if sp_ready:

					tag_id = int(string['tag_id'], 16)

					new_tag = Tag(TagId=tag_id)

					tag_line = render_to_string('tag_line.html', {
						'tag': new_tag,
					})

					tag_prop = TagProperties(Tag_id=new_tag)

					# запрос на sp добавить метку
					tag = self.tag_init(**tag_prop.as_dict())
					self.commands['save_tags']['tags'].append(tag)
					command = self.commands['save_tags']
					save_tags = MainappBase().request_to_sp(command)

					if save_tags['result']['status'] == 'ok':

						new_tag.save()
						tag_prop.save()

						result = 'Метка добавлена'

					else:
						result = 'Во время добавления метки произошла ошибка'

					return JsonResponse({
						'sp_ready': sp_ready,
						'result': result,
						'tag_line': tag_line,
					})

				else:
					return JsonResponse({
						'sp_ready': sp_ready,
					})
			# показать форму добавить объект слежения
			if string['method'] == 'add_node_form':

				sp_ready = MainappBase().check_sp_connection()

				if sp_ready:

					form = NodePropertiesForm()

					result = render_to_string('add_node_form.html', {
						'form': form,
					})

					return JsonResponse({
						'sp_ready': sp_ready,
						'result': result,
					})

				else:
					return JsonResponse({
						'sp_ready': sp_ready,
					})

			# показать форму добавить метку
			if string['method'] == 'add_tag_form':

				sp_ready = MainappBase().check_sp_connection()

				if sp_ready:

					command = self.commands['get_unregistered_devices']

					unregistered_devices = MainappBase().request_to_sp(command)

					result = render_to_string('add_tag_form.html', {
						'unregistered_devices': unregistered_devices['result']['devs'],
					})

					return JsonResponse({
						'sp_ready': sp_ready,
						'result': result,
					})

				else:
					return JsonResponse({
						'sp_ready': sp_ready,
					})


			# удалить метки
			if string['method'] == 'submit_delete':

				sp_ready = MainappBase().check_sp_connection()

				if sp_ready:

					tags = string['tags']

					for tag in tags:

						self.commands['delete_tags']['tags'].append({'tagId': hex(tag)})

					command = self.commands['delete_tags']

					delete_tags = MainappBase().request_to_sp(command)

					if delete_tags['result']['status'] == 'ok':

						TagProperties.objects.filter(Tag_id__in=tags).delete()

						result = 'Метки удалены'

					else:
						result = 'Во время удаления меток произошла ошибка'

					return JsonResponse({
						'sp_ready': sp_ready,
						'result': result,
					})

				else:
					return JsonResponse({
						'sp_ready': sp_ready,
					})
			if string['method'] == 'submit_node_properties':

				sp_ready = MainappBase().check_sp_connection()

				if sp_ready:

					if 'files' in string:

						image = string['files']['images']

					else:

						image = None

					node_id = string['node_id']

					properties = MainappBase().request_params_to_dict(string['properties'])

					# ищем уже связанного с таким tag_id объект слежения
					already_have = NodeProperties.objects.filter(Tag_id=properties['Tag'])

					#если есть стираем связь с меткой
					if already_have:

						node = Ajax().node_init(**already_have[0].as_dict())
						node['tagId'] = hex(0)

						self.commands['update_nodes']['nodes'].append(node)
						command = self.commands['update_nodes']
						update_node = MainappBase().request_to_sp(command)

						already_have.update(Tag_id=None)

					node_prop = NodeProperties.objects.filter(Node_id=node_id)
					properties['Node_id'] = node_id
					form = NodePropertiesForm(properties)

					if form.is_valid():

						properties['id'] = properties['Node_id']
						del properties['Node_id']

						node = Ajax().node_init(**properties)
						self.commands['update_nodes']['nodes'].append(node)
						command = self.commands['update_nodes']
						update_node = MainappBase().request_to_sp(command)

						if update_node['result']['status'] == 'ok':
							del properties['id']

							node_prop = NodeProperties.objects.filter(Node_id=node_id)

							node_prop.update(**properties)

							node_prop = NodeProperties.objects.get(Node_id=node_id)

							node_prop.image = image

							node_prop.save()

							is_valid = True

							result = 'Настройки человека(объекта) обновлены'

					else:

						is_valid = False

						result = render_to_string('node_properties.html', {
							'form': form,
							'node_id': node_id,
						})

					return JsonResponse({
						'sp_ready': sp_ready,
						'result': result,
						'is_valid': is_valid,
					})

				else:
					return JsonResponse({
						'sp_ready': sp_ready,
					})

			if string['method'] == 'submit_tag_properties':

				sp_ready = MainappBase().check_sp_connection()

				if sp_ready:
					tag_id = string['tag_id']

					properties = MainappBase().request_params_to_dict(string['properties'])

					tag_prop = TagProperties.objects.filter(Tag_id=tag_id)

					form = TagPropertiesForm(properties)

					if form.is_valid():

						properties['Tag'] = tag_id

						# обновляем на sp
						tag = Ajax().tag_init(**properties)
						self.commands['save_tags']['tags'].append(tag)
						command = self.commands['save_tags']
						save_tags = MainappBase().request_to_sp(command)

						if save_tags['result']['status'] == 'ok':

							tag_prop.update(**properties)

							is_valid = True

							result = 'Настройки метки обновлены'

					else:

						is_valid = False

						result = render_to_string('tag_properties.html', {
							'form': form,
							'tag_id': tag_id,
						})

					return JsonResponse({
						'sp_ready': sp_ready,
						'result': result,
						'is_valid': is_valid,
					})
				else:
					return JsonResponse({
						'sp_ready': sp_ready,
					})
			if string['method'] == 'get_node_properties':

				sp_ready = MainappBase().check_sp_connection()

				if sp_ready:

					node_id = string['node_id']

					node_prop = NodeProperties.objects.get(Node_id=node_id)

					form = NodePropertiesForm(instance=node_prop)

					result = render_to_string('node_properties.html', {
						'form': form,
						'node_id': node_id,
					})

					return JsonResponse({
						'sp_ready': sp_ready,
						'result': result,
					})

			if string['method'] == 'get_tag_properties':

				sp_ready = MainappBase().check_sp_connection()

				if sp_ready:

					tag_id = string['tag_id']

					tag_prop = TagProperties.objects.get(Tag_id=tag_id)

					form = TagPropertiesForm(instance=tag_prop)

					result = render_to_string('tag_properties.html', {
						'form': form,
						'tag_id': tag_id,
					})

					return JsonResponse({
						'sp_ready': sp_ready,
						'result': result,
					})

				else:

					return JsonResponse({
						'sp_ready': sp_ready,
					})
			if string['method'] == 'get_list_of_nodes':

				sp_ready = MainappBase().check_sp_connection()

				if sp_ready:

					nodes = MainappBase().request_to_sp(self.commands[string['method']]
						)['result']['nodes']

					result = render_to_string('node_result.html', {
						'nodes': nodes,
						'nodes_json': json.dumps(nodes),
					})

					return JsonResponse({
						'result': result,
						'sp_ready': sp_ready,
						'nodes': nodes,
					})

				else:
					return JsonResponse({
						'sp_ready': sp_ready,
					})

			if string['method'] == 'get_list_of_tags':

				sp_ready = MainappBase().check_sp_connection()

				if sp_ready:

					tags = MainappBase().request_to_sp(self.commands[string['method']])['result']['tags']

					result = render_to_string('tag_result.html', {
						'tags': tags,
						'tags_json': json.dumps(tags),
					})

					return JsonResponse({
						'result': result,
						'sp_ready': sp_ready,
						'tags': tags,
					})

				else:
					return JsonResponse({
						'sp_ready': sp_ready,
					})
			# загрузить все люди(объекты) из сервера позиционирования
			if string['method'] == 'load_all_nodes_to_web':

				sp_ready = MainappBase().check_sp_connection()

				if sp_ready:

					list_of_nodes = MainappBase().request_to_sp(
						self.commands['get_list_of_nodes']
					)['result']['nodes']

					# удаляем только имеющиеся на сервере позиционирования
					to_delete = Node.objects.exclude(
						id__in=[node['id'] for node in list_of_nodes]).delete()

					already_have = Node.objects.filter(
						id__in=[node['id'] for node in list_of_nodes])

					must_be_created = [node for node in list_of_nodes if not any(
						alh_node.id == node['id'] for alh_node in already_have)]

					new_nodes = []
					for node in must_be_created:
						new_nodes.append(Node(id=node['id']))

					Node.objects.bulk_create(new_nodes)

					# properties

					# исключаем записи, не имеющие привязки к tag
					list_of_nodes = [node for node in list_of_nodes if int(node['tagId'], 0) != 0]

					already_have = NodeProperties.objects.filter(
						Node_id__in=[node['id'] for node in list_of_nodes])

					must_be_created = [node for node in list_of_nodes
					 if not any(alh_node.Node_id == node['id'] for alh_node in already_have)]

					new_prop = []
					for node in must_be_created:
						new_prop.append(self.__update_node_properties(node))

					if new_prop:
						NodeProperties.objects.bulk_create(new_prop)

					updated_prop = []
					for node in already_have:
						node_sp = [n for n in list_of_nodes if n['id'] == node.Node_id]
						if node_sp:
							updated_prop.append(self.__update_node_properties(node_sp[0]))

					if updated_prop:
						bulk_update(updated_prop)

					text = 'Новых людей(объектов):{0}. Обновлено людей(объектов):{1}.'.format(
						len(new_prop), len(updated_prop))

					return JsonResponse({
						'sp_ready': sp_ready,
						'result': text,
					})

				else:
					return JsonResponse({
						'sp_ready': sp_ready,
					})

			# загрузить все метки из сервера позиционирования
			if string['method'] == 'load_all_to_web':

				sp_ready = MainappBase().check_sp_connection()

				if sp_ready:

					list_of_tags = MainappBase().request_to_sp(
						self.commands['get_list_of_tags'])['result']

					# удаляем иеющиеся только на сервере позиционирования
					to_delete = Tag.objects.exclude(
						id__in=[int(tag['tagId'], 0) for tag in list_of_tags['tags']]).delete()

					already_have = Tag.objects.filter(
						id__in=[int(tag['tagId'], 0) for tag in list_of_tags['tags']])

					must_be_created = [tag for tag in list_of_tags['tags'
						] if not any(alh_tag.id == int(tag['tagId'], 0) for alh_tag in already_have)]

					new_tags = []

					for tag in must_be_created:
						new_tags.append(Tag(TagId=int(tag['tagId'], 0)))

					Tag.objects.bulk_create(new_tags)

					#properties
					already_have = TagProperties.objects.filter(
						Tag_id__in=[int(tag['tagId'], 0) for tag in list_of_tags['tags']])

					must_be_created = [tag for tag in list_of_tags['tags'
						] if not any(alh_tag.Tag_id == int(tag['tagId'], 0) for alh_tag in already_have)]

					new_prop = []
					for tag in must_be_created:
						new_prop.append(self.__update_tag_properties(tag))

					if new_prop:
						TagProperties.objects.bulk_create(new_prop)

					updated_prop = []
					for tag in already_have:
						tag_sp = [t for t in list_of_tags['tags'] if int(t['tagId'], 0) == tag.Tag_id]
						if tag_sp:
							updated_prop.append(self.__update_tag_properties(tag_sp[0]))

					if updated_prop:
						bulk_update(updated_prop)

					text = 'Новых меток:{0}. Обновлено меток:{1}.'.format(len(new_prop), len(updated_prop))

					return JsonResponse({
						'sp_ready': sp_ready,
						'result': text,
					})
				else:
					return JsonResponse({
						'sp_ready': sp_ready,
					})