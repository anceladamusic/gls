""" serializers tag and node properties to serve api requests """
import ipdb
from typing import (Optional)
from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
    ImageField,
)
from rest_framework_bulk import (
    BulkSerializerMixin,
    BulkListSerializer,
)

from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist

from tag_node.models import (
    TagProperties,
    NodeProperties,
)
from mainapp.models import Image
from mainapp.api.serializers import ImageSerializer


class TagPropertiesSerializer(BulkSerializerMixin, ModelSerializer):
    """ serves tagproperties model objects """

    class Meta:
        """ props """

        model = TagProperties
        list_serializer_class = BulkListSerializer
        update_lookup_field = 'id'
        fields = [field.name for field in TagProperties._meta.fields]
        read_only_fields = ['id', 'Tag']


class NodePropertiesSerializer(BulkSerializerMixin, ModelSerializer):
    """ serves nodeproperties model objects """

    content_type = SerializerMethodField(required=False)
    image = SerializerMethodField(required=False)

    @staticmethod
    def get_content_type(node_property: NodeProperties):
        c_type = ContentType.objects.get_for_models(node_property)
        return c_type[node_property].id

    @staticmethod
    def get_image(node_property: NodeProperties) -> Optional[dict]:
        c_type = ContentType.objects.get_for_models(node_property)
        try:
            image = Image.objects.get(
                content_type=c_type[node_property], object_id=node_property.Node.id)
            serializer = ImageSerializer(instance=image, many=False)
            return serializer.data
        except ObjectDoesNotExist:
            return None

    class Meta:
        """ props """

        model = NodeProperties
        list_serializer_class = BulkListSerializer
        fields = [field.name for field in NodeProperties._meta.fields] + \
            ['image', 'content_type']
        update_lookup_field = 'id'
        read_only_fields = ['id', 'Node']
