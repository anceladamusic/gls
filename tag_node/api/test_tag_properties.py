""" UPDATE or GET only(post and delete serves from Tag object)! test tag properties object """
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

from values.models import Tag
from tag_node.models import TagProperties

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class TagPropertiesAPITestCase(APITestCase):
    """ test TagProperties """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        Tag.objects.create(**{
            'id': 8000,
        })

    def tearDown(self):

        Tag.objects.all().delete()

    def test_not_login(self):
        """ test when user isn't login """

        url = api_reverse('api-tag_node:tag_properties-list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ if login """

        url = api_reverse('api-tag_node:tag_properties-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_multiple(self):
        """ updates multiple objects """

        url = api_reverse('api-tag_node:tag_properties-list')

        data = [{
            'id': TagProperties.objects.first().id,
            'registered': False,
        }]

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
