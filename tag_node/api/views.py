""" serves NodeProperty TagProperty models """
import requests
import ipdb
import json
from pprint import pprint
from typing import (
    List,
    Optional,
    Sequence,
)
from rest_framework.request import Request
from rest_framework.permissions import IsAuthenticated
from rest_framework_bulk import BulkModelViewSet
from requests.exceptions import ConnectionError


from django.conf import settings

from mainapp.rest_bulk_destroyer import RestBulkDestroyer
from tag_node.models import TagProperties, NodeProperties

from .serializers import (
    TagPropertiesSerializer,
    NodePropertiesSerializer,
)


class TagPropertiesViewSet(RestBulkDestroyer, BulkModelViewSet):
    """ creates tag_property model rest viewset """

    lookup_field = 'id'
    serializer_class = TagPropertiesSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        return TagProperties.objects.all()


class NodePropertiesViewSet(RestBulkDestroyer, BulkModelViewSet):
    """ creates node_property model rest viewset """

    lookup_field = 'id'
    serializer_class = NodePropertiesSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs) -> List[NodeProperties]:
        """get list of NodeProperties objects

        Returns:
            [NodeProperty] -- List of nodeproperty objects
        """
        request: Request = self.request

        not_in_groups: str = request.query_params.get('not_in_groups', None)

        if not_in_groups:
            return NodeProperties.objects.filter(Node__usergroupnode__isnull=bool(not_in_groups))

        node_ids: Optional[str] = request.query_params.get(
            'node_ids', None)

        if node_ids:

            node_properties: List[NodeProperties] = NodeProperties.objects.filter(
                Node_id__in=json.loads(node_ids))

            return node_properties

        return NodeProperties.objects.all()
