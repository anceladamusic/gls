
""" tests api node properties """

import io
import ipdb
import json
from PIL import Image as PILImage
from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings
from rest_framework.response import Response

from django.core.files.images import ImageFile
from django.contrib.auth.models import User

from tag_node.models import NodeProperties
from values.models import (
    Node,
    Tag,
)

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class NodePropertiesAPITestCase(APITestCase):

    """ test NodeProperties """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        Tag.objects.create(**{'id': 8000})

        Node.objects.create(**{
            'id': 1,
        })

        Node.objects.create(**{
            'id': 2,
        })

    def tearDown(self):

        nodes = Node.objects.all()
        nodes.delete()

        tag = Tag.objects.first()
        tag.delete()

    def test_not_login(self):
        """ test when user isn't login """

        url = api_reverse('api-tag_node:node_properties-list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ if login """

        url = api_reverse('api-tag_node:node_properties-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_with_params(self):
        """ if param """

        url = api_reverse('api-tag_node:node_properties-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = {
            'not_in_groups': True
        }

        response = self.client.get(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_with_second_param(self):
        """ if second param """

        url = api_reverse('api-tag_node:node_properties-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        data = {
            'node_ids': str([Node.objects.first().id, Node.objects.last().id]),
        }

        response: Response = self.client.get(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_detail(self):
        """ patch single"""

        url = api_reverse('api-tag_node:node_properties-detail', kwargs={
            'id': NodeProperties.objects.first().id,
        })

        data = {
            'name': 'new name',
            'description': 'new description',
            'Tag': Tag.objects.first().id,
        }

        self.client.credentials(
            HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
