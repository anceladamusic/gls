""" routers to REST API objects"""
from rest_framework_bulk.routes import BulkRouter

from django.urls import path
from django.conf.urls import include

from .views import (
    TagPropertiesViewSet,
    NodePropertiesViewSet,
)

router = BulkRouter()
router.register(r'tag_properties', TagPropertiesViewSet, 'tag_properties')
router.register(r'node_properties', NodePropertiesViewSet, 'node_properties')

urlpatterns = [
    path('api-tag_node/', include((router.urls,
                                   'api-tag_node'), namespace='api-tag_node')),
]
