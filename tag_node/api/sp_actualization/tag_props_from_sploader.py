""" import tag properties from Positional server to my API REST WEB """

import ipdb
import requests
from pprint import pprint

from rest_framework_jwt.settings import api_settings

from django.contrib.auth import settings
from django.contrib.auth.models import User

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER

positional_server_host = settings.POSITIONAL_SERVER_HOST
positional_server_port = settings.POSITIONAL_SERVER_PORT


data = {
    'command': 'listTags'
}

response = requests.post(
    f'{positional_server_host}:{positional_server_port}', json=data)

result = response.json()

if result['tags']:

    user = User.objects.get(username='ancel')

    payload = payload_handler(user)

    token_rsp = encode_handler(payload)

    tags = result['tags']

    tag_props_url = 'http://192.168.1.92:8000/api-tag_node/tag_properties/'

    tag_props_data = [{
        'Tag': int(tag['tagId'], 0),
        'registered': tag['properties']['registered'],
    } for tag in tags]

    headers = {
        'Authorization': f'JWT {token_rsp}'
    }

    response = requests.post(
        tag_props_url, json=tag_props_data, headers=headers)
