""" import node properties from Positional server to my WEB REST API """

import ipdb
import requests
from pprint import pprint

from rest_framework_jwt.settings import api_settings

from django.contrib.auth import settings
from django.contrib.auth.models import User

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER

positional_server_host = settings.POSITIONAL_SERVER_HOST
positional_server_port = settings.POSITIONAL_SERVER_PORT


data = {
    'command': 'listNodes'
}

response = requests.post(
    f'{positional_server_host}:{positional_server_port}', json=data)

result = response.json()

if result['nodes']:

    user = User.objects.get(username='ancel')

    payload = payload_handler(user)

    token_rsp = encode_handler(payload)

    nodes = result['nodes']

    node_props_url = 'http://192.168.1.92:8000/api-tag_node/node_properties/'

    node_props_data = [{
        'Node': node['id'],
        'Tag': int(node['tagId'], 0),
        'name': node['name'],
        'description': node['description'],
    } for node in nodes if int(node['tagId'], 0) != 0]

    node_props_data += [{
        'Node': node['id'],
        'Tag': None,
        'name': node['name'],
        'description': node['description'],
    } for node in nodes if int(node['tagId'], 0) == 0]

    headers = {
        'Authorization': f'JWT {token_rsp}'
    }

    response = requests.post(
        node_props_url, json=node_props_data, headers=headers)
