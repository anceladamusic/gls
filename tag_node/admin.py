""" serves all models in app"""
from django.apps import apps
from django.contrib import admin
from .models import *


# class UserGroupNodeInline(admin.TabularInline):
#     model = UserGroupNode
#     extra = 0


class Inlines():
    """ admin inlines"""

    def __init__(self):
        self.model = {
            # 'node_group.usergroup': [
            #     UserGroupNodeInline,
            # ],
        }


app_models = apps.get_app_config('tag_node').get_models()

for model in app_models:

    class ModelAdmin(admin.ModelAdmin):
        """ register user models """
        list_display = [field.name for field in model._meta.fields]

        if str(model._meta) in Inlines().model:

            inlines = Inlines().model[str(model._meta)]

    admin.site.register(model, ModelAdmin)
