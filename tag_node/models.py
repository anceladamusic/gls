""" nodeproperties, tagproperties, usernode objects"""
import ipdb
import requests
import json
import datetime

from typing import (
    Optional,
    Dict,
    Any,
    Union,
    NewType,
    Sequence,
    Mapping,
)
from pprint import pprint
from rest_framework.reverse import reverse as api_reverse
from rest_framework.response import Response

from django.core.exceptions import ObjectDoesNotExist
from django import forms
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import (
    post_save,
    pre_save,
)

from values.models import (
    Node,
    Tag,
)


HOST: str = settings.POSITIONAL_SERVER_HOST
PORT: int = settings.POSITIONAL_SERVER_PORT
ACTIVE: bool = settings.POSITIONAL_SERVER_IS_ACTIVE

MODELS_SIGNALS_IS_ACTIVE: bool = settings.MODELS_SIGNALS_IS_ACTIVE


class NodeProperties(models.Model):
    """ sets node parameters """

    LINE_CHOICES = (
        ('ГК', 'ГК'),
        ('ЗЩ', 'ЗЩ'),
        ('ПЗ', 'ПЗ'),
        ('НП', 'НП'),
    )

    GENDER_CHOICES = (
        ('Мужской', 'Мужской'),
        ('Женский', 'Женский'),
    )

    class Meta():
        """ props """
        verbose_name = 'Настройка объекта слежения'
        verbose_name_plural = 'Настройки объекта слежения'

    Node = models.OneToOneField(
        Node,
        null=True,
        on_delete=models.CASCADE,
        verbose_name='Нода',
    )
    name: Optional[str] = models.CharField(
        'Наименование', max_length=200, default='', blank=True)
    description: Optional[str] = models.TextField(
        'Описание', default='', blank=True)
    first_name: str = models.CharField(
        'Имя игрока', max_length=50, blank=True)
    last_name: str = models.CharField(
        'Фамилия игрока', max_length=50)
    patronymic: str = models.CharField(
        'Отчество игрока', max_length=50, default='', blank=True)
    line: str = models.CharField('Амплуа игрока', max_length=200,
                                 choices=LINE_CHOICES, default='HB')
    player_number: int = models.IntegerField(
        'Номер игрока', null=True, blank=True)
    date_of_birth: datetime.date = models.DateField(
        'Дата рождения игрока', default=datetime.date.today()-datetime.timedelta(days=365*18))
    gender: str = models.CharField(
        'Пол игрока', max_length=200, choices=GENDER_CHOICES, default='MALE')
    height: float = models.FloatField('Рост игрока', default=175, validators=[
                                      MinValueValidator(100), MaxValueValidator(300)])
    weight: float = models.FloatField('Вес игрока', default=80, validators=[
                                      MinValueValidator(30), MaxValueValidator(150)])
    Tag = models.ForeignKey(
        Tag,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name='Mетка',
    )

    def __str__(
        self) -> str: return f'{self.last_name} {self.first_name} {self.patronymic}'

    def get_node(self) -> Dict[str, Any]:
        """ Converts to POSITIONAL_SERVER Node object
        """

        return {
            'id': self.Node.id,
            'name': self.name,
            'description': self.description,
            'tagId': hex(self.Tag.id) if self.Tag else None,
        }


@receiver(pre_save, sender=NodeProperties)
def delete_old_node_property(sender: NodeProperties, instance: NodeProperties, **kwargs) -> None:
    """ removes if exists connections with tagId with some nodes to make new node connection with tagId possible

    Arguments:
        instance {NodeProperties} -- model class
    """
    if MODELS_SIGNALS_IS_ACTIVE:
        try:
            node_property: Optional[NodeProperties] = NodeProperties.objects.get(
                Tag=instance.Tag) if instance.Tag else None

            if node_property:
                node_property.Tag = None
                node_property.save()

        except ObjectDoesNotExist:
            pass


@receiver(post_save, sender=NodeProperties)
def update_nodeproperties(sender: NodeProperties, instance: NodeProperties, created: bool, **kwargs) -> None:
    """ Updates NodeProperties object in PISITIONAL_SERVER

    Arguments:
        instance {NodeProperties} -- model class
        created {bool} -- The actual instance being saved

    """
    if MODELS_SIGNALS_IS_ACTIVE:
        if ACTIVE:

            if not created:
                data = {
                    'command': 'updateNodes',
                    'nodes': [instance.get_node()],
                }

                try:

                    response = requests.post(
                        f'{HOST}:{PORT}',
                        json.dumps(data),
                    )

                except ConnectionError:
                    pprint('no updated nodeproperties in SP')


class TagProperties(models.Model):
    """ tag objects """

    class Meta():
        """ props """
        verbose_name = 'Настройка метки'
        verbose_name_plural = 'Настройки меток'

    RSSI = 'rssi'
    GPS = 'gps'

    LOCATION_METHOD_CHOICES = (
        (RSSI, 'rssi'),
        (GPS, 'gps'),
    )

    LOCATION_METHOD_DEFAULT = '{0},{1}'.format(RSSI, GPS)

    BUTTON_CALL = 'buttonCall'
    BUTTON_ACCEPT = 'buttonAccept'
    BEEPER = 'beeper'
    CHARGE_LEVEL = 'chargeLevel'
    ACCELEROMETER = 'accelerometer'
    BAROMETER = 'barometer'

    SENSORS_CHOICES = (
        (BUTTON_CALL, 'buttonCall'),
        (BUTTON_ACCEPT, 'buttonAccept'),
        (BEEPER, 'beeper'),
        (CHARGE_LEVEL, 'chargeLevel'),
        (ACCELEROMETER, 'accelerometer'),
        (GPS, 'gps'),
        (BAROMETER, 'barometer'),
    )

    SENSORS_DEFAULT = '{0},{1},{2},{3},{4},{5},{6}'.format(BUTTON_CALL, BUTTON_ACCEPT, BEEPER,
                                                           CHARGE_LEVEL, ACCELEROMETER, GPS, BAROMETER)

    USED_SENSORS = '{0},{1},{2},{3},{4}'.format(BUTTON_CALL, BUTTON_ACCEPT, BEEPER,
                                                CHARGE_LEVEL, ACCELEROMETER)

    Tag = models.OneToOneField(
        Tag,
        on_delete=models.CASCADE,
        # null=True,
        # blank=True,
        verbose_name='метка',
    )
    registered: bool = models.BooleanField('Зарегистрирована', default=True)
    location_methods = models.CharField(
        'Методы определения положения',
        max_length=200,
        null=True,
        blank=True,
        default=LOCATION_METHOD_DEFAULT,
    )
    sensors: str = models.CharField('Сенсоры', max_length=200, null=True, blank=True,
                                    default=SENSORS_DEFAULT)
    move: int = models.IntegerField('Идет', default=1000, validators=[
        MinValueValidator(0)])
    stay: int = models.IntegerField('Стоит', default=5000, validators=[
        MinValueValidator(0)])
    mode: str = models.CharField('Режим', max_length=20, default="motion")
    max_speed_stay: float = models.FloatField('Максимальная скорость при "Стоит"',
                                              default=0.3, validators=[MinValueValidator(0.0)])
    max_speed_move: float = models.FloatField('Максимальная скорость при "Идет"',
                                              default=1.5, validators=[MinValueValidator(0.0)])
    max_speed_run: float = models.FloatField('Максимальная скорость при "Бежит"',
                                             default=2.5, validators=[MinValueValidator(0.0)])
    smooth_coefficient: float = models.FloatField('Коэфициент "Сглаживания"',
                                                  default=0.5, validators=[MinValueValidator(0.0)])
    rf_short_addr: int = models.IntegerField('Адрес RF',
                                             default=0, validators=[MinValueValidator(0)])
    pan_id: int = models.IntegerField('Идентификатор "pan id"',
                                      default=452, validators=[MinValueValidator(0)])
    timeslot: int = models.IntegerField('Слот памяти "timeslot"',
                                        default=0, validators=[MinValueValidator(0)])
    channel_rssi: int = models.IntegerField('Канал "RSSI"', default=26, validators=[
        MinValueValidator(0), MaxValueValidator(26)])
    channel_glonas: int = models.IntegerField('Канал "GLONAS"', default=25, validators=[
        MinValueValidator(0), MaxValueValidator(26)])
    tx_power_level: int = models.IntegerField('Уровень "tx power"', default=-4, validators=[
        MinValueValidator(-22), MaxValueValidator(5)])
    firmware_vesrsion: int = models.IntegerField('Версия ПО',
                                                 default=10, validators=[MinValueValidator(0)])
    used_sensors: str = models.CharField('Используемые сенсоры', max_length=200, null=True, blank=True,
                                         default=USED_SENSORS)

    def __str__(self) -> str: return f'{self.Tag}'

    def as_sp_props(self) -> Dict[str, Any]:
        return {
            'registered': self.registered,
            'locationMethods': self.location_methods,
            'sensors': self.sensors,
            'timeUpdateLocation': {
                'move': self.move,
                'stay': self.stay,
            },
            'correctionFileter': {
                'mode': self.mode,
                'maxSpeedStay': self.max_speed_stay,
                'maxSpeedMove': self.max_speed_move,
                'maxSpeedRun': self.max_speed_run,
                'smoothCoefficient': self.smooth_coefficient,
            },
            'hardwareSettings': {
                'rfShortAddr': self.rf_short_addr,
                'panId': self.pan_id,
                'timeslot': self.timeslot,
                'channelRssi': self.channel_rssi,
                'channelGlonas': self.channel_glonas,
                'txPowerLevel': self.tx_power_level,
                'firmwareVesrsion': self.firmware_vesrsion,
                'usedSensors': self.used_sensors,
            }
        }

    def get_tag(self) -> Dict[str, Any]:
        """ Converts to POSITIONAL_SERVER Tag object

        Returns:
            Dict[str, Any] -- Positional server Tag object
        """

        return {
            'tagId': hex(self.Tag.id),
            'properties': self.as_sp_props(),
        }


@receiver(post_save, sender=TagProperties)
def update_tag_properties(sender: TagProperties, instance: TagProperties, created: bool, **kwargs) -> None:
    """Updates TagProperties object in POSITIONAL_SERVER

    Arguments:

        sender {TagProperties} -- The model object

        instance {TagProperties} -- The Actual instance being saved

        created {bool} -- object is created flag

    Returns:
        None
    """
    if MODELS_SIGNALS_IS_ACTIVE:

        if ACTIVE:

            if not created:
                data = {
                    'command': 'saveTags',
                    'tags': [instance.get_tag()],
                }

                try:

                    response = requests.post(
                        f'{HOST}:{PORT}',
                        json.dumps(data),
                    )

                except ConnectionError:
                    pprint('no updated Tag in SP')
