""" serves api rest delete multiple queryset"""

import ipdb
import json

from rest_framework.exceptions import ValidationError
from rest_framework.response import Response


class RestBulkDestroyer():

    """ adds rest bulk delete to rest model view """

    def allow_bulk_destroy(self, qs, filtered):
        """ destroyes miltiple """

        ids = [obj['id'] for obj in json.loads(self.request.body.decode())]

        filtered = filtered.filter(id__in=ids)

        if not filtered:
            raise ValidationError(
                detail='Could not find any objects to delete', code=400)

        return filtered
