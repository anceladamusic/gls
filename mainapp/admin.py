
from django.apps import apps
from django.contrib import admin
from django.contrib.sessions.models import Session

from .models import *


class SessionAdmin(admin.ModelAdmin):
    def _session_data(self, obj):
        return obj.get_decoded()
    list_display = ['session_key', '_session_data', 'expire_date']


admin.site.register(Session, SessionAdmin)

# class VerticesBuildingInline(admin.TabularInline):
# 	model = VerticesBuilding
# 	extra = 0


class Inlines():

    def __init__(self):
        self.model = {
            # 'scene.verticesbuilding': [
            # 	VerticesBuildingInline,
            # ],
        }


app_models = apps.get_app_config('mainapp').get_models()

for model in app_models:

    class ModelAdmin(admin.ModelAdmin):

        list_display = [field.name for field in model._meta.fields]

        if str(model._meta) in Inlines().model:

            inlines = Inlines().model[str(model._meta)]

    admin.site.register(model, ModelAdmin)
