import os
import requests
import json
import datetime
import pytz
import ipdb

from shapely.geometry import Polygon, MultiPoint
from urllib.parse import parse_qs
from datetime import timedelta
import dateutil.parser as dateparser

from django.utils import timezone
from django.contrib import auth
from django.conf import settings
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile

from scene.models import *
from incomezonedefine.models import *
from node_group.models import *

django_timezone = settings.TIME_ZONE
timezone.activate(pytz.timezone(django_timezone))
current_tz = timezone.get_current_timezone()


class Base():

    def __init__(self):
        self.django_timezone = settings.TIME_ZONE

    def datetime_localize(self, date_or_datetime):

        timezone.activate(pytz.timezone(self.django_timezone))

        current_tz = timezone.get_current_timezone()

        return current_tz.localize(date_or_datetime+datetime.timedelta(hours=3))
