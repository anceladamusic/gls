""" mainapp objects and models routes """
from rest_framework_bulk.routes import BulkRouter

from django.urls import path
from django.conf.urls import include

from .views import (
    session_list,
    ImageViewSet,
)

router = BulkRouter()
router.register(r'image', ImageViewSet, 'image')

urlpatterns = [
    path('api-mainapp/session_list/', session_list, name='session_list'),
    path('api-mainapp/', include((router.urls, 'api-mainapp'), namespace='api-mainapp')),
]
