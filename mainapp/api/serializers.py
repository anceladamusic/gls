""" mainapp models serializer """
from rest_framework.serializers import ModelSerializer
from rest_framework_bulk import (BulkSerializerMixin, BulkListSerializer)

from mainapp.models import Image


class ImageSerializer(BulkSerializerMixin, ModelSerializer):

    """ Image """

    class Meta:
        model = Image
        list_serializer_class = BulkListSerializer
        fields = [
            field.name for field in Image._meta.fields if field.name != 'owner']
        read_only_fields = ['id']
        update_lookup_field = 'id'
