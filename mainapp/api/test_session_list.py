
""" api test """

import json
import ipdb


from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User
from django.contrib.sessions.backends.db import SessionStore

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class SessionListAPITestCase(APITestCase):

    """ test SessionList """

    def setUp(self):

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.save()

        user_obj = User.objects.first()

        s = SessionStore()
        s.create()
        self.session_key = s.session_key

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

    def test_not_login(self):
        """ test when user isn't auth """

        url = api_reverse('session_list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ if login """

        url = api_reverse('session_list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')
        pprint(response.data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
