""" api tests Image model """
import json
import io
from PIL import Image as PILImage
from pprint import pprint
import ipdb
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User
from django.core.files.images import ImageFile
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER

from values.models import (
    Node,
)
from tag_node.models import NodeProperties
from mainapp.models import Image


class ImageAPITestCase(APITestCase):

    """ test Image model """

    def setUp(self):
        """ set up parameters """

        user = User.objects.create(
            username='denis', email='anceladamusic@gmail.com')
        user.set_password('PDVAncel115648')
        user.has_perm('shop.change_image')
        user.save()

        user2 = User.objects.create(
            username='denis2', email='anceladamusic@gmail.com'
        )
        user2.set_password('PDVAncel115648')
        user2.save()

        user_obj = User.objects.first()

        payload = payload_handler(user_obj)

        self.token_rsp = encode_handler(payload)

        Node.objects.create(**{
            'id': 1,
        })

        Node.objects.create(**{
            'id': 2,
        })

        self.file = io.BytesIO()
        img = PILImage.new('RGB', (100, 100), color=(155, 0, 0))
        img.save(self.file, 'png')
        self.file.name = 'test.png'
        self.file.seek(0)

        Image.objects.create(**{
            'image': ImageFile(self.file),
            'owner': user_obj,
            'content_type': ContentType.objects.get(model='nodeproperties'),
            'object_id': NodeProperties.objects.first().id,
        })

        Image.objects.create(**{
            'image': ImageFile(self.file),
            'owner': user2,
            'content_type': ContentType.objects.get(model='nodeproperties'),
            'object_id': NodeProperties.objects.last().id,
        })

    def tearDown(self):

        Node.objects.all().delete()

        Image.objects.all().delete()

    def test_not_login(self):
        """ check unaviable if unauthorized """

        url = api_reverse('api-mainapp:image-list')

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_login(self):
        """ test list if authorized """

        url = api_reverse('api-mainapp:image-list')

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.get(url, {}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post(self):
        """ test create """

        url = api_reverse('api-mainapp:image-list')

        file = io.BytesIO()
        img = PILImage.new('RGB', (100, 100), color=(155, 0, 0))
        img.save(file, 'png')
        file.name = 'test.png'
        file.seek(0)

        data = {
            'image': file,
            'content_type': ContentType.objects.get(model='nodeproperties').id,
            'object_id': NodeProperties.objects.first().id,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='multipart')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_postonly_file(self):
        """ test create """

        url = api_reverse('api-mainapp:image-list')

        file = io.BytesIO()
        img = PILImage.new('RGB', (100, 100), color=(155, 0, 0))
        img.save(file, 'png')
        file.name = 'test.png'
        file.seek(0)

        data = {
            'image': file,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.post(url, data, format='multipart')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_patch_not_owner(self):
        """ test permission """

        url = api_reverse('api-mainapp:image-detail',
                          kwargs={'id': Image.objects.last().id})

        data = {
            'content_type': ContentType.objects.get(model='nodeproperties').id,
            'object_id': NodeProperties.objects.last().id,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_patch_content_type(self):
        """ test updates only object_id and content_type """

        url = api_reverse('api-mainapp:image-detail',
                          kwargs={'id': Image.objects.first().id})

        data = {
            'content_type': ContentType.objects.get(model='nodeproperties').id,
            'object_id': NodeProperties.objects.last().id,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_patch_image(self):
        """ tests updates only image """

        url = api_reverse('api-mainapp:image-detail',
                          kwargs={'id': Image.objects.first().id})

        file = io.BytesIO()
        img = PILImage.new('RGB', (100, 100), color=(155, 0, 0))
        img.save(file, 'png')
        file.name = 'test123.png'
        file.seek(0)

        data = {
            'image': file,
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.patch(url, data, format='multipart')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete(self):
        """ deletes one object """

        url = api_reverse('api-mainapp:image-detail',
                          kwargs={'id': Image.objects.first().id})

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
