import ipdb
import json

from rest_framework.decorators import (
    api_view,
    permission_classes
)
from rest_framework.permissions import (
    IsAuthenticated,
)
from rest_framework.response import Response
from rest_framework import status
from rest_framework_bulk import BulkModelViewSet

from django.contrib.sessions.models import Session
from django.utils import timezone

from mainapp.rest_permissions import IsOwnerOrReadOnly
from mainapp.rest_bulk_destroyer import RestBulkDestroyer
from mainapp.models import Image
from mainapp.api.serializers import (
    ImageSerializer
)


class ImageViewSet(RestBulkDestroyer, BulkModelViewSet):
    """
    images
    """

    lookup_field = 'id'
    serializer_class = ImageSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        return serializer.save(owner=self.request.user)

    def get_queryset(self, *args, **kwargs):
        if getattr(self.request.user, 'is_authenticated'):
            image = Image.objects.filter(owner=self.request.user)
            return image

    def perform_destroy(self, serializer):
        if self.request.body:
            objects = json.loads(self.request.body)
            Image.objects.filter(owner=self.request.user, id__in=(
                obj['id'] for obj in objects)).delete()
        else:
            serializer.delete()


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def session_list(request):
    """
    get list of sessions
    """

    sessions = Session.objects.filter(
        expire_date__gte=timezone.now()).values('session_key')

    result = {
        'sessions': [session['session_key'] for session in sessions]
    }

    return Response(data=result, status=status.HTTP_200_OK)
