
from rest_framework import permissions

class IsOwnerOrReadOnly(permissions.BasePermission):

	""" allows only owner to update """

	def has_object_permission(self, request, view, obj):
		""" allows get, head or options request """
		if request.method in permissions.SAFE_METHODS:
			return True

		return obj.owner == request.user
