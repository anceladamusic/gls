

from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from django.conf import settings

MODELS_SIGNALS_IS_ACTIVE: bool = settings.MODELS_SIGNALS_IS_ACTIVE


class Image(models.Model):
    """ creates Image store """

    class Meta():
        verbose_name = 'Фото'
        verbose_name_plural = 'Фотографии'

    image = models.ImageField(
        verbose_name='Картинка', upload_to='images/full_size/')
    image_cropped = models.ImageField(
        verbose_name='Скропанная картинка', upload_to='images/cropped/', null=True)
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name='Пользователь')
    content_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE, null=True)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self): return str(self.image)


class File(models.Model):

    """ File """
    class Meta:

        """ params """

        verbose_name = 'файл'
        verbose_name_plural = 'файлы'

    source = models.FileField('Файл', upload_to='files/')
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        if self.content_object:
            return self.content_object.as_dict()['name']
        else:
            return 'Нет наименования'
