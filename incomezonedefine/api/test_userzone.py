
from pprint import pprint
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from rest_framework_jwt.settings import api_settings

from django.contrib.auth.models import User

from scene.models import LoadLandscape
from ..models import UserZone

payload_handler = api_settings.JWT_PAYLOAD_HANDLER
encode_handler = api_settings.JWT_ENCODE_HANDLER


class UserzoneAPITestCase(APITestCase):

	""" test UserZone """

	def setUp(self):

		user = User.objects.create(
			username='denis', email='anceladamusic@gmail.com')
		user.set_password('PDVAncel115648')
		user.save()

		user2 = User.objects.create(username='denis2', email='anc@gmail.com')
		user2.set_password('PDVAncel1156482')
		user2.save()

		user_obj = User.objects.first()

		payload = payload_handler(user_obj)

		self.token_rsp = encode_handler(payload)

		self.landscape = LoadLandscape.objects.create(**{
			"name": "сцена для тестирования взаимодействия ws-sp",
			"camera_position_x": 70.0,
			"camera_position_y": -80.0,
			"camera_position_z": 50.0,
			"camera_up_x": 0.0,
			"camera_up_y": 0.0,
			"camera_up_z": 1.0,
			"controls_target_x": 67.0,
			"controls_target_y": 40.0,
			"controls_target_z": 0.0,
			"dae_rotation_x": 3.14159265359,
			"dae_rotation_y": 3.14159265359,
			"dae_rotation_z": 3.14159265359,
			"dae_position_x": 76.639468457,
			"dae_position_y": 74.03660896,
			"dae_position_z": 6.0,
			"minx": 0.0,
			"miny": 0.0,
			"minz": 0.0,
			"maxx": 149.998046875,
			"maxy": 149.998046875,
			"maxz": 0.0,
			"circle_step_symbol": True,
			"get_wall_height_symbol": True,
			"light_target_symbol": True,
			"server_ip_address": "http://192.168.1.111:7123"
		})

		self.userzone = UserZone.objects.create(**{
			'name': 'new userzone',
			'LoadLandscape': self.landscape,
			'User': user_obj,
		})

		UserZone.objects.create(**{
			'name': 'userzone 2',
			'LoadLandscape': self.landscape,
			'User': user2,
		})

	def test_not_login(self):
		""" test when user isn't auth """

		url = api_reverse('api-incomezonedefine:userzone-list')

		response = self.client.get(url, {}, format='json')

		self.assertEqual(response.status_code,
						 status.HTTP_401_UNAUTHORIZED)

	def test_login(self):
		""" if login """

		url = api_reverse('api-incomezonedefine:userzone-list')

		self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

		response = self.client.get(url, {}, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_multiple_post(self):
		""" posts multiple objects """

		url = api_reverse('api-incomezonedefine:userzone-list')

		data = [{
			'name': 'one more userzone',
			'LoadLandscape': self.landscape.id,
		}]

		self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

		response = self.client.post(url, data, format='json')

		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_patch_multiple(self):
		""" patches multiple objects """

		url = api_reverse('api-incomezonedefine:userzone-list')

		self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

		data = [{
			'id': self.userzone.id,
			'name': 'renamed userzone',
		}]

		response = self.client.patch(url, data, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)


	def test_patch_multiple_not_owner(self):
		""" not patches multiple if not owner """

		url = api_reverse('api-incomezonedefine:userzone-list')

		self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

		data = [{
			'id': UserZone.objects.last().id,
			'name': 'renamed userzone',
		}]

		response = self.client.patch(url, data, format='json')

		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_delete_multiple(self):
		""" deletes multiple objects """

		url = api_reverse('api-incomezonedefine:userzone-list')

		self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

		data = [{
			'id': UserZone.objects.first().id,
		}]

		response = self.client.delete(url, data, format='json')

		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


	def test_delete_multiple_not_owner(self):
		""" not owner deletes multiple objects """

		url = api_reverse('api-incomezonedefine:userzone-list')

		self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_rsp)

		data = [{
			'id': UserZone.objects.last().id,
		}]

		response = self.client.delete(url, data, format='json')

		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
