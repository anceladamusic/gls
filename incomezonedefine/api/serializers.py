from rest_framework.serializers import (
    ModelSerializer,
    PrimaryKeyRelatedField,
    SerializerMethodField,
)

from rest_framework_bulk import (
    BulkSerializerMixin,
    BulkListSerializer,
)

from ..models import UserZone


class UserzoneSerializer(BulkSerializerMixin, ModelSerializer):
    """ serializes UserZone """

    static_type = SerializerMethodField('add_type')

    @staticmethod
    def add_type(value):
        """ adds field type of static"""
        return 'userzone'

    class Meta:
        """ props """

        model = UserZone
        list_serializer_class = BulkListSerializer
        fields = [
            field.name for field in UserZone._meta.fields if field.name != 'User'] + ['static_type']
        read_only_fields = ['id', 'User']
