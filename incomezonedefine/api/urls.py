
from rest_framework_bulk.routes import BulkRouter

from django.urls import path
from django.conf.urls import include

from .views import (
    UserzoneViewSet,
)

router = BulkRouter()
router.register(r'userzone', UserzoneViewSet, 'userzone')

urlpatterns = [
    path('api-incomezonedefine/', include((router.urls,
                                           'api-incomezonedefine'), namespace='api-incomezonedefine')),
]
