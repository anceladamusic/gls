
import ipdb

from rest_framework_bulk import BulkModelViewSet
from rest_framework.permissions import IsAuthenticated

from mainapp.rest_permissions import IsOwnerOrReadOnly
from mainapp.rest_bulk_destroyer import RestBulkDestroyer
from .serializers import UserzoneSerializer
from ..models import UserZone


class UserzoneViewSet(RestBulkDestroyer, BulkModelViewSet):
    """ userzones """

    lookup_field = 'id'
    serializer_class = UserzoneSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        return serializer.save(User=self.request.user)

    def get_queryset(self, *args, **kwargs):
        if getattr(self.request.user, 'is_authenticated'):
            if 'landscape_id' in self.request.GET:
                return UserZone.objects.all().filter(
                    User=self.request.user,
                    LoadLandscape_id=self.request.GET.get('landscape_id'))
            return UserZone.objects.all().filter(User=self.request.user)
