from django.apps import apps
from django.contrib import admin
from django_mptt_admin.admin import DjangoMpttAdmin
from .models import *

# зоны контроля


class VerticesUserZoneInline(admin.TabularInline):
    model = VerticesUserZone


class GroupUserZoneUserZoneInline(admin.TabularInline):
    model = GroupUserZoneUserZone

# зоны исключения


class Inlines():

    def __init__(self):
        self.model = {
            'incomezonedefine.userzone': [
                VerticesUserZoneInline,
            ],
            'incomezonedefine.groupuserzone': [
                GroupUserZoneUserZoneInline,
            ],
        }


app_models = apps.get_app_config('incomezonedefine').get_models()

for model in app_models:

    if not model.is_hidden():
        class ModelAdmin(admin.ModelAdmin):
            list_display = [field.name for field in model._meta.fields]

            if str(model._meta) in Inlines().model:

                inlines = Inlines().model[str(model._meta)]

        admin.site.register(model, ModelAdmin)
