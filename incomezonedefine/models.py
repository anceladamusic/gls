""" list of objects models"""
from django.db import models
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.conf import settings

from scene.models import (LoadLandscape)

MODELS_SIGNALS_IS_ACTIVE: bool = settings.MODELS_SIGNALS_IS_ACTIVE

#################
# зоны контроля
#################


class ControlZone(models.Model):

    class Meta():
        verbose_name = 'зона контроля'
        verbose_name_plural = 'зоны контроля'

    Name = models.CharField('наименование', blank=True,
                            null=True, max_length=200)
    Description = models.TextField('описание', blank=True, null=True)
    x = models.FloatField('x', null=True, blank=True)
    y = models.FloatField('y', null=True, blank=True)
    z = models.FloatField('z', null=True, blank=True)
    width = models.FloatField('ширина', null=True, blank=True)
    height = models.FloatField('высота', null=True, blank=True)
    depth = models.FloatField('глубина', null=True, blank=True)
    angle = models.FloatField('угол поворота', null=True, blank=True)
    server_id = models.IntegerField(
        'идентификатор на сервере позиционирования', null=True, blank=True, default=-1)
    LoadLandscape = models.ForeignKey(
        LoadLandscape, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Сцена')

    def __str__(self):
        return self.Name

    @staticmethod
    def get_table_type():
        return 'models.Model'

    @staticmethod
    def is_hidden():
        return False


class UserZone(models.Model):

    class Meta():
        verbose_name = 'Зона пользователя'
        verbose_name_plural = 'Зоны пользователя'

    name = models.CharField(
        'наименование', max_length=200, blank=True, null=True)
    User = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name='пользователь')
    LoadLandscape = models.ForeignKey(
        LoadLandscape, on_delete=models.CASCADE, null=True, blank=True, verbose_name='сцена')

    def __str__(self):
        return 'user:{0}|name:{1}'.format(self.User, self.name)

    @property
    def owner(self):
        """ adds user prop """
        return self.User

    def as_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'User': self.User_id,
            'LoadLandscape': self.LoadLandscape_id,
        }

    @staticmethod
    def get_table_type():
        return 'models.Model'

    @staticmethod
    def is_hidden():
        return False

    def get_type(self):
        return 'userzone'

    def get_name(self):
        return self.name

    def get_report_choices(self):
        return {
            'id': {
                'option': '{0} | {1}'.format(self.name, self.LoadLandscape),
                'value': self.id,
            }
        }


class VerticesUserZone(models.Model):

    xCoord = models.FloatField()
    yCoord = models.FloatField()
    zmin = models.FloatField(blank=True, null=True)
    zmax = models.FloatField(blank=True, null=True)
    UserZone = models.ForeignKey(UserZone, on_delete=models.CASCADE)

    def __str__(self):
        return '{0}'.format(self.UserZone)

    @staticmethod
    def get_table_type():
        return 'models.Model'

    @staticmethod
    def is_hidden():
        return True


class GroupUserZone(models.Model):

    class Meta():
        verbose_name = 'Группа зон пользователя'
        verbose_name_plural = 'Группы зон пользователей'

    GroupName = models.CharField(
        'наименование', max_length=200, null=True, blank=True)
    GroupDescription = models.TextField('описание', null=True, blank=True)
    User = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name='пользователь', null=True, blank=True)
    LoadLandscape = models.ForeignKey(
        LoadLandscape, on_delete=models.CASCADE, verbose_name='сцена', null=True, blank=True)

    def __str__(self):
        return '{0}'.format(self.GroupName)

    @staticmethod
    def get_table_type():
        return 'models.Model'

    @staticmethod
    def is_hidden():
        return False


class GroupUserZoneUserZone(models.Model):

    GroupUserZone = models.ForeignKey(GroupUserZone, on_delete=models.CASCADE)
    UserZone = models.OneToOneField(UserZone, on_delete=models.CASCADE)

    def __str__(self):
        return 'groupname:{0}|uzone:{1}'.format(self.GroupUserZone, self.UserZone)

    @staticmethod
    def get_table_type():
        return 'models.Model'

    @staticmethod
    def is_hidden():
        return True
