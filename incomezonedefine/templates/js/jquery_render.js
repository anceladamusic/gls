//дистанция между камерой и сеткой

minimum_distance = 40;
color = 0xfbfffe;
/*common_module.render.build_grid_when_less_distance(minimum_distance, color);*/

//end incomezonedefine


//incomezonedefine
// objectForVerticalMovement окно изменения позиции объекта по вертикали следует за объектом
incomezonedefine_module.render.vertical_movement_window();

//добавить helpers на vertices
incomezonedefine_module.render.vertices_helpers_follow();

//удалить точки многоугольника
incomezonedefine_module.render.remove_vertices_meshes_n_helpers();

//когда AddVerticeEnabled вычислять координаты мыши
incomezonedefine_module.render.mouse_coords_add_vertice_follow();

//когда moveenable вычислять координаты мыши на координатной сетке по горизонтали
incomezonedefine_module.render.mouse_coords_object_follow();

//helpers следуют за зонами контроля czone
incomezonedefine_module.render.helper_follow_czone(CzoneShowed);

//helpers следуют за userzone
incomezonedefine_module.render.helper_follow_zone(userzone_module.userzone);

//helpers следуют за excludezone
incomezonedefine_module.render.helper_follow_zone(excludezone);

//helpers следуют за роутерами router
incomezonedefine_module.render.helper_follow_object(Objects);

//userzonewindow следует за объектом
incomezonedefine_module.render.window_follow(uzoneForWindow, $('#userzonewindow'));

//exludezonewindow следует за объектом
incomezonedefine_module.render.window_follow(ezoneForWindow, $('#excludezonewindow'));

//czonewindow следует за объектом
incomezonedefine_module.render.window_follow(czoneForWindow, $('#czonewindow'));

// оbjectwindow следует за объектом
incomezonedefine_module.render.window_follow(objectForWindow, $('#objectwindow'));