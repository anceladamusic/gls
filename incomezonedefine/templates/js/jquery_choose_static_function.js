//common
chooseStatic = function(){

    $('#staticwindow').css({display: 'none'});

    //camera switch
    common_module.camera_toggle(colladaObjects.landscape[0], staticForWindow.name);

    var dae_name, elem, zmin, kabinet, building, floor;
    
    var parameters;
    
    var flr = staticForWindow.parent;
    
    var static_arr_node;
    
    var static_list = [];
    
    //if building chosen
    if (staticForWindow.name.indexOf('building') != -1){
    //endcommon

        //incomezonedefine
        removeGrid();
        //end incomezonedefine

        //common
        dae_name = staticForWindow.name;
        //endcommon

        //remove opacity from all buildings
        common_module.buildings_opacity_back(colladaObjects.landscape[0]);

        //common
        common_module.opacity_finder(colladaObjects.landscape[0], dae_name, 0.8);
        
        //show hidden objects
        common_module.toggle_visible_on(colladaObjects.landscape[0], dae_name);
        //endcommon

        //incomezonedefine
        //show linked to building objects
        if ($('#showobjects')[0].checked==true){
            
            objecttype = $('#objecttype_table_list option:selected')[0].value;
            
            object_module.show_object_type('showlinkedobjectsbuilding', objecttype, dae_name);
            
        }
        
        //show linked to building control zones
        if ($('#showczones')[0].checked==true){
            
            controlzone_module.show_czone('showlinkedczonesbuilding', dae_name);
            
        }
        
        //show linked to building excludezones
        if ($('#showexcludezone')[0].checked==true){
            
            excludezone_module.show_ezone('showlinkedezonebuilding', dae_name);
            
        }
        
        //show linked to building userzones
        if ($('#showuserzone')[0].checked==true){
            
            userzone_module.show_user_zone('showlinkeduzonebuilding', dae_name);
            
        }
        //end incomezonedefine

        
        //lookup z coords of the last floor
        //incomezonedefine
        
        elem = [];
        
        common_module.elem_look_up(colladaObjects.landscape[0], dae_name, elem);
        
        zmin = [];
        
        $.each(elem[0].children, function(index){
            if ('BoxMin' in elem[0].children[index]){
                zmin.push(elem[0].children[index].BoxMin.z);    
            }
        });
        
        selectedstaticZ = Math.max.apply(Math, zmin);
    
        selected_element_name = dae_name;
        selected_element_type = 'building';
        //end incomezonedefine

        //remove ligthup if kabinet chosen
        //common
        minmaxstatic_selected_kab = 0; 

        //common
        building = [];
    
        static_arr_node = common_module.get_branch_from_static_arr(
            staticarr,
            staticForWindow.db_id,
            building
        );
    
    
        $.each(building[0].children, function(i){
          static_list.push({
            'id': building[0].children[i].id,
            'dae_name': building[0].children[i].dae_name
          });
        });
        
        //remove elems from static
        clearMinmaxstatic();
    
        //make request on building static_plane
        parameters = {};
        parameters.static_list = static_list;
        parameters.method = 'get_static_vertices';
        parameters.landscape_id = landscape_id;
        parameters.type = 'floor';
        makeAjax(parameters);

    // if floor chosen
    } else if (staticForWindow.name.indexOf('floor') != -1){
    //end common
    
        //incomezonedefine
        removeGrid();
        //end incomezonedefine

        //incomezonedefine
        dae_name = staticForWindow.name;
        
        // build net
        var color = new THREE.Color(0xfff);
        common_module.build_grid(100, 200, staticForWindow.Center, color);
        //end incomezonedefine

        //incomezonedefine
        //show linked objects
        if ($('#showobjects')[0].checked==true){
            objecttype = $('#objecttype_table_list option:selected')[0].value;
            object_module.show_object_type('showlinkedobjectsfloor', objecttype, dae_name);
        }
        //show linked czones
        if ($('#showczones')[0].checked==true){
            controlzone_module.show_czone('showlinkedczonesfloor', dae_name);
        }
        // show linked excludezones
        if ($('#showexcludezone')[0].checked==true){
            excludezone_module.show_ezone('showlinkedezonefloor', dae_name);
        }
        //show linked userzones
        if ($('#showuserzone')[0].checked==true){
            userzone_module.show_user_zone('showlinkeduzonefloor', dae_name);
        }

        //find coords of floor
        selectedstaticZ = staticForWindow.BoxMin.z;
        
        selected_element_name = dae_name;
        selected_element_type = 'floor';
        //end incomezonedefine

        //common
        //remove lightup, if kabinet chosen
        
        //remove lightup, if kabinet chosen
        minmaxstatic_selected_kab = 0;
        flr = staticForWindow.parent.parent;
    
        //fill kabinet
        kabinet = [];
    
        static_arr_node = common_module.get_branch_from_static_arr(staticarr, staticForWindow.db_id, kabinet);
    
        $.each(kabinet[0].children, function(i){
          static_list.push({
            'id': kabinet[0].children[i].id,
            'dae_name': kabinet[0].children[i].dae_name
          });
        });
    
        //toogle floor lookup
        common_module.look_up_floor(colladaObjects.landscape[0], staticForWindow.name, {});
        common_module.camera_toggle(colladaObjects.landscape[0], staticForWindow.name);
    
        //remove elems from static
        clearMinmaxstatic();
    
        //make request to build static_plane
        parameters = {};
        parameters.static_list = static_list;
        parameters.method = 'get_static_vertices';
        parameters.landscape_id = landscape_id;
        parameters.type = 'kabinet';
        makeAjax(parameters);

    // if kabinet chosen
    } else if(staticForWindow.name.indexOf('kabinet') != -1){
    //end common

        kabinet = [];
        common_module.get_branch_from_static_arr(staticarr, staticForWindow.db_id, kabinet);
        kabinet = kabinet[0];
        
        floor = [];
        common_module.get_branch_from_static_arr(staticarr, kabinet.parent_id, floor);
        floor = floor[0];
        
        $.each(floor.children, function(i){
            static_list.push({
              'id': floor.children[i].id,
              'dae_name': floor.children[i].dae_name
            });
        });
      
        //remove elems from static
        clearMinmaxstatic();

        parameters = {};
        parameters.static_list = static_list;
        parameters.method = 'get_static_vertices';
        parameters.type = 'kabinet';
        parameters.landscape_id = landscape_id;
        makeAjax(parameters);

        $.ajax({
            type: "POST",
            url: "/ajax/",
            data: JSON.stringify(parameters),
            contentType: "application/json; charset=utf-8",
            headers: {
                'X-CSRFToken': getCookie('csrftoken')
            },
            dataType: "json",
            async: true,
            success: function(data){

                //remove elems from static
                clearMinmaxstatic();

                $.each(data.string, function(i){
                    minmaxstatic.push(data.string[i]);
                });

                $.each(minmaxstatic, function(i){
                    var elem = [];
                    common_module.elem_look_up(
                        colladaObjects.landscape[0],
                        minmaxstatic[i].dae_name,
                        elem
                    );
                    elem = elem[0];
                    var minz = elem.parent.BoxMin.z;
                    //show object
                    var varr = minmaxstatic[i].vertices;
                    color = 0x66a182;
                    var offset = 0.09;
                    var transparent = true;
                    var opacity = 1;
                    //build plane
                    var a = common_module.build_static_plane(
                        varr, elem.geometry, color, minz, offset, transparent, opacity);
                    scene.add(a);
                    minmaxstatic[i].mesh = a;
                    minmaxstatic[i].mesh.material.visible = false;
                });
            
                //incomezonedefine
                removeGrid();
                //end incomezonedefine

                //incomezonedefine
                //show helper
                incomezonedefine_module.create_box_helper(staticForWindow);
                //end incomezone

                dae_name = staticForWindow.name;

                // build net
                color = new THREE.Color(0xfff);
                common_module.build_grid(100, 200, staticForWindow.Center, color);

                //common
                //lightup kabinet which chosen
                $.each(minmaxstatic, function(i){
                    if (minmaxstatic[i].dae_name == dae_name ){
                        minmaxstatic[i].mesh.material.visible = true;
                    } else {
                        minmaxstatic[i].mesh.material.visible = false;
                    }
                });
                //end common

                //incomezonedefine
                //chow linked objects
                if ($('#showobjects')[0].checked==true){
                    objecttype = $('#objecttype_table_list option:selected')[0].value;
                    object_module.show_object_type('showlinkedobjectskabinet', objecttype, dae_name);
                }

                //show linked controlzones
                if ($('#showczones')[0].checked==true){
                    controlzone_module.show_czone('showlinkedczoneskabinet', dae_name);
                }
                // show excludezone
                if ($('#showexcludezone')[0].checked==true){
                    excludezone_module.show_ezone('showlinkedezonekabinet', dae_name);
                }

                // show userzone
                if ($('#showuserzone')[0].checked==true){
                    userzone_module.show_user_zone('showlinkeduzonekabinet', dae_name);
                }
                //end incomezonedefine

                //lookup kabinet
                common_module.look_up_kabinet(colladaObjects.landscape[0], staticForWindow.name, {});
            }
        });
    }
}