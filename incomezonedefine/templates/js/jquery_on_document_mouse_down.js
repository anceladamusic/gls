common_module.init.raycaster_properties(event, mouse, camera);
//end common

//incomezonedefine
//objects
var interseptedObjects = [];

//controlzones
var interseptedCzones = [];

//excludezones
var interseptedEzones = [];

//userzones
var interseptedUzones = [];

//static
var interseptedStatic = [];

//lookup userzone
interseptedUzones = incomezonedefine_module.init.intersects_uzone(
	event,
	userzone_module.userzone,
	intersectsUzones,
	selected_element_type,
	interseptedUzones
);

//lookup excludezone
interseptedEzones = incomezonedefine_module.init.intersects_ezone(
	event,
	excludezone,
	intersectsEzones,
	selected_element_type,
	interseptedEzones
);

//lookup Objects
interseptedObjects = incomezonedefine_module.init.intersects_objects(
	event,
	Objects,
	intersectsObjects,
	interseptedObjects
);

//lookup Czone
interseptedCzones = incomezonedefine_module.init.intersects_czones(
	event,
	CzoneShowed,
	intersectsCzones,
	interseptedCzones
);

// end incomezonedefine

//incomezonedefine
//lookup static
if (interseptedObjects.length == 0 && interseptedCzones.length == 0 &&
 interseptedEzones.length == 0 && interseptedUzones.length == 0){
	// end incomezonedefine
   
	//incomezonedefine
	//lookup static
	interseptedStatic = common_module.init.intersects_static(
		event,
		minmaxstatic,
		intersectsStatic,
		interseptedStatic
	);
	//end incomezonedefine
   
	//common
}
//end commmon

// incomezonedefine
//if not object and not... catch static
if (interseptedObjects.length == 0 && interseptedCzones.length == 0 &&
 interseptedEzones.length == 0 && interseptedUzones.length == 0 && interseptedStatic.length == 0){
 
	//end incomezonedefine
	
	var building = [];
	var floor = [];

	//common
	//lookup floors in building
	common_module.init.elem_look_up_like_text(
	 colladaObjects.landscape[0], 'building', building);

	$.each(building, function(index){
		common_module.init.elem_look_up_like_text(building[index], 'floor', floor);
	});
 
	//catch floor
	var intersectsFloor = common_module.init.catch_floor(floor);
 
	$.each(intersectsFloor, function(){
		if (intersectsFloor.length > 0){
	
			building = [];
			common_module.get_parent_from_static_arr(staticarr, intersectsFloor[0].object.db_id, building);
	
			common_module.look_up_building(colladaObjects.landscape[0], building[0].dae_name);
			return false;
		}
	});
 
	//if (intersectsFloor.length > 0){
	// 
	// dae_elem = intersectsFloor[0].object.parent.parent.name;
	// common_module.look_up_building(colladaObjects.landscape[0], dae_elem);
	// return false;
	//
	//}

}